use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::collections::VecDeque;

fn find2(map : &Vec<Vec<i8>>, q : i8) -> Option<(i32,i32)> {
    let it = map.iter().map(
        |row| row.iter().position(|&x| x==q)
    ).enumerate();

    for (y,j) in it {
        match j {
            Some(x) => { return Some((x as i32,y as i32)); }
            None => {}
        }
    }
    None
}

fn bfs(mat : &Vec<Vec<i8>>, start : (i32,i32), goal : i8, direction : i8) -> Option<usize> {
    let mut seen : Vec<(i32,i32)> = vec![start];
    let mut queue : VecDeque<((i32,i32), usize)> = VecDeque::from([(start,0)]);

    let h = mat.len() as i32;
    let w = mat[0].len() as i32;

    while let Some(((x0,y0), l)) = queue.pop_front() {
        let current = mat[y0 as usize][x0 as usize];
        if current == goal {
            return Some(l);
        }

        for (x, y) in [(x0,y0+1),(x0,y0-1),(x0+1,y0),(x0-1,y0)] {
            if seen.contains(&(x,y)) {
                continue;
            }

            if x < 0 { continue; }
            if y < 0 { continue; }
            if x >= w { continue; }
            if y >= h { continue; }

            let new = match mat[y as usize][x as usize] {
                69 => 'z' as i8,
                x => x
            };

            if direction*(new - current) > 1 {
                continue;
            }
            seen.push((x,y));
            queue.push_back(((x,y), l+1));
        }
    }
    None
}

fn parse_input(lines : Vec<String>) -> (Vec<Vec<i8>>, (i32,i32)) {
    let mut map : Vec<Vec<i8>> = lines.iter().map(
        |l| l.chars().map(|c| c as i8).collect::<Vec<i8>>()
    ).collect();

    match find2(&map, 'S' as i8) {
        Some((x, y)) => {
            map[y as usize][x as usize] = 'a' as i8;
            (map, (x,y))
        },
        None => { panic!("Can't find start"); }
    }
}

fn solve12a(lines : Vec<String>) -> usize {
    let (map, start) = parse_input(lines);
    bfs(&map, start, 'E' as i8, 1).unwrap()
}

fn solve12b(lines : Vec<String>) -> usize {
    let (map, _) = parse_input(lines);
    match find2(&map, 'E' as i8) {
        Some(end) => bfs(&map, end, 'a' as i8, -1).unwrap(),
        None => { panic!("Can't find end"); }
    }
}

fn main() {
    let path = Path::new("input12.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve12a(lines.clone()));
    println!("{}", solve12b(lines.clone()));
}
