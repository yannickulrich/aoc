class monkey:
    def __init__(self, block):
        self.items = list([
            int(i) for i in block[1].split(": ")[1].split(', ')
        ])
        self.op = eval("lambda old : " + block[2].split(' = ')[1])
        self.mod = int(block[3].split(' ' )[-1])
        self.targets = (
            int(block[4].split(' ')[-1]),
            int(block[5].split(' ')[-1])
        )

        self.ninspect = 0

    def round(self, monkeys, mod=None):
        for i in self.items:
            self.ninspect += 1
            level = self.op(i)
            if mod:
                level = level % mod
            else:
                level = level // 3
            if level % self.mod == 0:
                monkeys[self.targets[0]].catch(level)
            else:
                monkeys[self.targets[1]].catch(level)
        self.items = []

    def catch(self, item):
        self.items.append(item)


monkeys = [
    monkey(block.split('\n'))
    for block in open('input11.txt').read().split('\n\n')
]

for i in range(20):
    for m in monkeys:
        m.round(monkeys)

business = sorted([m.ninspect for m in monkeys])
print(business[-1]*business[-2])


monkeys = [
    monkey(block.split('\n'))
    for block in open('input11.txt').read().split('\n\n')
]
mod = 1
for i in monkeys: mod *= i.mod


for i in range(20):
    for i,m in enumerate(monkeys):
        m.round(monkeys, mod)

business = sorted([m.ninspect for m in monkeys])
print(business[-1]*business[-2])
