mat = ToCharacterCode /@ StringSplit[
  Import["input12.txt", "String"],
  "\n"
] - 97;

start = FirstPosition[mat, -14];
finish = FirstPosition[mat, -28];
mat[[start[[1]], start[[2]]]] = 0;
mat[[finish[[1]], finish[[2]]]] = 25;

neighbours = Select[
  Flatten[
    Outer[f[{##}] &, Range@Length[mat], Range[Length[mat[[1]]]]] /. {
      f[a_] :> {
        f[a] -> f[a - {1, 0}],
        f[a] -> f[a - {0, 1}],
        f[a] -> f[a + {1, 0}],
        f[a] -> f[a + {0, 1}]
      }
    }
  ],
  FreeQ[0 | f[{Length[mat] + 1, _}] | f[{_, Length[mat[[1]]] + 1}]]
];


AllowedQ[f[{x1_, y1_}] -> f[{x2_, y2_}]] := \
  (mat[[x2, y2]] - mat[[x1, y1]]) <= 1

allowed = Select[neighbours, AllowedQ];

Print[Length@FindShortestPath[Graph[allowed],start // f, finish // f]-1];
