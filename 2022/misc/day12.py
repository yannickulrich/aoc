from tqdm import tqdm
txt = open('input12.txt').read()
dat = [[ord(i)-ord('a') for i in j] for j in txt.splitlines()]



def astar(dat, start, finish):

    n = len(dat)
    m = len(dat[0])
    def pos2ind(p):
        x,y = p
        return y*m+x

    def ind2pos(i):
        return (i%m,i//m)

    def neighbours(i):
        x,y = ind2pos(i)
        if x > 0:
            yield pos2ind((x-1,y))
        if x < m-1:
            yield pos2ind((x+1,y))
        if y > 0:
            yield pos2ind((x,y-1))
        if y < n-1:
            yield pos2ind((x,y+1))

    def h(i):
        x,y = ind2pos(i)
        xf,yf = finish
        return (abs(x - xf) + abs(y - yf))

    def datat(i):
        x,y = ind2pos(i)
        return dat[y][x]

    inf = 10000000
    prev = {i: None for i in range(n*m)}
    Q = [pos2ind(start)]

    gdist = {i: inf for i in range(n*m)}
    gdist[pos2ind(start)] = 0

    fdist = {i: inf for i in range(n*m)}
    fdist[pos2ind(start)] = h(pos2ind(start))

    while len(Q) > 0:
        u = min(Q, key=fdist.get)
        Q.remove(u)

        if u == pos2ind(finish):
            break

        dat0 = datat(u)
        for v in neighbours(u):
            if datat(v) > dat0+1:
                continue

            tgs = gdist[u] + 1
            if tgs < gdist[v]:
                gdist[v] = tgs
                fdist[v] = tgs + h(v)
                prev[v] = u

                if v not in Q:
                    Q.append(v)


    acc = 0
    u = pos2ind(finish)
    S = [u]
    while True:
        u = prev[u]
        if u == None:
            break
        S.append(u)
        acc += datat(u)

    return S

start = [(i.index(-14),j) for j,i in enumerate(dat) if -14 in i][0]
goal  = [(i.index(-28),j) for j,i in enumerate(dat) if -28 in i][0]
dat[start[1]][start[0]] = 0
dat[goal[1]][goal[0]] = 25

print(len(astar(dat, start, goal))-1)


opts = []
for y, row in tqdm(enumerate(dat)):
    for x, col in enumerate(row):
        if col == 0:
            opts.append(len(astar(dat, (x,y), goal))-1)
print(min([i for i in opts if i>0]))
