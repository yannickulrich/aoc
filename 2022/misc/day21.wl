StringReplace[
    Import["input21.txt", "Text"],
    {":" -> "=", "\n" -> ";"}
] // ToExpression;
root // Print

Clear /@ Names["Global`*"];

StringReplace[
  StringReplace[
      Import["input21.txt", "Text"],
      {":" -> "=", "\n" -> ";"}
  ], {
    RegularExpression["root= (....) . (....)"] -> "root = $1 == $2",
    RegularExpression["humn= ....;"] -> ""
}] // ToExpression;
root // Solve // Print
