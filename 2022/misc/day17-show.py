import sys
import numpy as np
import matplotlib.pyplot as plt

while True:
    dat = eval(sys.stdin.readline())
    mat=np.zeros((30,7))
    for x,y in dat:
        mat[y][x]=1

    im1 = plt.imshow(mat[::-1])
    plt.show()
