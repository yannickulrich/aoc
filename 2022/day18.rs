use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::collections::HashSet;
use std::collections::VecDeque;

type Point = (i32,i32,i32);
type Points = HashSet<Point>;

fn parse(lines : Vec<String>) -> Points {
    let mut ans = HashSet::new();
    ans.extend(lines.iter().filter_map(
        |l| match l.split(",").collect::<Vec<&str>>()[..] {
            [x, y, z] => match (
                    x.parse::<i32>(),
                    y.parse::<i32>(),
                    z.parse::<i32>()
                ) {
                (Ok(x), Ok(y), Ok(z)) => Some((x+1,y+1,z+1)),
                _ => None
            },
            _ => None
        }
    ));
    ans
}

fn faces(inp : &Points, (x,y,z) : Point) -> Vec<Point> {
    let try : Vec<Point> = vec![
        (x+1,y,z),
        (x-1,y,z),
        (x,y+1,z),
        (x,y-1,z),
        (x,y,z+1),
        (x,y,z-1)
    ];
    try.into_iter().filter(|p| !inp.contains(&p)).collect()
}

fn solve18a(lines : Vec<String>) -> usize {
    let inp : Points = parse(lines);
    inp.iter().map(|p| faces(&inp, *p).len()).sum()
}

fn bfs(inp : &Points, size : i32) -> Points {
    let mut seen : Points = HashSet::from([(size,size,size)]);
    let mut queue : VecDeque<Point> = VecDeque::from([(size,size,size)]);

    while let Some(p) = queue.pop_front() {
        for (x,y,z) in faces(inp, p) {
            if x < 0 || x > size ||
               y < 0 || y > size ||
               z < 0 || z > size {
                   continue;
            }
            if seen.contains(&(x,y,z)) {
                continue;
            }

            seen.insert((x,y,z));
            queue.push_back((x,y,z));
        }
    }
    seen
}

fn solve18b(lines : Vec<String>) -> usize {
    let inp : Points = parse(lines);
    let size = inp.iter().flat_map(
        |(x,y,z)| [x.abs(),y.abs(),z.abs()]
    ).max().unwrap() + 1;

    let exterior = bfs(&inp, size);
    inp.iter().flat_map(|p| faces(&inp, *p)).filter(
        |p| exterior.contains(&p)
    ).count()
}

fn main() {
    let path = Path::new("input18.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve18a(lines.clone()));
    println!("{}", solve18b(lines.clone()));
}
