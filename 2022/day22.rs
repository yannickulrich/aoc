use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::collections::HashSet;
use std::collections::HashMap;
use std::ops::{Add,Sub,Neg};

type Point = (usize, usize);
type Points = HashSet<Point>;
#[derive(Debug, Clone, Copy)]
enum Instruction {
    Move(usize),
    Right, Left
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum Direction {
    Right, Down, Left, Up
}

impl Add<u8> for Direction {
    type Output = Self;
    fn add(self, other : u8) -> Self {
        assert_eq!(other, 1);
        match self {
            Direction::Right => Direction::Down,
            Direction::Down => Direction::Left,
            Direction::Left => Direction::Up,
            Direction::Up => Direction::Right,
        }
    }
}

impl Sub<u8> for Direction {
    type Output = Self;
    fn sub(self, other : u8) -> Self {
        assert_eq!(other, 1);
        ((self + 1) + 1) + 1
    }
}

impl Neg for Direction {
    type Output = Self;
    fn neg(self) -> Self {
        match self {
            Direction::Down => Direction::Up,
            Direction::Up => Direction::Down,
            Direction::Left => Direction::Right,
            Direction::Right => Direction::Left
        }
    }
}

fn parse(lines : &Vec<String>) -> (Points, Points, Option<Point>, Vec<Instruction>) {
    let mut spaces : Points = HashSet::new();
    let mut walls : Points = HashSet::new();
    let mut start : Option<Point> = None;

    let mut it = lines.iter();
    let mut y : usize = 0;
    loop {
        match it.next() {
            Some(line) if line.len() == 0 => { break; },
            None => { panic!("iterator ended to early"); },
            Some(line) => {
                for (x,c) in line.chars().enumerate() {
                    match c {
                        ' ' => {},
                        '#' => { walls.insert((x,y)); },
                        '.' => {
                            if start == None { start = Some((x, y)); }
                            spaces.insert((x,y));
                        },
                        _ => { panic!("parse error"); }
                    }
                }
            }
        }
        y += 1;
    }

    let instr = it.next().unwrap().split_inclusive(&['R','L'][..]).flat_map(
        |l| match l.split_at(l.len()-1) {
                (n, "R") => vec![
                    Instruction::Move(n.parse::<usize>().expect("")),
                    Instruction::Right
                ],
                (n, "L") => vec![
                    Instruction::Move(n.parse::<usize>().expect("")),
                    Instruction::Left
                ],
                (n, m) => vec![
                    Instruction::Move((n.to_owned()+m).parse::<usize>().expect(""))
                ]
        }
    ).collect::<Vec<Instruction>>();

    (spaces, walls, start, instr)
}

fn walk(spaces : &Points, walls : &Points, jump_table : &HashMap<(Point, Direction), (Point, Direction)>, loc : &mut Point, steps : usize, dir : &mut Direction) {
    for _ in 0..steps {
        let new = match dir {
            Direction::Right => (loc.0+1, loc.1),
            Direction::Down => (loc.0, loc.1+1),
            Direction::Left => (loc.0-1, loc.1),
            Direction::Up => (loc.0, loc.1-1)
        };

        if walls.contains(&new) { return; }
        if spaces.contains(&new) {
            *loc = new;
        } else {
            let (new, newdir) = *jump_table.get(&(*loc, *dir)).unwrap();

            if !spaces.contains(&new) { return; }
            *loc = new; *dir = newdir;
        }
    }
}

fn solve(spaces : &Points, walls : &Points, start : &Option<Point>, instrs : &Vec<Instruction>, jump_table : &HashMap<(Point, Direction), (Point, Direction)>) -> usize {
    let mut dir = Direction::Right; // facing right
    let mut loc = start.unwrap();

    for i in instrs {
        match i {
            Instruction::Move(n) => {
                walk(&spaces, &walls, &jump_table, &mut loc, *n, &mut dir);
            }
            Instruction::Right => { dir = dir + 1; },
            Instruction::Left => { dir = dir - 1; }
        }
    }

    (loc.1+1)*1000 + (loc.0+1)*4 + (dir as usize)
}

fn solve22a(lines : Vec<String>) -> usize {
    let (spaces, walls, start, instrs) = parse(&lines);

    let mut jump_table : HashMap<(Point, Direction), (Point, Direction)> = HashMap::new();

    for (x,y) in spaces.iter().chain(walls.iter()) {
        if !(spaces.contains(&(*x-1,*y)) || walls.contains(&(*x-1,*y))) {
            // left edge, find right one
            let first_outside = (*x..).find(
                |xx| !(spaces.contains(&(*xx,*y)) || walls.contains(&(*xx,*y)))
            ).unwrap();
            jump_table.insert(((*x,*y), Direction::Left), ((first_outside-1,*y), Direction::Left));
            jump_table.insert(((first_outside-1,*y), Direction::Right), ((*x,*y), Direction::Right));
        }

        if !(spaces.contains(&(*x,*y-1)) || walls.contains(&(*x,*y-1))) {
            // top edge, find bottom one

            let first_outside = (*y..).find(
                |yy| !(spaces.contains(&(*x,*yy)) || walls.contains(&(*x,*yy)))
            ).unwrap();
            jump_table.insert(((*x,*y), Direction::Up), ((*x, first_outside-1), Direction::Up));
            jump_table.insert(((*x, first_outside-1), Direction::Down), ((*x,*y), Direction::Down));
        }
    }

    solve(&spaces, &walls, &start, &instrs, &jump_table)
}

fn dir2edge(mul : usize, panel : u8, dir : Direction, rev : bool) -> Vec<Point> {
    let l0 = if mul == 50 {
        match panel {
            1 => (1,0),
            2 => (2,0),
            3 => (1,1),
            4 => (0,2),
            5 => (1,2),
            6 => (0,3),
            _ => { panic!("panels are 1..6"); }
        }
    } else {
        assert_eq!(mul,4);
        match panel {
            1 => (2,0),
            2 => (0,1),
            3 => (1,1),
            4 => (2,1),
            5 => (2,2),
            6 => (3,2),
            _ => { panic!("panels are 1..6"); }
        }
    };

    let vec : Vec<Point> = match dir {
        Direction::Right => (0..mul).map(|y| (mul*l0.0 + mul-1, mul*l0.1+y)).collect(),
        Direction::Down => (0..mul).map(|x| (mul*l0.0+x, mul*l0.1 + mul-1)).collect(),
        Direction::Left => (0..mul).map(|y| (mul*l0.0, mul*l0.1+y)).collect(),
        Direction::Up => (0..mul).map(|x| (mul*l0.0+x, mul*l0.1)).collect(),
    };

    if rev {
        vec.into_iter().rev().collect()
    } else {
        vec
    }
}

fn panel_jump(mul : usize, f : u8, t : u8, df : Direction, dt : Direction, rev : bool) -> Vec<((Point,Direction), (Point,Direction))> {
    dir2edge(mul, f, df, true).iter().zip(
        dir2edge(mul, t, -dt, rev).iter().rev()
    ).flat_map(|(p1, p2)| [
       ((*p1,df), (*p2,dt)),
       ((*p2,-dt), (*p1,-df))
    ]).collect()
}

fn solve22b(lines : Vec<String>) -> usize {
    let (spaces, walls, start, instrs) = parse(&lines);
    let mut jump_table : HashMap<(Point, Direction), (Point, Direction)> = HashMap::new();

    // We now assume the shape up to a multiplier that is 4 for
    // the example and 50 for the main puzzle

    /*          +--^--+--^--+
     *          |  6  |  6  |
     *          <4 1  |  2 5>
     *          |     |  3  |
     *          +-----+--V--+
     *          |     |
     *          <4 3 2>
     *          |     |
     *    +--^--+-----+
     *    |  3  |     |
     *    <1 4  |  5 2>
     *    |     |  6  |
     *    +-----+--V--+
     *    |     |
     *    <1 6 5>
     *    |  2  |
     *    +--V--+
     *
     */

    let mul = 50;
    if mul == 50 {
        jump_table.extend(panel_jump(mul, 1, 6, Direction::Up   , Direction::Right, false));
        jump_table.extend(panel_jump(mul, 1, 4, Direction::Left , Direction::Right, true));

        jump_table.extend(panel_jump(mul, 2, 6, Direction::Up   , Direction::Up   , false));
        jump_table.extend(panel_jump(mul, 2, 5, Direction::Right, Direction::Left , true ));
        jump_table.extend(panel_jump(mul, 2, 3, Direction::Down , Direction::Left , false));

        jump_table.extend(panel_jump(mul, 3, 4, Direction::Left , Direction::Down , false));

        jump_table.extend(panel_jump(mul, 5, 6, Direction::Down , Direction::Left , false));
    } else if mul == 4 {
        jump_table.extend(panel_jump(mul, 1, 6, Direction::Right, Direction::Left , true ));
        jump_table.extend(panel_jump(mul, 1, 3, Direction::Left , Direction::Down , false));
        jump_table.extend(panel_jump(mul, 1, 2, Direction::Up   , Direction::Down , true ));

        jump_table.extend(panel_jump(mul, 2, 5, Direction::Down , Direction::Up   , true ));
        jump_table.extend(panel_jump(mul, 2, 6, Direction::Left , Direction::Up   , false));

        jump_table.extend(panel_jump(mul, 3, 5, Direction::Down , Direction::Right, false));

        jump_table.extend(panel_jump(mul, 4, 6, Direction::Right, Direction::Down , true ));
    }

    solve(&spaces, &walls, &start, &instrs, &jump_table)
}

fn main() {
    let path = Path::new("input22.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve22a(lines.clone()));
    println!("{}", solve22b(lines.clone()));
}
