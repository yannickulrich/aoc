use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn solve1a (lines : Vec<i32> ) -> i32 {
    lines.split(
        |&l| l == 0
    ).map(
        |x| x.iter().sum::<i32>()
    ).max().unwrap()
}

fn solve1b (lines : Vec<i32> ) -> i32 {
    let mut tallies : Vec<i32> = lines.split(
        |&l| l == 0
    ).map(
        |x| x.iter().sum::<i32>()
    ).collect();
    tallies.sort();
    return tallies.iter().rev().take(3).sum();
}


fn main() {
    let path = Path::new("input01.txt");
    let lines : Vec<i32> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| match l.expect("Can't parse").parse::<i32>() {
                Ok(n) => n,
                _ => 0
            }
        ).collect(),
    };

    println!("{}", solve1a(lines.clone()));
    println!("{}", solve1b(lines));
}
