#![allow(dead_code)]

use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::collections::HashSet;
use std::collections::VecDeque;
use std::cmp::Ordering;
use std::ops::{Add,Sub,Mul,Div};

type Point = (usize,usize);
type Point3D = (usize,usize,usize);
type Points3D = HashSet<Point3D>;
type Map = Vec<(Direction, Point)>;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Ord, PartialOrd)]
enum Direction {
    Right, Down, Left, Up
}

fn parse(lines : &Vec<String>) -> (Map, usize, usize) {
    let mut parsed : Vec<(Direction, Point)>
        = lines.iter().enumerate().skip(1).take(lines.len()-2).flat_map(
            |(y, line)|
                line.chars().enumerate().skip(1).take(line.len()-2).filter_map(
                    move |(x, c)| match c {
                        '>' => Some((Direction::Right, (x,y))),
                        '<' => Some((Direction::Left , (x,y))),
                        '^' => Some((Direction::Up   , (x,y))),
                        'v' => Some((Direction::Down , (x,y))),
                        _ => None
                    }
                )
        ).collect();
    parsed.sort_unstable_by_key(|(d,_)| *d);

    (parsed, lines[0].len(), lines.len())
}

fn weather_forecast(map : &mut Map, w : usize, h : usize, n : usize) -> Points3D {
    // We need at most map.len()*n entries but quite often
    // storms will collide.
    let mut ans : Points3D = HashSet::with_capacity(map.len()*n / 2);
    for i in 0..n {
        for (d, (x,y)) in map.iter_mut() {
            ans.insert((*x,*y,i));

            match d {
                // We want (x+1)%w but w include the walls and x is
                // unity-indexed. Meaning we need to write
                //    ((x-1)+1) % (w-2) + 1
                Direction::Right => *x = (*x % (w-2)) + 1,
                Direction::Down  => *y = (*y % (h-2)) + 1,
                // We want (x-1)%w but w include the walls, x is
                // unity-indexed, and Rust's % is weird. Meaning
                // we need to write
                //    ((w-2)+(x-1)-1) % (w-2) + 1
                Direction::Left  => *x = (w+*x-4) % (w-2) + 1,
                Direction::Up    => *y = (h+*y-4) % (h-2) + 1
            }
        }
    }
    ans
}

fn bfs(map : &Points3D, w : usize, h : usize, start : Point3D, goal : Point, modulo : usize) -> usize {
    let mut seen : Points3D = HashSet::new();
    let mut queue : VecDeque<Point3D> = VecDeque::from([start]);

    while let Some((x0,y0, l)) = queue.pop_front() {
        if (x0, y0) == goal {
            return l;
        }

        for (x, y) in [(x0,y0+1),(x0,y0-1),(x0+1,y0),(x0-1,y0),(x0,y0)] {
            if seen.contains(&(x,y,l+1)) { continue };
            if !(x==start.0 && y==start.1) && !((x,y) == goal) {
                if x < 1 || x > w-2 { continue };
                if y < 1 || y > h-2 { continue };
            }
            if !map.contains(&(x,y,(l+1)%modulo)) {
                queue.push_back((x,y,l+1));
                seen.insert((x,y,l+1));
            }
        }
    }
    usize::MAX
}

fn gcd<T>(a : T, b : T) -> T
where
    T : Ord + Add + Sub<Output=T> + Copy
{
    match a.cmp(&b) {
        Ordering::Equal => a,
        Ordering::Less => gcd(a, b-a),
        Ordering::Greater => gcd(a-b, b)
    }
}

fn lcm<T>(a : T, b : T) -> T
where
    T : Ord + Mul<Output=T> + Div<Output=T> + Add + Sub<Output=T> + Copy
{
    a * b / gcd(a, b)
}

fn solve24a(lines : Vec<String>) -> usize {
    let (mut map, w, h) : (Map, usize, usize) = parse(&lines);
    let modulo = lcm(w-2,h-2);
    let map3d = weather_forecast(&mut map, w, h, modulo);
    bfs(&map3d, w, h, (1,0,0), (w-2,h-1), modulo)
}

fn solve24b(lines : Vec<String>) -> usize {
    let (mut map, w, h) : (Map, usize, usize) = parse(&lines);
    let modulo = lcm(w-2,h-2);
    let map3d = weather_forecast(&mut map, w, h, modulo);
    let t1 = bfs(&map3d, w, h, (1,0,0), (w-2,h-1), modulo);
    let t2 = bfs(&map3d, w, h, (w-2,h-1,t1), (1,0), modulo);
    bfs(&map3d, w, h, (1,0,t2), (w-2,h-1), modulo)
}

/////////////////////////////////////////////////////////////////////////
#[allow(non_snake_case,non_upper_case_globals)]
pub mod Cell {
    pub const None  : u8 = 0;
    pub const Right : u8 = 1;
    pub const Down  : u8 = 2;
    pub const Left  : u8 = 4;
    pub const Up    : u8 = 8;
    pub const Elf   : u8 = 32;
    pub const Wall  : u8 = 16;
}

struct MapCA {
    w : usize, h : usize,
    dat : Vec<u8>,
}

impl MapCA {
    fn get(&self, x : usize, y : usize) -> Option<&u8> {
        if x >= self.w { return None; }
        if y >= self.h { return None; }
        Some(&self.dat[x + y*self.w])
    }
    fn insert(&mut self, (x,y) : (usize, usize), value : u8) {
        *self.dat.get_mut(x + y*self.w).unwrap() = value;
    }
    fn for_each<F>(&self, func : F) -> MapCA
        where F : Fn(Point, u8) -> u8
    {
        MapCA {
            w : self.w, h : self.h,
            dat : self.dat.iter().enumerate().map(
                |(n, c)| func((n%self.w, n/self.w), *c)
            ).collect()
        }
    }
    fn apply_each<F>(&mut self, func : F)
        where F : Fn(u8) -> u8
    {
        for i in self.dat.iter_mut() {
            *i = func(*i);
        }
    }
}

fn parse_ca(lines : &Vec<String>) -> MapCA {
    MapCA {
        w : lines[0].len(), h : lines.len(),
        dat : lines.iter().flat_map(
            |line|
                line.chars().map(
                    |c| match c {
                        '#' => Cell::Wall ,
                        '>' => Cell::Right,
                        '<' => Cell::Left ,
                        '^' => Cell::Up   ,
                        'v' => Cell::Down ,
                        _   => Cell::None ,
                    }
                )
        ).collect()
    }
}

fn evolve_ca(map : &MapCA) -> MapCA {
    let w = map.w;
    let h = map.h;
    map.for_each(
        |(x,y), c| {
            let ans = (
                // move storms
                (*map.get((((x-1)+1) % (w-2)) + 1, y).unwrap_or(&Cell::None) & Cell::Left)
              | (*map.get(x, (((y-1)+1) % (h-2)) + 1).unwrap_or(&Cell::None) & Cell::Up)
              | (*map.get(((w-2)+(x-1)-1) % (w-2) + 1, y).unwrap_or(&Cell::None) & Cell::Right)
              | (*map.get(x, ((h-2)+(y-1)-1) % (h-2) + 1).unwrap_or(&Cell::None) & Cell::Down)
            ) | (
                // move elf
                (*map.get(x+1, y).unwrap_or(&Cell::None) & Cell::Elf)
              | (*map.get(x, y+1).unwrap_or(&Cell::None) & Cell::Elf)
              | (*map.get(x-1, y).unwrap_or(&Cell::None) & Cell::Elf)
              | (*map.get(x, y-1).unwrap_or(&Cell::None) & Cell::Elf)
            ) | (
                // stay put
                c & Cell::Elf
            );
            if ans > Cell::Elf {
                // there's an elf here and also a strom
                ans ^ Cell::Elf
            } else {
                ans
            }
        }
    )
}

fn draw(map : &MapCA) {
    for y in 0..map.h {
        for x in 0..map.w {
            match *map.get(x,y).unwrap() {
                Cell::None  => { print!(".") },
                Cell::Right => { print!(">") },
                Cell::Down  => { print!("v") },
                Cell::Left  => { print!("<") },
                Cell::Up    => { print!("^") },
                Cell::Elf   => { print!("E") },
                Cell::Wall  => { print!("#") }
                _           => { print!("*") }
            }
        }
        print!("\n");
    }
}

fn solve_ca(map : &mut MapCA, start : Point, goal : Point) -> usize {
    map.apply_each(
        |c| c & (Cell::Right | Cell::Down | Cell::Left | Cell::Up | Cell::Wall)
    );

    map.insert(start, Cell::Elf);
    map.insert(goal, Cell::None);

    for i in 0.. {
        *map = evolve_ca(map);
        if *map.get(goal.0, goal.1).unwrap() == Cell::Elf {
            return i + 1;
        }
    }
    usize::MAX
}

fn solve24a_ca(lines : Vec<String>) -> usize {
    let mut map : MapCA = parse_ca(&lines);
    let w = map.w; let h = map.h;
    solve_ca(&mut map, (1,0), (w-2,h-1)) + 1
}

fn solve24b_ca(lines : Vec<String>) -> usize {
    let mut map : MapCA = parse_ca(&lines);
    let w = map.w; let h = map.h;
    let t0 = solve_ca(&mut map, (1,0), (w-2,h-1));
    let t1 = solve_ca(&mut map, (w-2,h-1), (1,0));
    let t2 = solve_ca(&mut map, (1,0), (w-2,h-1));
    t0 + t1 + t2
}

fn main() {
    let path = Path::new("input24.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve24a_ca(lines.clone()));
    println!("{}", solve24b_ca(lines.clone()));
}
