use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::cmp::Ordering;

fn follow1(head : i32, tail : i32) -> i32 {
    let d = (head - tail).abs();
    if d < 2 { return tail; }

    let s = (head-tail)/d;
    return tail + (d-1) * s;
}

fn test_follow1() {
    assert_eq!(follow1(2, 1), 1);
    assert_eq!(follow1(1, 2), 2);
    assert_eq!(follow1(1, 1), 1);

    assert_eq!(follow1(3, 1), 2);
    assert_eq!(follow1(1, 3), 2);
}

fn follow((hx,hy) : (i32, i32), (tx,ty) : (i32,i32)) -> (i32, i32) {
    let dx = (hx-tx).abs();
    let dy = (hy-ty).abs();
    if dx <= 1 && dy <= 1 {
        return (tx,ty);
    }

    match dx.cmp(&dy) {
        Ordering::Equal  => (follow1(hx,tx), follow1(hy,ty)),
        Ordering::Less   => (hx, follow1(hy,ty)),
        Ordering::Greater=> (follow1(hx,tx), hy)
    }
}

fn test_follow() {
    assert_eq!(follow((2,1), (1,1)), (1,1));
    assert_eq!(follow((1,1), (2,2)), (2,2));
    assert_eq!(follow((1,1), (1,1)), (1,1));

    assert_eq!(follow((3,1), (1,1)), (2,1));
    assert_eq!(follow((1,3), (1,1)), (1,2));

    assert_eq!(follow((2,1), (1,3)), (2,2));
    assert_eq!(follow((3,2), (1,3)), (2,2));

    // Bonus tests!
    assert_eq!(follow((1,1), (4,4)), (2,2));

}

fn parse1<'a>(tup : (i32,i32), d : &'a str) -> Result<std::iter::Take<std::iter::Repeat<(i32, i32)>>, &'a str> {
    match d.parse::<usize>() {
        Ok(n) => Ok(std::iter::repeat(tup).take(n)),
        _ => Err(d.clone())
    }
}

fn parse(lines : Vec<String>) -> Vec<(i32,i32)> {

    lines.iter().flat_map(
        |l| match l.split_once(' ') {
            Some(("R", d)) => parse1(( 1, 0),d).expect(""),
            Some(("L", d)) => parse1((-1, 0),d).expect(""),
            Some(("U", d)) => parse1(( 0, 1),d).expect(""),
            Some(("D", d)) => parse1(( 0,-1),d).expect(""),
            _ => panic!("parse")
        }
    ).collect()
}

fn solve9a(lines : Vec<String>) -> usize {
    let mut seen : Vec<i32> = vec![];
    let mut head = (0,0);
    let mut tail = (0,0);
    for (dx,dy) in parse(lines) {
        head.0 += dx; head.1 += dy;
        tail = follow(head, tail);
        seen.push(tail.0 * 10000 + tail.1);
    }

    seen.sort_unstable();
    seen.dedup();
    return seen.len();
}

fn solve9b(lines : Vec<String>) -> usize {
    let mut seen : Vec<i32> = vec![];

    let mut rope : [(i32,i32); 10] = [(0,0); 10];
    for (dx,dy) in parse(lines) {
        rope[0].0 += dx; rope[0].1 += dy;
        for i in 1..10 {
            rope[i] = follow(rope[i-1], rope[i]);
        }
        seen.push(rope[9].0 * 10000 + rope[9].1);
    }

    seen.sort_unstable();
    seen.dedup();
    return seen.len();
}

fn main() {
    test_follow1();
    test_follow();

    let path = Path::new("input09.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve9a(lines.clone()));
    println!("{}", solve9b(lines.clone()));
}
