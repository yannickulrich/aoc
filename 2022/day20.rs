use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

#[derive(Debug,Clone,Copy)]
struct DLList {
    index : usize,
    value : i64,
    next : usize,
    prev : usize
}

fn parse(lines : &Vec<String>) -> Vec<DLList> {
    let nn = lines.len() as i32;

    lines.iter().enumerate().map(
        |(n,l)| DLList {
            index : n,
            value : l.parse::<i64>().expect(""),
            next : (((n as i32) + 1 + nn) % nn) as usize,
            prev : (((n as i32) - 1 + nn) % nn) as usize
        }
    ).collect()
}

fn linearise(state : &Vec<DLList>) -> Vec<i64> {
    let mut ans = vec![];

    let mut curr : &DLList = &state[0];
    while ans.len() < state.len() {
        ans.push(curr.value);
        curr = &state[curr.next];
    }
    ans
}

fn mixing(state : &mut Vec<DLList>) {
    let len = state.len() as i64;
    let mod_shift = (
        state.iter().map(|l| l.value).max().unwrap() / len + 1
    ) * (len-1);

    for n in 0..len {

        let curr = state[n as usize];
        if curr.value == 0 { continue; }

        // Delete current
        state[curr.next].prev = state[curr.prev].index;
        state[curr.prev].next = state[curr.next].index;

        let mut prev = curr;
        for _ in 0..(curr.value + mod_shift)%(len-1) {
            prev = state[prev.next];
        }
        let target_prev = prev.index;

        // and splice back in
        let target_next = state[target_prev].next;
        state[curr.index].next = state[target_prev].next;
        state[curr.index].prev = state[target_prev].index;
        state[target_prev].next = curr.index;
        state[target_next].prev = curr.index;

    }
}

fn get_ans(state : &Vec<DLList>) -> i64 {
    let lin = linearise(&state);
    lin.iter().cycle().skip_while(
        |x| x != &&0
    ).step_by(1000).skip(1).take(3).sum()
}

fn solve20a(lines : Vec<String>) -> i64 {
    let mut state = parse(&lines);

    mixing(&mut state);
    get_ans(&state)
}

fn solve20b(lines : Vec<String>) -> i64 {
    let mut state : Vec<DLList> = parse(&lines).iter().map(
        |el| DLList {
            index : el.index,
            value : 811589153 * el.value,
            next : el.next,
            prev : el.prev
        }
    ).collect();

    for _ in 0..10 {
        mixing(&mut state);
    }
    get_ans(&state)
}

fn main() {
    let path = Path::new("input20.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve20a(lines.clone()));
    println!("{}", solve20b(lines.clone()));
}
