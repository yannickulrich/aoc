use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::collections::HashSet;

fn parse1(line : String) -> Result<((i32,i32), (i32,i32)), String> {
    match line.split(&['=', ',', ':'][..]).collect::<Vec<&str>>()[..] {
        [_, x0, _, y0, _, x1, _, y1] => Ok((
                (x0.parse().expect(""),y0.parse().expect("")),
                (x1.parse().expect(""),y1.parse().expect(""))
        )),
        _ => Err(line)
    }
}

fn parse(lines : Vec<String>) -> (
    Vec<((i32,i32),i32)>, Vec<(i32,i32)>, i32, i32
) {
    let mut min_x =  1000000;
    let mut max_x = -1000000;
    let mut sensors : Vec<((i32,i32),i32)> = vec![];
    let mut beacons : Vec<(i32,i32)> = vec![];
    for i in lines {
        let ((x0,y0),(x1,y1)) = parse1(i).expect("");
        sensors.push(((x0,y0), (x0-x1).abs() + (y0-y1).abs()));
        beacons.push((x1,y1));

        if x0 < x1 {
            if x0 - (x1-x0) < min_x { min_x = x0 - (x1-x0); }
            if x1 > max_x { max_x = x1; }
        } else {
            if x1 < min_x { min_x = x1; }
            if x0 + (x0-x1) > max_x { max_x = x0 + (x0-x1); }
        }
    }
    return (sensors, beacons, min_x, max_x);
}

fn valid(sensors : &Vec<((i32,i32),i32)>, x : i32, y : i32) -> bool {
    sensors.iter().any(
        |((x0,y0),d)|
            (x0-x).abs() + (y0-y).abs() <= *d
    )
}

fn solve15a(lines : Vec<String>) -> i32 {
    let (sensors, beacons, min_x, max_x) = parse(lines);

    let mut n = 0;
    let y = 2000000;
    for x in min_x..=max_x {
        if valid(&sensors, x, y) && !beacons.contains(&(x,y)) { n += 1 };
    }

    n
}

fn avalues(((x0, y0), d) : ((i32,i32),i32)) -> (i32,i32,i32,i32) {
    // The exclusion polygon is bounded by these four linear equations
    //    y = a.0 + x, y = a.1 - x, y = a.2 + x, y = a.3 - x
    // The ai can be fixed by setting x = x0 +- d. We have
    (
        -d - x0 + y0,
         d - x0 + y0,
         d + x0 + y0,
        -d + x0 + y0
    )
}


fn solve15b(lines : Vec<String>) -> i64 {
    // Candidate points are d_i+1 from s_i for some i. Let us take two
    // of these, w.l.o.g. 1 and 2. Then the solution is at r s.t.
    //       |r-s1| = d1+1 and |r-s2| = d2+1.
    // Using the lines defined above, for a valid solution line we
    // have to have either of the following
    //  a1.0-1 = a2.1+1 (lower right & top left)
    //  a1.1+1 = a2.0-1 (top left & lower right)
    //  a1.2+1 = a2.3-1 (top right & lower left)
    //  a1.3-1 = a2.2+1 (lower left & top right)
    // These lines of allowed solutions can be seen below
    // -2 ..........#..........................
    // -1 .........###.........................
    //  0 ....S...#####........................
    //  1 .......#######........S..............
    //  2 ......#########S.....................
    //  3 .....###########SB...................
    //  4 ....#############....................
    //  5 ...###############..../..............
    //  6 ..#################../1..............
    //  7 .#########S#######S#/111.............
    //  8 ..#################/11111............
    //  9 ...###############*1111111...........
    // 10 ...\B############/111111111..........
    // 11 ...2\###########*11111111111.........
    // 12 ..222\#########/1111111111111........
    // 13 .22222X#######/111111111111111.......
    // 14 ..222/.\#####X11111111S11111111......
    // 15 ...2/....###/.\111111111111111.......
    // 16 .../......#/B..\1111111111111........
    // 17 ........../.....\1111111111B.........
    // 18 ........./.......\111111111..........
    // 19 ..................\1111111...........
    // 20 ............S......\11111............
    // 21 ....................\111.............
    // 22 .....................\1B.............
    //
    // However, we can go further. A valid solution also needs to
    // be at the intersection of with a down-sloping line because it
    // wouldn't be unique otherwise. The * isn't valid, the X might be.

    let (sensors, _, _, _) = parse(lines);
    let amat : Vec<(i32,i32,i32,i32)> = sensors.iter().map(|s| avalues(*s)).collect();

    let mut up : HashSet<i32> = HashSet::new();
    up.extend(amat.iter().map(|a| a.0-1));
    up.extend(amat.iter().map(|a| a.2+1));
    let mut down : HashSet<i32> = HashSet::new();
    down.extend(amat.iter().map(|a| a.1+1));
    down.extend(amat.iter().map(|a| a.3-1));

    for u in up.iter() {
        for d in down.iter() {
            let x0 = (d-u)/2;
            let y0 = (d+u)/2;

            if x0 < 0 || x0 > 4000000 { continue; }
            if y0 < 0 || y0 > 4000000 { continue; }
            if !valid(&sensors, x0, y0) {
                return (x0 as i64) * 4_000_000 + (y0 as i64);
            }
        }
    }

    return 0;
}

fn main() {
    let path = Path::new("input15.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve15a(lines.clone()));
    println!("{}", solve15b(lines.clone()));
}
