use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn rsplit_once<'a>(s : &'a Option<&String>, delim : &str) -> Option<(&'a str, &'a str)> {
    match s {
        Some(line) => line.rsplit_once(delim),
        None => None
    }
}

fn runner(it : &mut std::slice::Iter<String>) -> (Vec<i32>, i32) {
    let mut dirs : Vec<i32> = Vec::new();
    let mut tally : i32 = 0;

    loop {
        match rsplit_once(&it.next(), " ") {
            Some(("dir", _)) => {},
            Some(("$", "ls")) => {},
            Some(("$ cd", "/")) => {},
            Some(("$ cd", "..")) => {
                dirs.push(tally);
                return (dirs, tally)
            },
            Some(("$ cd", _dir)) => {
                let (mut sub_dirs, sub_tally) = runner(it);
                dirs.append(&mut sub_dirs);
                tally += sub_tally;
            },
            Some((size, _)) => {
                tally += size.parse::<i32>().expect("Size is not int");
            },
            None => {
                return (dirs, tally)
            }
        }
    }
}


fn solve7a(lines : Vec<String>) -> i32 {
    let mut it = lines.iter();
    let (dirs, _) = runner(&mut it);

    dirs.iter().filter(
        |j| j < &&100000
    ).sum()
}

fn solve7b(lines : Vec<String>) -> i32 {
    let mut it = lines.iter();
    let (dirs, tally) = runner(&mut it);

    let to_delete = tally - 40000000;

    *dirs.iter().filter(
        |j| j > &&to_delete
    ).min().unwrap_or(&0)
}

fn main() {
    let path = Path::new("input07.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve7a(lines.clone()));
    println!("{}", solve7b(lines.clone()));
}

