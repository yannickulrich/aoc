use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::collections::VecDeque;
use std::collections::HashSet;
use std::collections::HashMap;
use std::iter::FromIterator;

type Point = (i32, i32);
type Points = HashSet<Point>;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum Direction {
    North, South, West, East
}

fn parse(lines : Vec<String>) -> Points {
    HashSet::from_iter(lines.iter().enumerate().flat_map(
        |(y,line)| line.chars().enumerate().filter_map(
            move |(x,c)| if c == '#' { Some((x as i32, y as i32)) } else { None }
        )
    ))
}

fn execute(elves : &mut Points, order : &VecDeque<Direction>) -> bool {
    let mut instr : HashMap<Point, Option<Point>> = HashMap::new();

    for (x,y) in elves.iter() {
        let e = [
            !elves.contains(&(*x  ,*y-1)), // N   0
            !elves.contains(&(*x+1,*y-1)), // NE  1
            !elves.contains(&(*x+1,*y  )), // E   2
            !elves.contains(&(*x+1,*y+1)), // SE  3
            !elves.contains(&(*x  ,*y+1)), // S   4
            !elves.contains(&(*x-1,*y+1)), // SW  5
            !elves.contains(&(*x-1,*y  )), // W   6
            !elves.contains(&(*x-1,*y-1))  // NW  7
        ];

        if e.iter().all(|&i| i) { continue; }

        let dir = order.iter().find_map(
            |d| match *d {
                Direction::North => if e[7] && e[0] && e[1] { Some((*x  , *y-1)) } else { None },
                Direction::South => if e[3] && e[4] && e[5] { Some((*x  , *y+1)) } else { None },
                Direction::West  => if e[5] && e[6] && e[7] { Some((*x-1, *y  )) } else { None },
                Direction::East  => if e[1] && e[2] && e[3] { Some((*x+1, *y  )) } else { None }
            }
        );
        if let Some((nx, ny)) = dir {
            if let Some(lst) = instr.get_mut(&(nx,ny)) {
                *lst = None;
            } else {
                instr.insert((nx,ny), Some((*x,*y)));
            }
        }
    }

    let instr : HashMap<Point, Point>  = instr.iter().filter_map(
        |(&to,&from)|
            if let Some(from) = from {
                Some((from, to))
            } else { None }
    ).collect();

    elves.retain(|l| !instr.contains_key(l));
    elves.extend(instr.iter().map(|(_, to)| to));

    instr.is_empty()
}

fn solve23a(lines : Vec<String>) -> i32 {
    let mut elves = parse(lines);

    let mut order = VecDeque::from([
        Direction::North,
        Direction::South,
        Direction::West,
        Direction::East
    ]);

    for _ in 0..10 {
        execute(&mut elves, &order);

        order.rotate_left(1);
    }

    let width = elves.iter().max_by_key(|(x,_y)| x).unwrap().0
              - elves.iter().min_by_key(|(x,_y)| x).unwrap().0 + 1;
    let height= elves.iter().max_by_key(|(_x,y)| y).unwrap().1
              - elves.iter().min_by_key(|(_x,y)| y).unwrap().1 + 1;

    width*height - (elves.len() as i32)
}

fn solve23b(lines : Vec<String>) -> usize {
    let mut elves = parse(lines);

    let mut order = VecDeque::from([
        Direction::North,
        Direction::South,
        Direction::West,
        Direction::East
    ]);

    for n in 0..940 {
        if execute(&mut elves, &order) { return n+1; }

        order.rotate_left(1);
    }
    return 0;
}

fn main() {
    let path = Path::new("input23.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve23a(lines.clone()));
    println!("{}", solve23b(lines.clone()));
}
