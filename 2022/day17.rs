use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::collections::HashMap;

struct MyHashSet {
    hs : HashMap<i64,[bool; 7]>
}

impl MyHashSet {
    fn extend(&mut self, pieces : &Vec<(i64,i64)>) {
        for (x,y) in pieces {
            match self.hs.get_mut(&y) {
                Some(p) => p[*x as usize] = true,
                None => {
                    let mut arr = [false;7];
                    arr[*x as usize] = true;
                    self.hs.insert(*y, arr);
                }
            }
        }
    }

    fn contains(&self, (x, y) : &(i64,i64)) -> bool {
        match self.hs.get(&y) {
            Some(p) => p[*x as usize],
            None => false
        }
    }

    fn row_full(&self, y : i64) -> bool {
        match self.hs.get(&y) {
            Some(p) => p.iter().all(|x| *x),
            None => false
        }
    }
}

fn get_piece(n : i64, y : i64) -> Vec<(i64,i64)> {
    match n % 5 {
        //      0123456
        // y+0: ..####.
        0 => vec![(2,y), (3,y), (4,y), (5,y)],
        //      0123456
        // y+2: ...#...
        // y+1: ..###..
        // y+0: ...#...
        1 => vec![(3,y), (2,y+1), (3,y+1), (4,y+1), (3,y+2)],
        //      0123456
        // y+2: ....#..
        // y+1: ....#..
        // y+0: ..###..
        2 => vec![(2,y), (3,y), (4,y), (4,y+1), (4,y+2)],
        //      0123456
        // y+3: ..#....
        // y+2: ..#....
        // y+1: ..#....
        // y+0: ..#....
        3 => vec![(2,y), (2,y+1), (2,y+2), (2,y+3)],
        //      0123456
        // y+1: ..##...
        // y+0: ..##...
        4 => vec![(2,y), (3,y), (2,y+1), (3,y+1)],
        _ => panic!("maths broke")
    }
}

fn move_hor(fixed : &MyHashSet, piece : &Vec<(i64,i64)>, dx : i64) -> Vec<(i64,i64)> {
    let mut collision = false;
    let ans : Vec<(i64,i64)> = piece.iter().filter_map(
        |(x,y)| {
            if x+dx < 0 { collision = true; None }
            else if x+dx > 6 { collision = true; None }
            else if fixed.contains(&(*x+dx,*y)) { collision = true; None }
            else { Some((x+dx,*y)) }
        }
    ).collect();
    if collision {
        piece.clone()
    } else {
        ans
    }
}

fn move_ver(fixed : &MyHashSet, piece : &Vec<(i64,i64)>) -> Option<Vec<(i64,i64)>> {
    let mut collision = false;
    let ans : Vec<(i64,i64)> = piece.iter().filter_map(
        |(x,y)| {
            if fixed.contains(&(*x,*y-1)) { collision = true; None }
            else { Some((*x,*y-1)) }
        }
    ).collect();
    if collision {
        None
    } else {
        Some(ans)
    }
}

fn parse(line : String) -> Vec<i64> {
    line.chars().map(
        |c| match c {
            '>' =>  1,
            '<' => -1,
            _ => panic!("input error")
        }
    ).collect()
}

fn solve(lines : &Vec<String>, nit : i64) -> i64 {
    let instrl = lines[0].len() as i64;
    let instrs = parse(lines[0].clone());
    let mut instrs = instrs.iter().cycle();
    let mut instrc = 0;

    let mut fixed : MyHashSet = MyHashSet { hs : HashMap::new() };
    fixed.hs.insert(0, [true; 7]);

    let mut tallest : i64 = 0;
    let mut tallest_offset : i64 = 0;

    let mut cycle : Option<(i64,i64,i64)> = None;

    let mut n = 0;
    while n < nit {
        if n > 0 && tallest_offset == 0 && fixed.row_full(tallest) {
            match cycle {
                None => { cycle = Some((tallest, n, instrc)); },
                Some((tt, nn, ii)) => {
                    fixed = MyHashSet { hs : HashMap::new() };
                    fixed.hs.insert(0, [true; 7]);

                    assert_eq!((instrc - ii) % instrl, 0);

                    let ff_rounds = (nit-n)/(n-nn);
                    let ff_tallest = (ff_rounds as i64) * (tallest-tt);

                    n += ff_rounds * (n-nn);
                    tallest_offset = ff_tallest + tallest;
                    tallest = 0;
                    continue;
                }
            }
        }

        let mut piece = get_piece(n, tallest + 4);
        while let Some(instr) = instrs.next() {
            instrc += 1;
            piece = move_hor(&fixed, &piece, *instr);
            match move_ver(&fixed, &piece) {
                Some(p) => {piece = p},
                None => break
            }
        }
        let new_tallest = piece.iter().max_by_key(|(_,y)| y).unwrap().1;
        if new_tallest > tallest { tallest = new_tallest }
        fixed.extend(&piece);

        n+=1;
    }
    tallest + tallest_offset
}

fn solve17a(lines : Vec<String>) -> i64 {
    solve(&lines, 2022)
}

fn solve17b(lines : Vec<String>) -> i64 {
    solve(&lines, 1000000000000)
}

fn main() {
    let path = Path::new("input17.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve17a(lines.clone()));
    println!("{}", solve17b(lines.clone()));
}
