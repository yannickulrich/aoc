use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

struct Monkey {
    ninspect : usize,
    items : Vec<u64>,
    modulo : u64,
    true_target : usize,
    false_target : usize,
    func : Box<dyn Fn(u64)->u64>
}

fn parse(lines : Vec<String>) -> Vec<Monkey> {
    lines.chunks(7).map(|i| {
        let op = i[2].split_once("new = old ").unwrap().
                    1.split_once(" ");

        Monkey {
            ninspect: 0,
            items: i[1].split_once(": ").unwrap().1.split(", ").map(
                       |i| i.parse::<u64>().expect("")
                       ).collect::<Vec<u64>>(),
            modulo: i[3].split_once(" by ").unwrap().1
                        .parse::<u64>().expect(""),
            true_target: i[4].split_once(" monkey ").unwrap().1
                        .parse::<usize>().expect(""),
            false_target: i[5].split_once(" monkey ").unwrap().1
                        .parse::<usize>().expect(""),
            func: match op {
                Some(("*", "old")) => {
                    Box::new(move |a| a*a)
                },
                Some(("*", n)) => {
                    let nn = n.parse::<u64>().expect("");
                    Box::new(move |a| a*nn)
                },
                Some(("+", n)) => {
                    let nn = n.parse::<u64>().expect("");
                    Box::new(move |a| a+nn)
                },
                _ => panic!("")
            }
        }
    }).collect::<Vec<Monkey>>()
}

fn play1(state : &mut Vec<Monkey>, div : u64) {
    for i in 0..state.len() {
        state[i].ninspect += state[i].items.len();
        let true_target = state[i].true_target;
        let false_target = state[i].false_target;
        for item in state[i].items.clone() {
            let level =
                if div == 0 {
                    (state[i].func)(item) / 3
                } else {
                    (state[i].func)(item) % div
                };

            if level % state[i].modulo == 0 {
                state[true_target].items.push(level);
            } else {
                state[false_target].items.push(level);
            }
        }
        state[i].items.clear();
    }
}

fn get_ans(state : Vec<Monkey>) -> usize {
    let mut ninspects = state.iter().map(|l| l.ninspect).collect::<Vec<usize>>();
    ninspects.sort_unstable();
    return ninspects.pop().unwrap() * ninspects.pop().unwrap();
}

fn solve11a(lines : Vec<String>) -> usize {
    let mut state = parse(lines);
    for _ in 0..20 {
        play1(&mut state, 0);
    }
    get_ans(state)
}

fn solve11b(lines : Vec<String>) -> usize {
    let mut state = parse(lines);
    let modulo = state.iter().map(|l| l.modulo).product::<u64>();
    for _ in 0..10000 {
        play1(&mut state, modulo);
    }
    get_ans(state)
}

fn main() {
    let path = Path::new("input11.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve11a(lines.clone()));
    println!("{}", solve11b(lines.clone()));
}
