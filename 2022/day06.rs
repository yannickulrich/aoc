use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn duplicate_free<T: std::cmp::PartialEq>(buf : &Vec<T>) -> bool {
    buf.iter().enumerate().all(
        |(i,c)| buf.iter().enumerate().all(
            |(ii, cc)| {
                if i == ii { true }
                else if c == cc { false }
                else { true }
            }
        )
    )
}

fn solve6(line : String, n : usize) -> Result<usize, &'static str> {
    let vec : Vec<char> = line.clone().chars().collect();
    let ans = vec.windows(n).enumerate().find(
        |(_,c)| duplicate_free(&c.to_vec())
    );
    match ans {
        Some((s,_)) => Ok(n+s),
        None => Err("not found")
    }
}

fn solve6a(line : String) -> usize {
    solve6(line, 4).expect("")
}
fn solve6b(line : String) -> usize {
    solve6(line, 14).expect("")
}

fn main() {
    let path = Path::new("input06.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve6a(lines[0].clone()));
    println!("{}", solve6b(lines[0].clone()));
}
