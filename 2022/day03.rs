use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn intersect2((a,b) : (&str, &str)) -> Result<char, String> {
    a.chars().find(
        |&c| b.chars().any(|d| d == c)
    ).ok_or([a,b].join(" "))
}

fn intersect3((a,b,c) : (&str, &str,&str)) -> Result<char, String> {
    a.chars().find(
        |&x| b.chars().any(|d| d == x) && c.chars().any(|d| d == x)
    ).ok_or([a,b,c].join(" "))
}

fn score1(c : Result<char, String>) -> Result<i32, String> {
    match c {
        Ok(c @ 'A'..='Z') => Ok((c as i32 - 'A' as i32) + 27),
        Ok(c @ 'a'..='z') => Ok((c as i32 - 'a' as i32) + 1),
        Ok(c) => Err(String::from(c)),
        Err(x) => Err(String::from(x))
    }
}

fn solve3a(lines : Vec<String>) -> i32 {
    lines.iter().map(
        |line| score1(intersect2(line.split_at(line.len()/2)))
    ).map(|l| l.expect("Line failed")).sum()
}

fn solve3b(lines : Vec<String>) -> i32 {
    return lines.chunks(3).map(
        |l| match l {
            [a,b,c] => score1(intersect3((&a,&b,&c))),
            x => Err(x.join(" "))
        }
    ).map(|l| l.expect("Lines failed")).sum();
}

fn main() {
    let path = Path::new("input03.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve3a(lines.clone()));
    println!("{}", solve3b(lines.clone()));
}
