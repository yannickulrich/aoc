# Advent of Code 2021

The [Computing Club at IPPP Durham](https://notes.dmaitre.phyip3.dur.ac.uk/computing-club/) is doing [Advent of Code 2022](https://adventofcode.com/).
These are my solutions to the problems, trying to learn Rust.

Link: [Other people's solutions](https://gitlab.dur.scotgrid.ac.uk/computing-club/advent-of-code)

There will be some overlap esp. with [Ryan's solutions](https://gitlab.com/eidoom/aoc22).
