use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn parse1(line : &String) -> i64 {
    line.chars().fold(0,
        |acc, c| 5*acc + match c {
            '1' => 1,
            '2' => 2,
            '0' => 0,
            '-' => -1,
            '=' => -2,
            _ => {panic!("snafu");}
        }
    )
}

fn dec2fiv(n : i64) -> String {
    let mut nn = n;
    let mut ans = String::new();
    while nn > 0 {
        ans.push(match nn % 5 {
            0 => { '0' },
            1 => { nn -= 1 ; '1'},
            2 => { nn -= 2 ; '2'},
            3 => { nn += 2 ; '='},
            4 => { nn += 1 ; '-'},
            _ => {panic!("snafu");}
        });
        nn = nn / 5;
    }
    ans.chars().rev().collect::<String>()
}

fn solve25a(lines : Vec<String>) -> String {
    dec2fiv(lines.iter().map(parse1).sum())
}

fn main() {
    let path = Path::new("input25.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve25a(lines.clone()));
}
