use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn transpose<T>(v: Vec<Vec<T>>) -> Vec<Vec<T>>
where
    T: Clone,
{
    assert!(!v.is_empty());
    (0..v[0].len())
        .map(|i| v.iter().map(|inner| inner[i].clone()).collect::<Vec<T>>())
        .collect()
}

fn parse_line(op : String) -> Result<(usize, usize, usize), String> {
    let ans = op.split(" ").skip(1).step_by(2).map(
        |l| l.parse::<usize>()
    ).collect::<Vec<Result<usize,std::num::ParseIntError>>>();

    match ans[..] {
        [Ok(n), Ok(f), Ok(t)] => Ok((n,f-1,t-1)),
        _ => Err(op)
    }
}

fn parse_inp(lines : Vec<String>) -> (Vec<Vec<char>>, Vec<(usize,usize,usize)>) {
    let mut iter = lines.split(|l| l.len() == 0);

    let header = iter.next().unwrap();
    let state_pre = transpose(header.iter().rev().map(
        |l| l.chars().skip(1).step_by(4).collect::<Vec<char>>()
    ).collect::<Vec<Vec<char>>>());

    let state = state_pre.iter().map(
        |l|
            l.iter().filter(|&&x| x != ' ')
             .cloned().collect::<Vec<char>>()
    ).collect::<Vec<Vec<char>>>();

    let ops = iter.next().unwrap().iter().cloned().map(parse_line)
        .collect::<Result<Vec<(usize, usize, usize)>, String>>();

    (state, ops.expect(""))
}

fn get_ans(state : &Vec<Vec<char>>) -> String {
    state.iter().map(
        |l|
        if l.len() == 0 {' '} else { l[l.len()-1] }
    ).collect()
}

fn solve5a(lines : Vec<String>) -> String {
    let (mut state, ops) = parse_inp(lines);

    for (n,f,t) in ops {
        let i = state[f].len() - n;
        let d : Vec<char> = state[f].drain(i..).rev().collect();
        state[t].extend(d);
    }

    get_ans(&state)
}

fn solve5b(lines : Vec<String>) -> String {
    let (mut state, ops) = parse_inp(lines);

    for (n,f,t) in ops {
        let i = state[f].len() - n;
        let d : Vec<char> = state[f].drain(i..).collect();
        state[t].extend(d);
    }
    get_ans(&state)
}

fn main() {
    let path = Path::new("input05.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve5a(lines.clone()));
    println!("{}", solve5b(lines.clone()));
}
