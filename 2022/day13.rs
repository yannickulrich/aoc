use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::cmp::Ordering;

#[derive(Debug)]
enum List<T> {
    Node(Vec<List<T>>),
    Leaf(T),
}

impl<T> PartialOrd for List<T>
where
    T : Ord + Copy
{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(compare(self, other))
    }
}
impl<T> PartialEq for List<T>
where
    T : Ord + Copy
{
    fn eq(&self, other: &Self) -> bool {
        compare(self, other) == Ordering::Equal
    }
}

// right order <=> Less
fn compare<T>(t1 : &List<T>, t2 : &List<T>) -> Ordering
where
    T: Ord+Copy
{
    match (t1, t2) {
        (List::Leaf(l1), List::Leaf(l2)) => l1.cmp(&l2),
        (List::Leaf(l1), List::Node(_ )) => compare(&List::Node(vec![List::Leaf(*l1)]), t2),
        (List::Node(_ ), List::Leaf(l2)) => compare(t1, &List::Node(vec![List::Leaf(*l2)])),
        (List::Node(l1), List::Node(l2)) => {
            let mut i1 = l1.iter(); let mut i2 = l2.iter();
            loop {
                match (i1.next(), i2.next()) {
                    (None, Some(_)) => { return Ordering::Less; },
                    (Some(_), None) => { return Ordering::Greater; },
                    (None, None) => { return Ordering::Equal; },
                    (Some(ll1), Some(ll2)) => {
                        let ans = compare(ll1, ll2);
                        if ans != Ordering::Equal { return ans; }
                    }
                }
            }
        }
    }
}

fn parse1(it : &mut std::iter::Peekable<std::str::Chars>) -> List<u8> {
    let mut vec : Vec<List<u8>> = vec![];
    while let Some(p) = it.next() {
        match p {
            ',' => {},
            '[' => { vec.push(parse1(it)); },
            ']' => { return List::Node(vec); },
            n => {
                let nn : u8 = match it.peek() {
                    Some(']')|Some(',') => (n as u8) - ('0' as u8),
                    Some(_) =>
                        10 * ((n as u8) - ('0' as u8)) +
                        ((it.next().unwrap() as u8) - ('0' as u8))
                    ,
                    None => { panic!("syntax error"); }
                };
                vec.push(List::Leaf(nn));
            }
        }
    }
    List::Leaf(0)
}

fn parse_line(line : &String) -> List<u8> {
    let mut it1 = line.chars().peekable(); it1.next();
    parse1(&mut it1)
}

fn solve13a(lines : Vec<String>) -> usize {
    lines.chunks(3).enumerate().filter_map(
        |(i, pair)|
            if parse_line(&pair[0]) < parse_line(&pair[1]) { Some(i+1) }
            else { None }
    ).sum()
}

fn solve13b(lines : Vec<String>) -> usize {
    let mut list : Vec<(List<u8>, bool)> = lines.iter().filter_map(
        |l| if l.len() == 0 { None } else { Some((parse_line(l), false)) }
    ).collect();
    list.push((List::Node(vec![List::Node(vec![List::Leaf(2)])]),true));
    list.push((List::Node(vec![List::Node(vec![List::Leaf(6)])]),true));

    list.sort_by(|(a,_), (b,_)| a.partial_cmp(b).unwrap());

    let p1 = list.iter(). position(|(_, x)| *x).unwrap() + 1;
    let p2 = list.iter().rposition(|(_, x)| *x).unwrap() + 1;
    p1*p2
}

fn main() {
    let path = Path::new("input13.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve13a(lines.clone()));
    println!("{}", solve13b(lines.clone()));
}
