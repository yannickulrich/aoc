use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::collections::HashMap;
use std::collections::HashSet;
use std::collections::VecDeque;

type RoomList = HashMap<String, u64>;
type MapType = HashMap<u64,(i32,Vec<u64>)>;
type BFS = HashMap<u64, i32>;
type Graph = HashMap<u64,(i32,BFS)>;
#[derive(Eq,PartialEq,Hash)]
struct ValidPath {
    score : i32,
    path :u64
}

fn parse1(rooms : &RoomList, line : &String) -> (u64, i32, Vec<u64>) {
    let mut it = line.split(&['=', ',', ';', ' '][..]).skip(1);
    let name = *rooms.get(it.next().unwrap()).unwrap();
    it.next(); it.next(); it.next();
    let rate = it.next().unwrap().parse::<i32>().expect("");

    (
        name, rate,
        it.skip(5).step_by(2).map(
            |r| *rooms.get(r).unwrap()
        ).collect::<Vec<u64>>()
    )
}

fn bfs(map : &MapType, start : u64) -> BFS {
    let mut seen : HashMap<u64,u64> = HashMap::new();
    seen.insert(start.clone(), start.clone());
    let mut queue : VecDeque<(u64, usize)> = VecDeque::from([(start.clone(),0)]);

    while let Some((name, l)) = queue.pop_front() {
        for child in map[&name].1.iter() {
            if seen.contains_key(child) { continue; }

            seen.insert(*child, name);
            queue.push_back((*child, l+1));
        }
    }

    let mut ans : BFS = HashMap::new();

    for (room, (rate, _)) in map.iter() {
        if *rate == 0 { continue }
        if *room == start { continue }

        let mut my_room = room.clone();
        let mut n = 0;
        while my_room != start {
            my_room = seen[&my_room].clone();
            n += 1;
        }
        ans.insert(*room, n);
    }

    ans
}

fn parse(lines : Vec<String>) -> (Graph, u64) {
    let mut rooms : RoomList = HashMap::new();
    rooms.extend(lines.iter().enumerate().map(
        |(n,line)| (
            line.split(" ").skip(1).next().unwrap().to_string(),
            1 << (n as u64)
        )
    ));
    let start = *rooms.get("AA").unwrap();

    let mut map : MapType = HashMap::new();
    for line in lines.iter() {
        let (name, rate, children) = parse1(&rooms, line);
        map.insert(name, (rate, children));
    }

    let mut graph : Graph = HashMap::new();
    for (room, (rate, _)) in map.iter() {
        if *rate == 0 && *room != start { continue }

        graph.insert(*room, (*rate, bfs(&map, *room)));
    }
    (graph, start)
}

fn attempt_linear(graph : &Graph, t : i32, start : u64) -> HashSet<ValidPath> {
    let mut stack : Vec<(ValidPath, u64, i32)> = vec![(
        ValidPath { path : start, score : 0 },
        start, t
    )];
    let mut ans : HashSet<ValidPath> = HashSet::new();
    let mut best = 0;

    while let Some((path, current, time)) = stack.pop() {
        if time >= 30 {
            if path.score > best {
                best = path.score;
            }
            ans.insert(path);
            continue;
        }

        let mut del_max = 0;
        let (rate, exits) = &graph[&current];
        let mut buf : Vec<(ValidPath, u64, i32)> = exits.iter().filter_map(
            |(exit, dt)|
                if *exit & path.path == 0 {
                    Some((ValidPath {
                        path : path.path | current,
                        score : {
                            del_max += rate * (30-time);
                            path.score + rate * (30-time)
                        },
                    }, *exit, time+1+dt))
                } else { None }
        ).collect();

        // Calculate the total score under the assumption that
        // you can open all the closed valves by simultaneously
        // walking there and open them a minute later.
        let best_still_possible = path.score + del_max;
        // println!("{} {} {}", time, best_still_possible, best);

        if best_still_possible < best {
            continue;
        }
        stack.append(&mut buf);
    }

    ans
}

fn solve16a(lines : Vec<String>) -> i32 {
    let (graph, start) = parse(lines);
    let visited = attempt_linear(&graph, 0, start);
    visited.iter().map(|vp| vp.score).max().unwrap()
}

fn solve16b(lines : Vec<String>) -> i32 {
    let (graph, start) = parse(lines);
    let visited = attempt_linear(&graph, 4, start);
    let visited : Vec<&ValidPath> = visited.iter().collect();

    let mut tally = 0;
    for (n,vp1) in visited.iter().enumerate() {
        for vp2 in visited.iter().skip(n) {
            if vp1.score+vp2.score > tally && vp1.path & vp2.path == start {
                tally = vp1.score+vp2.score;
            }
        }
    }

    tally
}

fn main() {
    let path = Path::new("input16.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve16a(lines.clone()));
    println!("{}", solve16b(lines.clone()));
}
