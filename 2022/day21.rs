use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::collections::HashMap;
use std::iter::FromIterator;

#[derive(Debug, Clone, Copy)]
enum Element {
    Plus ( u32, u32 ),
    Minus ( u32, u32 ),
    Times ( u32, u32 ),
    Divide ( u32, u32 ),

    // A+B x
    Variable(f64, f64),
    Number(f64)
}

fn name2u32(name : &str) -> u32 {
    name.chars().enumerate().map(
        |(n,l)| (1<<(5*n)) * ((l as u32) - ('a' as u32))
    ).sum()
}

fn parse1(line : &String) -> (u32, Element) {
    match line.split_once(": ") {
        Some((name, value)) => {
            let vec = value.split_whitespace().collect::<Vec<&str>>();
            (
                name2u32(name),
                match vec[..] {
                    [n] => Element::Number(n.parse::<f64>().expect("")),
                    [a, "+", b] => Element::Plus(
                        name2u32(a), name2u32(b)
                    ),
                    [a, "-", b] => Element::Minus(
                        name2u32(a), name2u32(b)
                    ),
                    [a, "*", b] => Element::Times(
                        name2u32(a), name2u32(b)
                    ),
                    [a, "/", b] => Element::Divide(
                        name2u32(a), name2u32(b)
                    ),
                    _ => { panic!("parse error"); }
                }
            )
        },
        None => { panic!("parse error"); }
    }
}

fn parse(lines : &Vec<String>) -> HashMap<u32, Element> {
    HashMap::from_iter(lines.iter().map(parse1))
}

fn resolve1(inp : u32, state : &mut HashMap<u32, Element>) -> bool {
    match state[&inp] {
        Element::Number(_) => true,
        Element::Variable(_, _) => true,
        Element::Plus(a, b) => match (state[&a], state[&b]) {
            (Element::Number(a), Element::Number(b)) => {*state.get_mut(&inp).unwrap() = Element::Number(a+b); true},

            // aa+ab x + b
            (Element::Variable(aa, ab), Element::Number(b)) => {*state.get_mut(&inp).unwrap() = Element::Variable(aa+b,ab); true},
            (Element::Number(a), Element::Variable(ba, bb)) => {*state.get_mut(&inp).unwrap() = Element::Variable(ba+a,bb); true},
            _ => false
        },
        Element::Minus(a, b) => match (state[&a], state[&b]) {
            (Element::Number(a), Element::Number(b)) => {*state.get_mut(&inp).unwrap() = Element::Number(a-b); true},

            // aa+ab x - b
            (Element::Variable(aa, ab), Element::Number(b)) => {*state.get_mut(&inp).unwrap() = Element::Variable(aa-b,ab); true},
            // a - (ba+bb x)
            (Element::Number(a), Element::Variable(ba, bb)) => {*state.get_mut(&inp).unwrap() = Element::Variable(a-ba,-bb); true},
            _ => false
        },
        Element::Times(a, b) => match (state[&a], state[&b]) {
            (Element::Number(a), Element::Number(b)) => {*state.get_mut(&inp).unwrap() = Element::Number(a*b); true},

            // (aa+ab x) * b
            (Element::Variable(aa, ab), Element::Number(b)) => {*state.get_mut(&inp).unwrap() = Element::Variable(aa*b, ab*b); true},
            (Element::Number(a), Element::Variable(ba, bb)) => {*state.get_mut(&inp).unwrap() = Element::Variable(ba*a, bb*a); true},
            _ => false
        },
        Element::Divide(a, b) => match (state[&a], state[&b]) {
            (Element::Number(a), Element::Number(b)) => {*state.get_mut(&inp).unwrap() = Element::Number(a/b); true},
            // (aa+ab x) / b
            (Element::Variable(aa, ab), Element::Number(b)) => {*state.get_mut(&inp).unwrap() = Element::Variable(aa/b, ab/b); true},
            (Element::Number(_), Element::Variable(_,_)) => { panic!("Not implemented!"); },
            _ => false
        }
    }
}

fn solve21a(lines : Vec<String>) -> f64 {
    let mut state = parse(&lines);
    let mut todo : Vec<u32> = state.iter().filter_map(
        |(name, m)| match m {
            Element::Number(_) => None,
            _ => Some(name.clone())
        }
    ).collect();

    while todo.len() > 0 {
        todo.retain(|&i| !resolve1(i, &mut state));
    }

    match state[&name2u32("root")] {
        Element::Number(ans) => ans,
        _ => { panic!("Could not resolve answer"); }
    }
}

fn solve21b(lines : Vec<String>) -> f64 {
    let mut state = parse(&lines);

    let mut todo : Vec<u32> = state.iter().filter_map(
        |(name, m)| match m {
            Element::Number(_) => None,
            _ => Some(name.clone())
        }
    ).collect();

    // humn = 0+1*x
    *state.get_mut(&name2u32("humn")).unwrap() = Element::Variable(0.,1.);

    // root: LHR - RHS
    *state.get_mut(&name2u32("root")).unwrap() = match state[&name2u32("root")] {
        Element::Plus(a, b) => Element::Minus(a,b),
        Element::Minus(a, b) => Element::Minus(a,b),
        Element::Times(a, b) => Element::Minus(a,b),
        Element::Divide(a, b) => Element::Minus(a,b),
        _ => { panic!(""); }
    };

    while todo.len() > 0 {
        todo.retain(|&i| !resolve1(i, &mut state));
    }

    // a + b x = 0
    match state[&name2u32("root")] {
        Element::Variable(a, b) => -a/b,
        _ => { panic!("Could not resolve answer"); }
    }
}

fn main() {
    let path = Path::new("input21.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve21a(lines.clone()));
    println!("{}", solve21b(lines.clone()));
}
