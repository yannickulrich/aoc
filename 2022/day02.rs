use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn parse11(play: &str) -> Result<i32, &str> {
    match play {
        "X" | "A" => Ok(0),
        "Y" | "B" => Ok(1),
        "Z" | "C" => Ok(2),
        _ => Err(play),
    }
}

fn parse1(plays: String) -> Result<(i32, i32), String> {
    let line : Vec<Result<i32, &str>> = plays.split(" ").map(parse11).collect();
    match line[..] {
        [Ok(i), Ok(j)] => Ok((i,j)),
        _ => Err(plays)
    }
}

fn solve2a (lines : Vec<(i32,i32)> ) -> i32 {
    return lines.iter().map(
        // they play i, we play j
        |(i,j)| (
                j+1
            )+(
                3*((j-i+4)%3)
            )
    ).sum();
}

fn solve2b (lines : Vec<(i32,i32)> ) -> i32 {
    return lines.iter().map(
        // they play i, we loose (j=0), draw (j=1), win(j=2).
        // The outcome part is obviously just 3*j
        // One can show that the inversion of (j-i+4)%3 is (2 + i + j) % 3
        |(i,j)| (
                (2+i+j)%3 + 1
            )+(
                3*j
            )
    ).sum();
}

fn main() {
    let path = Path::new("input02.txt");
    let lines : Vec<(i32,i32)> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| parse1(l.expect("IO Error")).expect("Can't parse")
        )
    }.collect();

    println!("{}", solve2a(lines.clone()));
    println!("{}", solve2b(lines.clone()));
}
