use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::fs;

fn transpose<T>(v: Vec<Vec<T>>) -> Vec<Vec<T>>
where
    T: Clone,
{
    assert!(!v.is_empty());
    (0..v[0].len())
        .map(|i| v.iter().map(|inner| inner[i].clone()).collect::<Vec<T>>())
        .collect()
}

fn vm(lines : Vec<String>) -> Vec<i32> {
    vec!["noop".to_string()].iter().chain(lines.iter()).scan(
        1,
        |state, x| {
            match x.split_once(" ") {
                Some(("addx", n)) => {
                    let old = *state;
                    *state += n.parse::<i32>().expect("n");
                    Some(vec![ old, *state ])
                },
                None => Some(vec![*state]),
                _ => None
            }
        }
    ).flatten().collect::<Vec<i32>>()
}

fn solve10a(lines : Vec<String>) -> i32 {
    vm(lines).iter().enumerate().skip(19).step_by(40).map(
        |(n,i)| i*((n+1) as i32)
    ).sum()
}

fn ocr(picture : Vec<Vec<bool>>) -> String {
    let alphabet = fs::read_to_string("alphabet.txt")
        .expect("Can't read alphabet")
        .split("\n\n")
        .map(
            |l| l.split('\n').map(
                |x| x.chars().map(
                    |c| c == '#'
                ).collect::<Vec<bool>>()
            ).collect::<Vec<Vec<bool>>>()
        ).take(26).map(
            |l| transpose(l)
        ).collect::<Vec<Vec<Vec<bool>>>>();

    String::from_utf8(
        transpose(picture).chunks(5).map(
            |i| (
                alphabet.iter().position(
                    |r| r == i
                ).unwrap() + 65
            ) as u8
        ).collect::<Vec<u8>>()
    ).unwrap()
}

fn solve10b(lines : Vec<String>) -> String {
    let picture : Vec<Vec<bool>> = vm(lines).iter().enumerate().map(
        |(n,i)| {
            let x : i32 = ((n%40) + 1) as i32;
            *i <= x && x < *i+3
        }
    ).collect::<Vec<bool>>().chunks(40).take(6).map(
        |l : &[bool]| l.to_vec()
    ).collect::<Vec<Vec<bool>>>();
    ocr(picture)
}

fn main() {
    let path = Path::new("input10.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve10a(lines.clone()));
    println!("{}", solve10b(lines.clone()));
}
