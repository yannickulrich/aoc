use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn parse_mat(lines : Vec<String>) -> Vec<Vec<i32>> {
    lines.iter().map(
        |l| l.chars().map(
            |d| d as i32 - '0' as i32
        ).collect::<Vec<i32>>()
    ).collect::<Vec<Vec<i32>>>()
}

fn empty_mat<T>(size_x : usize, size_y : usize, d : T) -> Vec<Vec<T>>
where
    T: Copy,
{
    (1..size_y-1).map(
        |_| vec![d].repeat(size_x)
    ).collect::<Vec<Vec<T>>>()
}

fn transpose<T>(v: Vec<Vec<T>>) -> Vec<Vec<T>>
where
    T: Clone,
{
    assert!(!v.is_empty());
    (0..v[0].len())
        .map(|i| v.iter().map(|inner| inner[i].clone()).collect::<Vec<T>>())
        .collect()
}

fn check_spot(mat : &Vec<Vec<i32>>, x : usize, y : usize) -> bool {
    let (left, right) = mat[y].split_at(x);
    let lmax = left.iter().max().unwrap();
    let rmax = right.iter().skip(1).max().unwrap();
    (lmax < &mat[y][x]) || (rmax < &mat[y][x])
}

fn solve8a(lines : Vec<String>) -> usize {
    let mat = parse_mat(lines);

    let size_x = mat[0].len();
    let size_y = mat.len();

    let mut ans_mat : Vec<Vec<bool>> = empty_mat(size_x, size_y, false);

    for y in 1..size_y-1 {
        for x in 1..size_x-1 {
            ans_mat[y-1][x-1] |= check_spot(&mat, x, y);
        }
    }

    let matt = transpose(mat);
    for x in 1..size_x-1 {
        for y in 1..size_y-1 {
            ans_mat[y-1][x-1] |= check_spot(&matt, y, x);
        }
    }

    ans_mat.iter().map(
        |x| x.iter().map(|y| *y as usize).sum::<usize>()
    ).sum::<usize>() + 2*size_x + 2*(size_y-2)
}

fn score_spot(mat : &Vec<Vec<i32>>, x : usize, y : usize) -> i32 {
    let (left, right) = mat[y].split_at(x);

    let ldist = left.iter().rev().position(|l| {
        l >= &&mat[y][x]
    }).unwrap_or(x-1)+1;
    let rdist = right.iter().skip(1).position(|l| {
        l >= &&mat[y][x]
    }).unwrap_or(mat[y].len()-x-2)+1;

    (ldist * rdist) as i32
}

fn solve8b(lines : Vec<String>) -> i32 {
    let mat = parse_mat(lines);

    let size_x = mat[0].len();
    let size_y = mat.len();

    let mut ans_mat : Vec<Vec<i32>> = empty_mat(size_x, size_y, 1);

    for y in 1..size_y-1 {
        for x in 1..size_x-1 {
            ans_mat[y-1][x-1] *= score_spot(&mat, x, y);
        }
    }

    let matt = transpose(mat);
    for x in 1..size_x-1 {
        for y in 1..size_y-1 {
            ans_mat[y-1][x-1] *= score_spot(&matt, y, x);
        }
    }
    *ans_mat.iter().map(|x| x.iter().max().unwrap()).max().unwrap()
}

fn main() {
    let path = Path::new("input08.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve8a(lines.clone()));
    println!("{}", solve8b(lines.clone()));
}
