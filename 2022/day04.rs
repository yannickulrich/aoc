use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn parse1(line : &String) -> Result<((i32,i32), (i32,i32)), &String> {
    let l1 : Vec<Result<i32,std::num::ParseIntError>> = line.split(&['-', ','][..]).map(
        |ll| ll.parse::<i32>()
    ).collect();

    match l1[..] {
        [Ok(a), Ok(b), Ok(c), Ok(d)] => Ok(((a,b),(c,d))),
        _ => Err(line)
    }
}

fn solve4a(lines : Vec<String>) -> i32 {
    lines.iter().map(parse1).map(|l| l.expect("Parse Error")).filter(
        |((a,b),(c,d))|  (a<=c) && (d<=b) || (c<=a) && (b<=d)
    ).count() as i32
}

fn solve4b(lines : Vec<String>) -> i32 {
    lines.iter().map(parse1).map(|l| l.expect("Parse Error")).filter(
        |((a,b),(c,d))|  std::cmp::max(a,c) <= std::cmp::min(b,d)
    ).count() as i32
}

fn main() {
    let path = Path::new("input04.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve4a(lines.clone()));
    println!("{}", solve4b(lines.clone()));
}
