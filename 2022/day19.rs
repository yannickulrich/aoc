use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::collections::HashSet;
use std::collections::VecDeque;

#[derive(Debug)]
struct Blueprint {
    id : i32,
    ore_ore : i32,
    clay_ore : i32,
    obsidian_ore : i32,
    obsidian_clay : i32,
    geode_ore : i32,
    geode_obsidian : i32,
    max_ore_req : i32
}

#[derive(Debug,Hash,PartialEq,Eq,Copy,Clone)]
struct State {
    time : i32,
    nore : i32,
    nclay : i32,
    nobsidian : i32,
    ngeode : i32,

    rore : i32,
    rclay : i32,
    robsidian : i32,
    rgeode : i32
}

fn get_new_states(blueprint : &Blueprint, old_state : &State, trem : i32) -> Vec<State> {
    let mut vec = vec![];

    if old_state.nore >= blueprint.geode_ore &&
        old_state.nobsidian >= blueprint.geode_obsidian {
        vec.push(State{
            time : old_state.time + 1,
            nore : old_state.nore - blueprint.obsidian_ore + old_state.rore,
            nclay : old_state.nclay + old_state.rclay,
            nobsidian : old_state.nobsidian - blueprint.geode_obsidian + old_state.robsidian,
            ngeode : old_state.ngeode + old_state.rgeode,

            rore : old_state.rore,
            rclay : old_state.rclay,
            robsidian : old_state.robsidian,
            rgeode : old_state.rgeode + 1
        });
        return vec;
    }

    let max_ore_req = blueprint.max_ore_req;
    let max_clay_req = blueprint.obsidian_clay;
    let max_obsidian_req = blueprint.geode_obsidian;

    if old_state.nore >= blueprint.obsidian_ore &&
        old_state.nclay >= blueprint.obsidian_clay &&
        old_state.robsidian < max_obsidian_req &&
        trem > 0 {
        vec.push(State{
            time : old_state.time + 1,
            nore : old_state.nore - blueprint.obsidian_ore + old_state.rore,
            nclay : old_state.nclay - blueprint.obsidian_clay + old_state.rclay,
            nobsidian : old_state.nobsidian + old_state.robsidian,
            ngeode : old_state.ngeode + old_state.rgeode,

            rore : old_state.rore,
            rclay : old_state.rclay,
            robsidian : old_state.robsidian + 1,
            rgeode : old_state.rgeode
        });
    }

    if old_state.nore >= blueprint.clay_ore &&
        old_state.rclay < max_clay_req &&
        trem > 4 {
        vec.push(State{
            time : old_state.time + 1,
            nore : old_state.nore - blueprint.clay_ore + old_state.rore,
            nclay : old_state.nclay + old_state.rclay,
            nobsidian : old_state.nobsidian + old_state.robsidian,
            ngeode : old_state.ngeode + old_state.rgeode,

            rore : old_state.rore,
            rclay : old_state.rclay + 1,
            robsidian : old_state.robsidian,
            rgeode : old_state.rgeode
        });
    }

    if old_state.nore >= blueprint.ore_ore &&
        old_state.rore < max_ore_req &&
        trem > 15 {
        vec.push(State{
            time : old_state.time + 1,
            nore : old_state.nore - blueprint.ore_ore + old_state.rore,
            nclay : old_state.nclay + old_state.rclay,
            nobsidian : old_state.nobsidian + old_state.robsidian,
            ngeode : old_state.ngeode + old_state.rgeode,

            rore : old_state.rore + 1,
            rclay : old_state.rclay,
            robsidian : old_state.robsidian,
            rgeode : old_state.rgeode
        });
    }

    if old_state.nore < 2*max_ore_req {
        vec.push(State{
            time : old_state.time + 1,
            nore : old_state.nore + old_state.rore,
            nclay : old_state.nclay + old_state.rclay,
            nobsidian : old_state.nobsidian + old_state.robsidian,
            ngeode : old_state.ngeode + old_state.rgeode,

            rore : old_state.rore,
            rclay : old_state.rclay,
            robsidian : old_state.robsidian,
            rgeode : old_state.rgeode
        });
    }

    vec
}

fn estimate_ngeode(state : &State, tmax : i32) -> i32 {
    let trem = tmax - state.time;

    state.ngeode
    + state.rgeode * trem      // We can this many in the time remaining
    + trem*(trem+1)/2          // Assuming we can build on geode bot every
                               // minute, we expect to reap this many
    + trem*(trem+1)*(trem+2)/6
}

fn bfs(blueprint : &Blueprint, start : State, tmax : i32) -> i32 {

    let mut seen : HashSet<State> = HashSet::from([start]);
    let mut queue : VecDeque<State> = VecDeque::from([start]);
    let mut max = 0;
    let mut best_estimate = 0;

    while let Some(old_state) = queue.pop_front() {
        if old_state.ngeode > max {
            max = old_state.ngeode;
            best_estimate = estimate_ngeode(&old_state, tmax);
        } else if old_state.ngeode + 2 < max {
            continue;
        }
        if estimate_ngeode(&old_state, tmax) + 2 < best_estimate {
            continue;
        }

        // check goal
        if old_state.time == tmax {
            continue;
        }

        for i in get_new_states(blueprint, &old_state, tmax - old_state.time) {
            if seen.contains(&i) { continue; }
            seen.insert(i);
            queue.push_back(i);
        }
    }

    max
}

fn parse_blueprint(line : &String) -> Blueprint {
    let s = line.split(&[' ',':'][..]).collect::<Vec<&str>>();

    let mut blueprint = Blueprint {
        id : s[1].parse().expect(""),
        ore_ore : s[7].parse().expect(""),
        clay_ore : s[13].parse().expect(""),
        obsidian_ore : s[19].parse().expect(""),
        obsidian_clay : s[22].parse().expect(""),
        geode_ore : s[28].parse().expect(""),
        geode_obsidian : s[31].parse().expect(""),
        max_ore_req : 0
    };

    let ore_req = [
        blueprint.ore_ore,
        blueprint.clay_ore,
        blueprint.obsidian_ore,
        blueprint.geode_ore
    ];
    blueprint.max_ore_req = *ore_req.iter().max().unwrap();
    blueprint
}

fn solve19a1(line : &String) -> i32 {
    let state0 = State{
        time : 0,
        nore : 0, nclay : 0, nobsidian : 0, ngeode : 0,
        rore : 1, rclay : 0, robsidian : 0, rgeode : 0
    };
    let blueprint = parse_blueprint(line);

    bfs(&blueprint, state0, 24) * blueprint.id
}

fn solve19a(lines : Vec<String>) -> i32 {
    lines.iter().map(solve19a1).sum()
}

fn solve19b1(line : &String) -> i32 {
    let state0 = State{
        time : 0,
        nore : 0, nclay : 0, nobsidian : 0, ngeode : 0,
        rore : 1, rclay : 0, robsidian : 0, rgeode : 0
    };
    let blueprint = parse_blueprint(line);

    bfs(&blueprint, state0, 32)
}

fn solve19b(lines : Vec<String>) -> i32 {
    lines.iter().take(3).map(solve19b1).product()
}

fn main() {
    let path = Path::new("input19.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve19a(lines.clone()));
    println!("{}", solve19b(lines.clone()));
}
