use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::collections::HashSet;

fn parse(lines : Vec<String>) -> HashSet<(u32,u32)> {
    let lines_u32 : Vec<Vec<(u32,u32)>> = lines.iter().map(
        |line| line.split(" -> ").map(
            |p| {
                let pp = p.split_once(",").unwrap();
                (
                    pp.0.parse::<u32>().expect(""),
                    pp.1.parse::<u32>().expect("")
                )
            }
        ).collect()).collect();

    let (steps_x, steps_y) : (Vec<((u32,u32),(u32,u32))>, Vec<_>)
                              = lines_u32.iter().flat_map(
        |line| line.windows(2).map(
            |arr| (
                if arr[0].0 < arr[1].0 { (arr[0].0,arr[1].0) } else { (arr[1].0,arr[0].0) },
                if arr[0].1 < arr[1].1 { (arr[0].1,arr[1].1) } else { (arr[1].1,arr[0].1) }
            )
        )
    ).partition(|((x0,x1),_)| x0 == x1);

    let mut ans = HashSet::new();
    ans.extend(steps_x.iter().flat_map(|((x0,_),(y0,y1))| (*y0..=*y1).map(move |y| (*x0, y))));
    ans.extend(steps_y.iter().flat_map(|((x0,x1),(y0,_))| (*x0..=*x1).map(move |x| (x, *y0))));

    ans
}

fn evolve1(blocks : &HashSet<(u32,u32)>, (x, y) : (u32,u32)) -> ((u32,u32), bool) {
    if blocks.contains(&(x, y+1)) {
        if !blocks.contains(&(x-1, y+1)) {
            ((x-1, y+1), false)
        } else if !blocks.contains(&(x+1, y+1)) {
            ((x+1, y+1), false)
        } else {
            ((x, y), true)
        }
    } else {
        ((x,y+1), false)
    }
}

fn drop1(blocks : &mut HashSet<(u32,u32)>, lowest : u32, mode : bool) -> bool {
    let mut point : (u32,u32) = (500,0);
    let mut stop = false;

    while !stop {
        (point, stop) = evolve1(blocks, point);
        if point.1 > lowest {
            if mode { break; } else { return true; }
        }
    }
    blocks.insert(point);
    point.0 == 500 && point.1 == 0
}

fn solve14(lines : Vec<String>, mode : bool, floor : u32) -> usize {
    let mut blocks = parse(lines);
    let lowest : u32 = *blocks.iter().map(|(_,y)| y).max().unwrap() + floor;
    let mut n = 0;

    while !drop1(&mut blocks, lowest, mode) { n += 1 };
    n
}

fn solve14a(lines : Vec<String>) -> usize {
    solve14(lines, false, 1)
}

fn solve14b(lines : Vec<String>) -> usize {
    solve14(lines, true, 0) + 1
}

fn main() {
    let path = Path::new("input14.txt");
    let lines : Vec<String> = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => io::BufReader::new(file).lines().map(
            |l| l.expect("IO Error")
        )
    }.collect();
    println!("{}", solve14a(lines.clone()));
    println!("{}", solve14b(lines.clone()));
}
