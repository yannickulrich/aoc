txt = open('input10.txt').read()

maps = {
    '(': ')',
    '[': ']',
    '{': '}',
    '<': '>',
}
scoreA = {
    ')': 3, ']': 57, '}': 1197, '>': 25137
}


def is_corrupt(l):
    scope = []
    for n,i in enumerate(l):
        if i in maps.keys():
            scope.append(i)
        elif i in maps.values():
            c = maps[scope.pop()]
            if c != i:
                return i, []
    return False, scope


totalscore = 0
for l in txt.splitlines():
    i, _ = is_corrupt(l)
    if i:
        totalscore += scoreA[i]

print(totalscore)


scoreB = {
    ')': 1, ']': 2, '}': 3, '>': 4
}
totalscores2 = []
for l in txt.splitlines():
    i, scope = is_corrupt(l)
    if i: continue
    compl = [maps[i] for i in reversed(scope)]
    score = 0
    for i in compl:
        score *= 5
        score += scoreB[i]
    totalscores2.append(score)

totalscores2 = sorted(totalscores2)
print(totalscores2[len(totalscores2)//2])
