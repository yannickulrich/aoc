import Data.List
import qualified Data.Map as M


toint :: String -> Integer
toint a = read a :: Integer
flatten arr = [y | x<- arr, y <- x]

type Point = (Integer, Integer, Integer)
tuplify3 [x,y,z] = (x,y,z)

topoint :: String -> Point
topoint a = tuplify3 $
            map toint $
            words $
            map repl $
            a
    where
        repl ',' = ' '
        repl a = a

parse1 :: String -> [[Point]] -> [[Point]]
parse1 ('-':'-':a) state = []:state
parse1 "" state = state
parse1 a (s:ss) = ((topoint a) : s) : ss

parse :: String -> [[Point]]
parse contents = tail $ foldr parse1 [[]] $ lines $ contents


r2 :: Point -> Point -> Integer
r2 (x1,y1,z1) (x2,y2,z2) = (x1-x2)*(x1-x2) +
                           (y1-y2)*(y1-y2) +
                           (z1-z2)*(z1-z2)


type GMap = ((Int, (Int, Int)), ((Integer, Integer), Integer))
type Match = [(Int, Int)]

catMaybes :: [Maybe a] -> [a]
catMaybes ms = [ x | Just x <- ms ]

geomap :: [Point] -> [GMap]
geomap sc = flatten $ map geomap1 $ zip sc [0..]
    where
        geomap1 (sk, k) = catMaybes $
                          [geomap11 ii jj | ii <- zip sc [0..],
                                            jj <- jr ii]
            where
                jr (_, i) = zip (drop (fromIntegral i+1) $ sc)
                                [i+1..]
                rk = r2 sk
                geomap11 (si, i) (sj, j)
                    | i == k    = Nothing
                    | j == k    = Nothing
                    | rk0 > rk1 = Just ((k, (i,j)), ((rk1,rk0), rij))
                    | otherwise = Just ((k, (i,j)), ((rk0,rk1), rij))
                    where
                        rk0 = rk si
                        rk1 = rk sj
                        rij = r2 si sj

swap (a,b) = (b,a)

type Thr = M.Map ((Integer, Integer), Integer) Int
tryMatch1 :: Thr -> [GMap] -> [GMap] -> Match -> Match
tryMatch1 _ [] _ state = state
tryMatch1 thr (g:gs) g2 state
    | length state == 12  = state
    | otherwise           = case (M.lookup (snd $ g) $ thr) of
        Nothing -> tryMatch1 thr gs g2 state
        Just m  -> tryMatch1 thr gs g2 new
            where
                new = if ne `elem` state
                    then state
                    else (ne:state)
                ne=(fst $ fst $ g,
                    fst $ fst $ (g2 !! m))

tryMatch :: [GMap] -> [GMap] -> Maybe Match
tryMatch g1 g2 = case (length ans) of
        12 -> Just ans
        _  -> Nothing
    where
        ans = tryMatch1 thr g1 g2 []
        thr = M.fromList $
              map swap $
              zip [0..] $
              map snd $
              g2




rotate :: Integer -> Point -> Point
rotate  0 (x,y,z) = ( x, y, z)
rotate  1 (x,y,z) = ( x, y,-z)
rotate  2 (x,y,z) = ( x,-y, z)
rotate  3 (x,y,z) = ( x,-y,-z)
rotate  4 (x,y,z) = (-x, y, z)
rotate  5 (x,y,z) = (-x, y,-z)
rotate  6 (x,y,z) = (-x,-y, z)
rotate  7 (x,y,z) = (-x,-y,-z)
rotate  8 (x,y,z) = ( x, z, y)
rotate  9 (x,y,z) = ( x, z,-y)
rotate 10 (x,y,z) = ( x,-z, y)
rotate 11 (x,y,z) = ( x,-z,-y)
rotate 12 (x,y,z) = (-x, z, y)
rotate 13 (x,y,z) = (-x, z,-y)
rotate 14 (x,y,z) = (-x,-z, y)
rotate 15 (x,y,z) = (-x,-z,-y)
rotate 16 (x,y,z) = ( y, x, z)
rotate 17 (x,y,z) = ( y, x,-z)
rotate 18 (x,y,z) = ( y,-x, z)
rotate 19 (x,y,z) = ( y,-x,-z)
rotate 20 (x,y,z) = (-y, x, z)
rotate 21 (x,y,z) = (-y, x,-z)
rotate 22 (x,y,z) = (-y,-x, z)
rotate 23 (x,y,z) = (-y,-x,-z)
rotate 24 (x,y,z) = ( y, z, x)
rotate 25 (x,y,z) = ( y, z,-x)
rotate 26 (x,y,z) = ( y,-z, x)
rotate 27 (x,y,z) = ( y,-z,-x)
rotate 28 (x,y,z) = (-y, z, x)
rotate 29 (x,y,z) = (-y, z,-x)
rotate 30 (x,y,z) = (-y,-z, x)
rotate 31 (x,y,z) = (-y,-z,-x)
rotate 32 (x,y,z) = ( z, x, y)
rotate 33 (x,y,z) = ( z, x,-y)
rotate 34 (x,y,z) = ( z,-x, y)
rotate 35 (x,y,z) = ( z,-x,-y)
rotate 36 (x,y,z) = (-z, x, y)
rotate 37 (x,y,z) = (-z, x,-y)
rotate 38 (x,y,z) = (-z,-x, y)
rotate 39 (x,y,z) = (-z,-x,-y)
rotate 40 (x,y,z) = ( z, y, x)
rotate 41 (x,y,z) = ( z, y,-x)
rotate 42 (x,y,z) = ( z,-y, x)
rotate 43 (x,y,z) = ( z,-y,-x)
rotate 44 (x,y,z) = (-z, y, x)
rotate 45 (x,y,z) = (-z, y,-x)
rotate 46 (x,y,z) = (-z,-y, x)
rotate 47 (x,y,z) = (-z,-y,-x)
rotate (- 1) p = rotate  1 p
rotate (- 2) p = rotate  2 p
rotate (- 3) p = rotate  3 p
rotate (- 4) p = rotate  4 p
rotate (- 5) p = rotate  5 p
rotate (- 6) p = rotate  6 p
rotate (- 7) p = rotate  7 p
rotate (- 8) p = rotate  8 p
rotate (- 9) p = rotate 10 p
rotate (-10) p = rotate  9 p
rotate (-11) p = rotate 11 p
rotate (-12) p = rotate 12 p
rotate (-13) p = rotate 14 p
rotate (-14) p = rotate 13 p
rotate (-15) p = rotate 15 p
rotate (-16) p = rotate 16 p
rotate (-17) p = rotate 17 p
rotate (-18) p = rotate 20 p
rotate (-19) p = rotate 21 p
rotate (-20) p = rotate 18 p
rotate (-21) p = rotate 19 p
rotate (-22) p = rotate 22 p
rotate (-23) p = rotate 23 p
rotate (-24) p = rotate 32 p
rotate (-25) p = rotate 36 p
rotate (-26) p = rotate 33 p
rotate (-27) p = rotate 37 p
rotate (-28) p = rotate 34 p
rotate (-29) p = rotate 38 p
rotate (-30) p = rotate 35 p
rotate (-31) p = rotate 39 p
rotate (-32) p = rotate 24 p
rotate (-33) p = rotate 26 p
rotate (-34) p = rotate 28 p
rotate (-35) p = rotate 30 p
rotate (-36) p = rotate 25 p
rotate (-37) p = rotate 27 p
rotate (-38) p = rotate 29 p
rotate (-39) p = rotate 31 p
rotate (-40) p = rotate 40 p
rotate (-41) p = rotate 44 p
rotate (-42) p = rotate 42 p
rotate (-43) p = rotate 46 p
rotate (-44) p = rotate 41 p
rotate (-45) p = rotate 45 p
rotate (-46) p = rotate 43 p
rotate (-47) p = rotate 47 p

add :: Point -> Point -> Point
sub :: Point -> Point -> Point
add (x1,y1,z1) (x2,y2,z2) = (x1+x2, y1+y2,z1+z2)
sub (x1,y1,z1) (x2,y2,z2) = (x1-x2, y1-y2,z1-z2)

type Transform = (Integer, Point)

findShift :: (Point->Point) -> [(Point, Point)] -> Maybe Point
findShift f vs = testShift (tail vs) $
                 sub (fst $ head vs) (f $ snd $ head $ vs)
    where
        testShift []      s = Just s
        testShift (v:vvs) s
            | s == sub (fst $ v) (f $ snd $ v)  = testShift vvs s
            | otherwise                         = Nothing


findtransform1 :: [Point] -> [Point] -> Maybe Match -> Maybe Transform

findtransform1 s1 s2 Nothing = Nothing
findtransform1 s1 s2 (Just match) = Just $ testR $ [0..47]
    where
        points = map (\(i,j) -> (s1 !! i, s2 !! j) ) $ match
        testR (n:ns) = case (findShift (rotate n) $ points) of
            Nothing -> testR ns
            Just s  -> (n, s)

findtransform :: [Point] -> [Point] -> Maybe Transform
findtransform s1 s2 = findtransform1 s1 s2 $
                      tryMatch (geomap $ s1)
                               (geomap $ s2)

alltransforms :: [[Point]] -> [[Maybe Transform]]
alltransforms dat = map alltransform1 $ [0..n]
    where
        n = (length dat) - 1
        alltransform1 :: Int -> [Maybe Transform]
        alltransform1 i = map alltransform11 [0..n]
            where
                ft = findtransform (dat !! i)
                alltransform11 :: Int -> Maybe Transform
                alltransform11 j
                    | j <= i    = Nothing
                    | otherwise = ft (dat !! j)

-- The transform is U = S . R. Hence
--   U^-1 = R^-1.S^-1
invert :: Maybe Transform -> Maybe Transform
invert (Just (n, (x,y,z))) = Just (-n, (-x,-y,-z))
invert Nothing             = Nothing

exec1 :: Transform -> Point -> Point
exec :: Transform -> [Point] -> [Point]
exec1 (n, p) v  | n >= 0  = add p $ rotate n $ v
                | n <  0  = rotate n $ add p $ v
exec t vs = map (exec1 t) vs

getTS :: [[Maybe Transform]] -> Int -> Int -> Maybe Transform
getTS ts i j  | i < j = invert $ (ts !! i !! j)
              | i > j = (ts !! j !! i)
              | i ==j = Nothing


transform :: [[Maybe Transform]] -> [Int] -> [Point] -> Maybe [Point]
transform ts (0:is) vs = Just vs
transform ts prev@(i:is) vs = foldr transform1 Nothing $
                              reverse $
                              filter (\(x,_) -> notElem x prev) $
                              zip [0..] $
                              map (getTS ts i) $
                              [0..(length ts)-1]
    where
        transform1 :: (Int, Maybe Transform) -> Maybe [Point] -> Maybe [Point]
        transform1 (_, Nothing)  Nothing = Nothing
        transform1 (j, (Just t)) Nothing = transform ts (j:prev) $ exec t $ vs
        transform1 _             (Just a)= Just a


transformAll :: [[Maybe Transform]] -> [[Point]] -> [[Point]]
transformAll ts dat = catMaybes $ map go $ zip [0..] $ dat
    where
        go (i, p) = transform ts [i] p


solveDay19A :: [[Point]] -> [[Maybe Transform]] -> Int
solveDay19A dat ts = length $
                     group $
                     sort $
                     flatten $
                     transformAll ts $ dat


dist :: Point -> Point -> Integer
dist (x1,y1,z1) (x2,y2,z2) = sum [abs (x1-x2),
                                  abs (y1-y2),
                                  abs (z1-z2)]


solveDay19B :: [[Point]] -> [[Maybe Transform]] -> Integer
solveDay19B dat ts = maximum $
                     [ dist x y | x <- sl,
                                  y <- sl]
    where
        sl = map head $
             catMaybes $
             map go $
             [0..length dat-1]

        go i = transform ts [i] [(0,0,0)]


main = do
    contents <- readFile "input19.txt"
    let dat = parse $ contents
    let xx = alltransforms $ dat
    print $ solveDay19A dat xx
    print $ solveDay19B dat xx
