import copy
txt = open('input11.txt').read()

mat = [[int(i) for i in j] for j in txt.splitlines()]


def get_neighbours(x,y):
    spanX = [x]
    spanY = [y]
    if x > 0:
        spanX.append(x-1)
    if x < 9:
        spanX.append(x+1)
    if y > 0:
        spanY.append(y-1)
    if y < 9:
        spanY.append(y+1)
    return [(x,y) for x in spanX for y in spanY]

def inc(mat):
    return [[i+1 for i in j] for j in mat]


def substep(flashes, mat):
    mat = copy.deepcopy(mat)
    flashes = copy.deepcopy(flashes)
    change = False

    n = []
    for x in range(10):
        for y in range(10):
            if mat[y][x] > 9 and not flashes[y][x]:
                flashes[y][x] = True
                change = True
                n = n + get_neighbours(x,y)

    for xx,yy in n:
        mat[yy][xx] += 1

    return change, flashes, mat


def cut(mat):
    return [[0 if i > 9 else i for i in j] for j in mat]


def step(mat):
    flashes = [[False for j in range(10)] for i in range(10)]
    mat = inc(mat)
    change = True
    while change:
        change,flashes,mat= substep(flashes, mat)

    mat = cut(mat)
    return mat, flashes


acc = 0
for i in range(100):
    mat, flashes = step(mat)
    acc += sum(sum(j) for j in flashes)

print(acc)


i = 0
mat = [[int(i) for i in j] for j in txt.splitlines()]
flashes = [[False for j in range(10)] for i in range(10)]
while not all(all(i) for i in flashes):
    mat, flashes = step(mat)
    i += 1
print(i)
