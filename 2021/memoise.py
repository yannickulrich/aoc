# From https://wiki.python.org/moin/PythonDecoratorLibrary#Memoize
def myhash(a):
    if type(a) == int:
        return a
    if type(a) == str:
        return a
    elif type(a) == tuple:
        return a
    elif type(a) == list:
        return tuple(a)
    elif type(a) == dict:
        return (tuple(a.keys()), tuple(a.values()))


class memoised(object):
    '''Decorator. Caches a function's return value each time it is called.
    If called later with the same arguments, the cached value is returned
    (not reevaluated).
    '''
    def __init__(self, func):
        self.func = func
        self.cache = {}

    def __call__(self, *args):
        h = hash(tuple(
            myhash(a) for a in args
        ))
        if h in self.cache:
            return self.cache[h]
        else:
            value = self.func(*args)
            self.cache[h] = value
        return value
