import Data.List

tuplify2 :: [a] -> (a,a)
tuplify2 [x,y] = (x,y)
detuplify2 :: (a,a) -> [a]
detuplify2 (x,y) = [x,y]

txt2map1 :: String -> (String,String)
txt2map1 line = tuplify2 $ words $ map repl line
    where
        repl '-' = ' '
        repl x = x

flatten arr = [y | x<- arr, y <- x]

all_caves :: [(String,String)] -> [String]
all_caves mat = map head $
                group $
                sort $
                flatten $
                map detuplify2 $
                mat

isin :: String -> (String, String) -> String
isin a (b, c)  | a == b    = c
               | a == c    = b
               | otherwise = ""


type Conns = [(String, [String])]
connections :: [(String,String)] -> Conns
connections mapp = map (\i -> (i, filtere $ innermap i)) $
                   all_caves $
                   mapp
    where
        innermap i = map (isin i) mapp
        filtere  i = filter (\s -> length s > 0) i


isLower :: Char -> Bool
isLower c = 90 < fromEnum c

flatmap :: (t -> [a]) -> [t] -> [a]
flatmap _ [] = []
flatmap f (x:xs) = f x ++ flatmap f xs

type CangoFunc = [String] -> String -> Bool
cangoA :: [String] -> String -> Bool
cangoA _ "start" = False
cangoA v c  | (isLower $ head c) && (c `elem` v) = False
            | (isLower $ head c)                 = True
            | otherwise                          = True

con4c :: String -> Conns -> [String]
con4c c conns = snd $
                head $
                filter (\(x,y) -> x == c) $
                conns

opts :: CangoFunc -> String -> [String] -> Conns -> [String]
opts cango c v connections = filter (cango v) $ con4c c $ connections


search :: CangoFunc -> String -> [String] -> Conns -> [[String]]
search _ "end" path conns = ["end":path]
search cango c path conns = flatmap (\i -> search cango i (c:path) conns) $
                            opts cango c (c:path) conns


solveDay12A :: [String] -> Int
solveDay12A lines = length $
                    search cangoA "start" [] $
                    connections $
                    map txt2map1 $
                    lines


hasAlreadyTwo :: [String] -> Bool
hasAlreadyTwo v = 0 < (length $ filter (\x -> 2==length x) $ group $ sort $ v)

isok v = not$ hasAlreadyTwo $ filter (isLower.head) $ v

cangoB :: [String] -> String -> Bool
cangoB _ "start" = False
cangoB _ "end" = True
cangoB v c  | (isLower $ head c) && (c `elem` v)  = isok v
            | (isLower $ head c)                  = True
            | otherwise                           = True

solveDay12B :: [String] -> Int
solveDay12B lines = length $
                    search cangoB "start" [] $
                    connections $
                    map txt2map1 $
                    lines

main = do
    contents <- readFile "input12.txt"
    print $ solveDay12A $ lines $ contents
    print $ solveDay12B $ lines $ contents
