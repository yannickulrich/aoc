import re

txt=open('input17.txt').read()

target = re.findall('target area: x=(\d*)\.\.(\d*), y=(-\d*)\.\.(-\d*)', txt)[0]
target = [int(i) for i in target]

def sign(x):
    if x == 0: return 0
    return x//abs(x)


def testsol(vx, vy):
    tx = (target[0], target[1])
    ty = (target[2], target[3])
    x = 0
    y = 0
    maxy = 0
    while True:
        x += vx
        y += vy
        vx -= sign(vx)
        vy -= 1

        if maxy < y: maxy = y

        if y < ty[0]:
            return False, 0
        if x > tx[1]:
            return False, 0

        if tx[0] <= x <= tx[1] and ty[0] <= y <= ty[1]:
            return True, maxy


maxmaxy = 0
n = 0
for vx in range(0,300):
    for vy in range(-100,200):
        ok, maxy = testsol(vx,vy)
        if ok:
            n += 1
            if maxmaxy < maxy:
                maxmaxy = maxy

print(maxmaxy)
print(n)
