txt=open('input09.txt').read()
dat = [[int(j) for j in i] for i in txt.splitlines()]


def get_neighbours(x,y):
    n = []
    if x > 0:
        n.append((x-1,y,dat[y][x-1]))
    if x < len(dat[y])-1:
        n.append((x+1,y,dat[y][x+1]))
    if y > 0:
        n.append((x,y-1,dat[y-1][x]))
    if y < len(dat)-1:
        n.append((x,y+1,dat[y+1][x]))
    return n

lowp = []
for y in range(len(dat)):
    for x in range(len(dat[y])):
        if dat[y][x] < min(h for _,_,h in get_neighbours(x,y)):
            lowp.append((x,y,dat[y][x]))

print(sum(h+1 for x,y,h in lowp))

def ascend(x,y,h):
    yield (x,y,h)
    for xx,yy,hh in get_neighbours(x,y):
        if hh == 9:
            continue
        if hh > h:
            #print("Ascending into (%d,%d)=%d" % (xx,yy,hh))
            yield from ascend(xx,yy,hh)


sizes = []
for x,y,h in lowp:
    sizes.append(len(set(ascend(x,y,h))))

acc = 1
for i in sorted(sizes)[-3:]:
    acc *= i
print(acc)
