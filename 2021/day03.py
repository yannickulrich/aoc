txt = open('input03.txt').read()
dat = [int(i,2) for i in txt.splitlines()]

numbers = list(reversed([1<<i for i in range(len(txt.splitlines()[0]))]))
gamma = [sum(i & n for i in dat) > n * len(dat)/2 for n in numbers]
eps = [not i for i in gamma]

gamma = int(''.join('1' if i else '0' for i in gamma),2)
eps = int(''.join('1' if i else '0' for i in eps),2)

print(gamma*eps)



dat2 = dat[:]
for n in numbers:
    zeroQ = (sum(i & n for i in dat2) >= n * len(dat2)/2)
    if zeroQ:
        dat2 = [i for i in dat2 if i & n]
    else:
        dat2 = [i for i in dat2 if not (i & n)]
    if len(dat2) == 1:
        break

ox = dat2[0]

dat2 = dat[:]
for n in numbers:
    oneQ = not (sum(i & n for i in dat2) >= n * len(dat2)/2)
    if oneQ:
        dat2 = [i for i in dat2 if i & n]
    else:
        dat2 = [i for i in dat2 if not (i & n)]
    if len(dat2) == 1:
        break

co2 = dat2[0]

print(ox*co2)
