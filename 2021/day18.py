import math
from copy import deepcopy as dc


def atind(lst, ind):
    if len(ind) == 0:
        return lst
    if len(ind) > 1:
        return atind(lst[ind[0]], ind[1:])
    else:
        return lst[ind[0]]


def chatind(lst, ind, what):
    if len(ind) > 1:
        return lst[:ind[0]] + \
               [chatind(lst[ind[0]], ind[1:], what)] + \
               lst[ind[0]+1:]
    else:
        what = [what]
        return lst[:ind[0]] + \
               what + \
               lst[ind[0]+1:]


def rneighbour(a, ind):
    if len(ind) == 0:
        return []
    if len(atind(a, ind[:-1])) > ind[-1]+1:
        newind = ind[:-1] + [ind[-1]+1]
        while type(atind(a, newind)) == list:
            newind.append(0)
        return newind
    else:
        return rneighbour(a, ind[:-1])


def lneighbour(a, ind):
    if len(ind) == 0:
        return []
    if ind[-1] > 0:
        newind = ind[:-1] + [ind[-1]-1]
        while type(atind(a, newind)) == list:
            newind.append(1)
        return newind
    else:
        return lneighbour(a, ind[:-1])


def explodeInd(a, ind):
    l, r = atind(a, ind)
    rn = rneighbour(a, ind)
    ln = lneighbour(a, ind)

    anew = chatind(a, ind, 0)
    if rn:
        anew = chatind(anew, rn, r + atind(a, rn))
    if ln:
        anew = chatind(anew, ln, l + atind(a, ln))
    return anew


def splitInd(a, ind):
    n = atind(a, ind)
    return chatind(a, ind, [
        math.floor(n/2),
        math.ceil(n/2)
    ])


def unnest(lst, ind=[], dir={}):
    sub = atind(lst, ind)
    if type(sub) == int:
        dir[tuple(ind)] = sub
    else:
        for i in range(len(sub)):
            dir = unnest(lst, ind + [i], dir)
    return dir


def explode(lst):
    nested4 = sorted(
        [i for i in unnest(lst,[],{}).keys() if len(i) > 4]
    )
    if len(nested4) > 0:
        return explodeInd(lst, list(nested4[0])[:-1])
    else:
        return lst


def split(lst):
    bigger10 = sorted(
        [i for i,j in unnest(lst,[],{}).items() if j >= 10]
    )
    if len(bigger10) > 0:
        return splitInd(lst, list(bigger10[0]))
    else:
        return lst

def reduce(lst):
    while True:
        lstold = dc(lst)
        lst = explode(lst)
        if lst != lstold:
            continue
        lst = split(lst)
        if lst != lstold:
            continue
        break
    return lst


def add(a, b):
    return reduce([dc(a),dc(b)])


def magnitude(lst):
    if type(lst) == int:
        return lst
    else:
        return 3*magnitude(lst[0]) + 2*magnitude(lst[1])
def test_explode():
    assert explode(
        [[[[[9,8],1],2],3],4]
        #[0,0,0,0]
    ) == [[[[0,9],2],3],4]
    assert explode(
        [7,[6,[5,[4,[3,2]]]]]
        #[1,1,1,1]
    ) == [7,[6,[5,[7,0]]]]
    assert explode(
        [[6,[5,[4,[3,2]]]],1]
        #[0,1,1,1]
    ) == [[6,[5,[7,0]]],3]
    assert explode(
        [[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]
        #[0,1,1,1]
    ) == [[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]
    assert explode(
        [[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]
        #[1,1,1,1]
    ) == [[3,[2,[8,0]]],[9,[5,[7,0]]]]
    assert add(
        [[[[4,3],4],4],[7,[[8,4],9]]],
        [1,1]
    ) == [[[[0,7],4],[[7,8],[6,0]]],[8,1]]
    assert magnitude(
        [[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]
    ) == 3488


txt = open('input18.txt').read()
dat = [eval(i) for i in txt.splitlines()]

acc = dat[0]
for i in dat[1:]:
    acc = add(acc, i)

print(magnitude(acc))

mv = 0
for i in dat:
    for j in dat:
        m = magnitude(add(i,j))
        if mv < m:
            mv = m
print(mv)
