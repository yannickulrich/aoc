import re
import math
import itertools

txt=open('input19.txt').read()

scanners = [
    [
        [int(k) for k in j.split(',')]
        for j in i.splitlines()[:-1]
    ]
    for i in re.split('--- scanner \d* ---\n', txt+'\n')[1:]
]

def r2(p1, p2):
    return (p1[0]-p2[0])*(p1[0]-p2[0]) + \
           (p1[1]-p2[1])*(p1[1]-p2[1]) + \
           (p1[2]-p2[2])*(p1[2]-p2[2])


def geomap(sc):
    for k in range(len(sc)):
        for i in range(len(sc)):
            for j in range(i+1, len(sc)):
                if k == i or k == j:
                    continue

                rk = [
                    r2(sc[i], sc[k]),
                    r2(sc[j], sc[k])
                ]
                rij = r2(sc[i], sc[j])
                if rk[0] > rk[1]:
                    yield ((k, (i,j)), ((rk[1],rk[0]), rij))
                else:
                    yield ((k, (i,j)), ((rk[0],rk[1]), rij))


def trymatch(mapA, mapB):
    thr = [i for _,i in mapB]
    thr = {j:i for i,j in enumerate(thr)}

    match = set()
    for indA, v in mapA:
        try:
            indB = thr[v]
            match.add((indA[0], mapB[indB][0][0]))
            if len(match) == 12:
                break
        except KeyError:
            pass

    assert len(match) == 12
    return match


def rotated():
    def sign(a):
        if a > 0:
            return '+'
        else:
            return '-'

    def build(a,b,c):
        return eval(
            (
                "lambda v : ["
                    "%sv[%d],"
                    "%sv[%d],"
                    "%sv[%d]"
                "]"
            ) % (
                sign(a), abs(a)-1,
                sign(b), abs(b)-1,
                sign(c), abs(c)-1
            )
        )

    for x,y,z in itertools.permutations([1,2,3]):
        yield build( x, y, z)
        yield build( x, y,-z)
        yield build( x,-y, z)
        yield build( x,-y,-z)
        yield build(-x, y, z)
        yield build(-x, y,-z)
        yield build(-x,-y, z)
        yield build(-x,-y,-z)


def sub(a, b):
    return [a[0]-b[0], a[1]-b[1], a[2]-b[2]]
def add(a, b):
    return [a[0]+b[0], a[1]+b[1], a[2]+b[2]]

def find_transformation(s1, s2, match):
    for r in rotated():
        shift = None
        for i, j in match:
            v1 = s1[i]
            v2 = r(s2[j])

            if shift:
                if shift != sub(v1, v2):
                    shift = None
                    break
            else:
                shift = sub(v1, v2)
        if shift:
            return (shift, r)


def exectransform(t, vs):
    s,r = t
    return [
        add(r(v), s) for v in vs
    ]

def transform(trafo, istart, vs, prev=[]):
    if (0, istart) in trafo:
        return exectransform(trafo[(0, istart)], vs)
    else:
        for i, j in trafo.keys():
            if j != istart:
                continue
            if i in prev:
                continue

            ans = transform(
                trafo, i, exectransform(trafo[(i,j)], vs), prev + [i]
            )
            if ans:
                return ans


geomaps = [list(geomap(i)) for i in scanners]
trafo = {}
for i1 in range(len(scanners)):
    for i2 in range(i1+1,len(scanners)):
        try:
            s1 = scanners[i1]
            s2 = scanners[i2]
            match = trymatch(geomaps[i1], geomaps[i2])
            trafo[(i1,i2)] = find_transformation(s1, s2, match)
            trafo[(i2,i1)] = find_transformation(s2, s1, [
                (j,i) for i,j in match
            ])
        except AssertionError:
            pass


beacons = set(tuple(i) for i in scanners[0])
for i in range(1,len(scanners)):
    new = transform(trafo, i, scanners[i])
    beacons.update(set(tuple(j) for j in new))

print(len(beacons))

scannerLoc = [
    transform(trafo, i, [(0,0,0)])[0]
    for i in range(len(scanners))
]

maxd = 0
for s1 in scannerLoc:
    for s2 in scannerLoc:
        d = abs(s1[0]-s2[0])+abs(s1[1]-s2[1])+abs(s1[2]-s2[2])
        if d > maxd:
            maxd = d

print(maxd)
