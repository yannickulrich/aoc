txt = open('input08.txt').read()

dat = [[j.split(' ') for j in i.split(' | ')] for i in txt.splitlines()]

def filterN(dat, n):
    return [[k for k in j if len(k) == n] for i,j in dat]

def flatten(x):
    return [j for i in x for j in i]

print(len(flatten(filterN(dat, 2) + filterN(dat, 4) + filterN(dat, 3) + filterN(dat, 7))))


def anyok(a, b):
    return sorted(a) == sorted(b)


def deduce(l):
    l = sorted(l, key=lambda x:len(x))
    ll = [set(i) for i in l]

    # We know that, for 1 to match, we need
    #
    #    anyok([cc,ff], l[0])
    #
    ccff = list(ll[0])
    # and for 7 to match, we need
    #
    #    anyok([aa,cc,ff], l[1])
    #
    # Even if we don't know if (cc,ff) = l[0] or (ff,cc) = l[0], we do
    # know that aa = l[1] \ l[0]
    aa = list(ll[1] - ll[0])[0]
    # For 4 to match, we need
    #
    #    anyok([bb,cc,dd,ff], l[2])
    #
    # Since we already know that cc and ff are given through l[0],
    # l[2] gives us bb and dd
    bbdd = list(ll[2] - ll[0])
    # We now have an idea for the letters a, b, c, d, and f but
    # nothing yet for e and g since these don't appear in the numbers
    # 1, 4, or 7. By everything that does appear in these numbers, we
    # are left with two combinations for ee and gg
    eegg = list(
        {'a','b','c','d','e','f','g'} - ll[0] - ll[1] - ll[2]
    )

    # Now we can build all remaining eight options
    options = [
        (
            aa,
            bbdd[i],
            ccff[j],
            bbdd[(i+1)%2],
            eegg[k],
            ccff[(j+1)%2],
            eegg[(k+1)%2]
        )
        for i in [0,1]
        for j in [0,1]
        for k in [0,1]
    ]

    # Filter such that 2,3,5 matches
    options = [
        (aa,bb,cc,dd,ee,ff,gg)
        for aa,bb,cc,dd,ee,ff,gg in options
        if ( # 2,3,5
            anyok([aa,cc,dd,ee,gg], l[3]) and
            anyok([aa,cc,dd,ff,gg], l[4]) and
            anyok([aa,bb,dd,ff,gg], l[5])
        ) or ( # 2,5,3
            anyok([aa,cc,dd,ee,gg], l[3]) and
            anyok([aa,cc,dd,ff,gg], l[5]) and
            anyok([aa,bb,dd,ff,gg], l[4])
        ) or ( # 3,2,5
            anyok([aa,cc,dd,ee,gg], l[4]) and
            anyok([aa,cc,dd,ff,gg], l[3]) and
            anyok([aa,bb,dd,ff,gg], l[5])
        ) or ( # 3,5,2
            anyok([aa,cc,dd,ee,gg], l[4]) and
            anyok([aa,cc,dd,ff,gg], l[5]) and
            anyok([aa,bb,dd,ff,gg], l[3])
        ) or ( # 5,2,3
            anyok([aa,cc,dd,ee,gg], l[5]) and
            anyok([aa,cc,dd,ff,gg], l[3]) and
            anyok([aa,bb,dd,ff,gg], l[4])
        ) or ( # 5,3,2
            anyok([aa,cc,dd,ee,gg], l[5]) and
            anyok([aa,cc,dd,ff,gg], l[4]) and
            anyok([aa,bb,dd,ff,gg], l[3])
        )

    ]
    assert len(options) == 1
    return options[0]


def get_digit(sol, digit):
    mapping = {
        'abcefg': 0,
        'cf': 1,
        'acdeg': 2,
        'acdfg': 3,
        'bcdf': 4,
        'abdfg': 5,
        'abdefg': 6,
        'acf': 7,
        'abcdefg': 8,
        'abcdfg': 9
    }
    soldict = dict(zip(sol, ('a','b','c','d','e','f','g')))
    return mapping[''.join(sorted(soldict[i] for i in digit))]


def solve_one_line(inp, line):
    sol = deduce(inp)
    ans = [get_digit(sol, i) for i in line]
    powers = [10**i for i in reversed(range(len(ans)))]
    return sum(i*j for i,j in zip(ans,powers))

print(sum(solve_one_line(inp, line) for inp,line in dat))
