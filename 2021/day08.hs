import Data.List


loadLine :: String -> ([String], [String])
loadLine l = splitAt 10 $
             words $
             map repl l
    where
        repl '|' = ' '
        repl c = c

nopQ :: [Int] -> String -> Bool
nopQ n s = elem (length s) n

filterNop :: [String] -> [String]
filterNop s = filter (nopQ [2,4,3,7]) $ s

flatten arr = [y | x<- arr, y <- x]

solveDay8A :: [String] -> Int
solveDay8A lines = length $
                   flatten $
                   map filterNop $
                   map snd $
                   map loadLine $
                   lines


anyok :: String -> String -> Bool
anyok a b = sort a == sort b


-- We know that, for 1 to match, we need
--
--    anyok([cc,ff], l[0])
--
ccff :: [String] -> [Char]
ccff l = head $ l
-- and for 7 to match, we need
--
--    anyok([aa,cc,ff], l[1])
--
-- Even if we don't know if (cc,ff) = l[0] or (ff,cc) = l[0], we do
-- know that aa = l[1] \ l[0]
aa :: [String] -> Char
aa l = head $ filter (\x -> not $ elem x (head l)) $ (l !! 1)
-- For 4 to match, we need
--
--    anyok([bb,cc,dd,ff], l[2])
--
-- Since we already know that cc and ff are given through l[0],
-- l[2] gives us bb and dd
bbdd :: [String] -> [Char]
bbdd l = filter (\x -> not $ elem x (head l)) $ l !! 2
-- We now have an idea for the letters a, b, c, d, and f but
-- nothing yet for e and g since these don't appear in the numbers
-- 1, 4, or 7. By everything that does appear in these numbers, we
-- are left with two combinations for ee and gg
eegg :: [String] -> [Char]
eegg l = filter (\x -> not $ elem x (l !! 0)) $
         filter (\x -> not $ elem x (l !! 1)) $
         filter (\x -> not $ elem x (l !! 2)) $
         "abcdefg"

-- Now we can build all remaining eight options
option :: [String] -> (Int,Int,Int) -> [Char]
option l (i,j,k) = [a,bd !! i, cf !! j, bd !! ((i+1)`mod`2), eg !! k, cf !! ((j+1)`mod`2), eg !! ((k+1)`mod`2)]
    where a  = aa l
          bd = bbdd l
          cf = ccff l
          eg = eegg l

bpower :: [[Int]] -> [[Int]]
bpower [[a,b]] = [[a],[b]]
bpower (a:as) = (map (\x -> (head$a):x) $ bpower as)++
                (map (\x -> (last$a):x) $ bpower as)

tuplify [a,b,c] = (a,b,c)

options :: [String] -> [[Char]]
options l = map (option l) $
            map tuplify $
            bpower [[0,1],[0,1],[0,1]]


filter235 :: [String] -> [Char] -> Bool
filter235 l [a,b,c,d,e,f,g]= and [anyok [a,c,d,e,g] (l!!3),
                                   anyok [a,c,d,f,g] (l!!4),
                                   anyok [a,b,d,f,g] (l!!5)]
filter253 :: [String] -> [Char] -> Bool
filter253 l [a,b,c,d,e,f,g]= and [anyok [a,c,d,e,g] (l!!3),
                                   anyok [a,c,d,f,g] (l!!5),
                                   anyok [a,b,d,f,g] (l!!4)]
filter325 :: [String] -> [Char] -> Bool
filter325 l [a,b,c,d,e,f,g]= and [anyok [a,c,d,e,g] (l!!4),
                                   anyok [a,c,d,f,g] (l!!3),
                                   anyok [a,b,d,f,g] (l!!5)]
filter352 :: [String] -> [Char] -> Bool
filter352 l [a,b,c,d,e,f,g]= and [anyok [a,c,d,e,g] (l!!4),
                                   anyok [a,c,d,f,g] (l!!5),
                                   anyok [a,b,d,f,g] (l!!3)]
filter523 :: [String] -> [Char] -> Bool
filter523 l [a,b,c,d,e,f,g]= and [anyok [a,c,d,e,g] (l!!5),
                                   anyok [a,c,d,f,g] (l!!3),
                                   anyok [a,b,d,f,g] (l!!4)]
filter532 :: [String] -> [Char] -> Bool
filter532 l [a,b,c,d,e,f,g]= and [anyok [a,c,d,e,g] (l!!5),
                                   anyok [a,c,d,f,g] (l!!4),
                                   anyok [a,b,d,f,g] (l!!3)]



filterany235 :: [String] -> [Char] -> Bool
filterany235 a b = or [filter235 a b,
                       filter253 a b,
                       filter325 a b,
                       filter352 a b,
                       filter523 a b,
                       filter532 a b]

cmpLength a b | (length a) > (length b)  = GT
              | otherwise                = LT
sortByLength :: [[a]] -> [[a]]
sortByLength xs = sortBy cmpLength xs

deduce :: [String] -> [Char]
deduce l = head $
           filter (filterany235 l) $
           options $
           l

allIndices arr n = filter fst $
                   zip (map (\x -> x==n) $ arr) [0..length arr]
index arr n = case(length t) of
                0 -> -1
                (_) -> snd $ head $ t
    where t = allIndices arr n


mapping :: [Char] -> Integer
mapping "abcefg" = 0
mapping "cf" = 1
mapping "acdeg" = 2
mapping "acdfg" = 3
mapping "bcdf" = 4
mapping "abdfg" = 5
mapping "abdefg" = 6
mapping "acf" = 7
mapping "abcdefg" = 8
mapping "abcdfg" = 9

fullMapping :: [Char] -> [Char] -> Integer
fullMapping sol req = mapping $
                      sort $
                      map (\x -> "abcdefg" !! x) $
                      map (index sol) $
                      req

digits2num :: [Integer] -> Integer
digits2num [k,h,d,u] = 1000*k+100*h+10*d+u

snd2num :: [String] -> [Char] -> Integer
snd2num req sol = digits2num $ map (fullMapping sol) $ req

workLine :: ([String], [String]) -> Integer
workLine (obs, req) = snd2num req $
                      deduce $
                      sortByLength $
                      obs


solveDay8B :: [String] -> Integer
solveDay8B lines = sum $
                   map workLine $
                   map loadLine $
                   lines


main = do
    contents <- readFile "input08.txt"
    print $ solveDay8A $ lines $ contents
    print $ solveDay8B $ lines $ contents
