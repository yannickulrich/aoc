# Advent of Code 2021

The [Computing Club at IPPP Durham](https://notes.dmaitre.phyip3.dur.ac.uk/computing-club/)
is doing [Advent of Code 2021](https://adventofcode.com/). These are
my solutions to the problems, trying to learn Haskell.

I usually start with a super bodged implementation in Python that I
can then use to compare to while building the nice Haskell one.

Link: [Other people's solutions](https://notes.dmaitre.phyip3.dur.ac.uk/computing-club/sessions/aoc21a/)

... and of course everything's better with [CI](https://gitlab.com/yannickulrich/aoc-2021/-/blob/master/.gitlab-ci.yml)

[![pipeline status](https://gitlab.com/yannickulrich/aoc-2021/badges/master/pipeline.svg)](https://gitlab.com/yannickulrich/aoc-2021/-/pipelines/latest)


The following puzzles are slower than I'd like
 * `day15.py`: the `min(Q, key=fdist.get)` is by far the slowest bit
 * `day18.py`: `atind` and `unnest` are slow. We also spend too much
               time with `deepcopy`
 * ~~`day19.py`: we spend most time in `r2` and `geomap`~~
 * ~~`day25.py`: the main loops is quite slow~~
 * `day13.hs`: almost all time spent in `dots2mat`
 * `day20.hs`: mostly in `indAt` (`get`'s lookup and `h`)
 * `day19.hs`: `r2`, the `lookup` and `fromList`
 * ~~`day21.hs`: lacking memoisation means this will never converge~~
