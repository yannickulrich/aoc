import qualified Data.Map as M


toint :: String -> Integer
toint a = read a :: Integer

parseLine :: String -> Integer
parseLine = toint.last.words

roll1 :: Integer -> Integer
roll1 d = mod (d+1) 100

roll3 :: Integer -> [Integer]
roll3 die = take 3 $ tail $ iterate roll1 $ die

type Player = (Integer, Integer)
type StateA = (Integer, Player, Player)
evolve :: Integer -> StateA -> Integer
evolve n (d, (p1, s1), (p2, s2))
    | (0 == mod n 2) && (s1n < 1000)   = evolve (n+1) $
                                         (dnew, (p1n, s1n), (p2, s2))
    | (1 == mod n 2) && (s2n < 1000)   = evolve (n+1) $
                                         (dnew, (p1, s1), (p2n, s2n))
    | otherwise                        = (3*n+3) * (min s1f s2f)
    where
        s1f = case (mod n 2) of
            0 -> s1n
            1 -> s1
        s2f = case (mod n 2) of
            0 -> s2
            1 -> s2n

        s1n = s1+p1n+1
        s2n = s2+p2n+1
        p1n = mod (p1 + (sum rolls) + 3) 10
        p2n = mod (p2 + (sum rolls) + 3) 10
        dnew = last $ rolls
        rolls = roll3 d


solveDay21A :: String -> Integer
solveDay21A contents = evolve 0 (-1, (pos!!0 - 1, 0), (pos!!1 - 1, 0))
    where pos = map parseLine $ lines $ contents

dieV 3 = 1
dieV 4 = 3
dieV 5 = 6
dieV 6 = 7
dieV 7 = 6
dieV 8 = 3
dieV 9 = 1

type StateB = (Player, Player)
type Memo = M.Map StateB (Integer, Integer)
type Repl = (Memo, (Integer, Integer))

play :: Memo -> StateB -> Repl
play memo g@((p1,s1), (p2,s2))
    | s2 >= 21  = (memo, (0,1))
    | otherwise = case (M.lookup g memo) of
        Just v  -> (memo, v)
        Nothing -> (M.insert g res memo2, res)
    where
        (memo2, res) = foldl forloop (memo, (0,0)) $ [3..9]
        forloop :: Repl -> Integer -> Repl
        forloop (m, (w1, w2)) v = (m', (w1+v1*n, w2+v2*n))
            where
                n = dieV v
                (m', (v2, v1)) = play m ((p2, s2), (p, s1+p+1))
                p = mod (p1+v) 10

mymax :: (Integer, Integer) -> Integer
mymax (a,b) = max a b

solveDay21B :: String -> Integer
solveDay21B contents = mymax $ snd $ play M.empty ((pos!!0 - 1, 0), (pos!!1 - 1, 0))
    where
        pos = map parseLine $ lines $ contents

main = do
    contents <- readFile "input21.txt"
    print $ solveDay21A $ contents
    print $ solveDay21B $ contents
