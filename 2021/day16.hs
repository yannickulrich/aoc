import Data.Bits

mask2int :: [Bool] -> Int
mask2int bm = sum $
              map (\(x,y) -> x*(shiftL 1 (y-1))) $
              zip (map fromEnum $ bm)
                  (reverse $ [0..(length bm)])

-- This converts a single String into an Integer
tointBin :: String -> Int
tointBin a = mask2int $ map (\x -> x=='1') $ a


hexmap :: Char -> String
hexmap '0'="0000"
hexmap '1'="0001"
hexmap '2'="0010"
hexmap '3'="0011"
hexmap '4'="0100"
hexmap '5'="0101"
hexmap '6'="0110"
hexmap '7'="0111"
hexmap '8'="1000"
hexmap '9'="1001"
hexmap 'A'="1010"
hexmap 'B'="1011"
hexmap 'C'="1100"
hexmap 'D'="1101"
hexmap 'E'="1110"
hexmap 'F'="1111"


flatten arr = [y | x<- arr, y <- x]
tobin :: String -> String
tobin h = flatten $ map hexmap h

header :: String -> ((Int, Int), String)
header b = ((tointBin $ fst $ sp0,tointBin $ fst $ sp1),snd $ sp1)
    where
        sp1 = splitAt 3 $ snd $ sp0
        sp0 = splitAt 3 b

parseLit1 :: String -> String -> (String, String)
parseLit1 acc b  | head b == '0'  = (newacc, snd $ sp)
                 | otherwise      = parseLit1 newacc $ snd sp
    where
        newacc = acc ++ (tail $ fst $ sp)
        sp = splitAt 5 b

parseLit :: String -> (Int, String)
parseLit b = (tointBin $ fst p, snd p)
    where p = parseLit1 "" b

applyOp :: Int -> [Int] -> Int
applyOp 0 a = sum $ a
applyOp 1 a = product $ a
applyOp 2 a = minimum $ a
applyOp 3 a = maximum $ a
applyOp 5 [a,b]  | a > b     = 1
                 | otherwise = 0
applyOp 6 [a,b]  | a < b     = 1
                 | otherwise = 0
applyOp 7 [a,b]  | a == b    = 1
                 | otherwise = 0

apply1 :: Int -> Int -> (([Int], Int), String) -> ((Int, Int), String)
apply1 vers typ ((ans, vacc), rem) = ((applyOp typ $ reverse $ ans, vers + vacc), rem)

parse :: String -> ((Int, Int), String)

parseWhile :: String -> (([Int], Int), String)
parseWhile body = (fst $ parse1 0 [] $ fst $ rem, snd $ rem)
    where
        rem = splitAt len $ snd $ b
        len = tointBin $ fst $ b
        b = splitAt 15 $ body

        parse1 :: Int -> [Int] -> String -> (([Int], Int),String)
        parse1 vacc ans []  = ((ans, vacc), "")
        parse1 vacc ans rem = parse1 (v+vacc) ([a]++ans) r
            where
                r = snd $ pans
                a = fst $ fst $ pans
                v = snd $ fst $ pans
                pans = parse rem


parseFor :: String -> (([Int], Int), String)
parseFor body = parse1 n 0 [] r
    where
        n = tointBin $ fst $ splitAt 11 body
        r =            snd $ splitAt 11 body

        parse1 :: Int -> Int -> [Int] -> String -> (([Int], Int),String)
        parse1 0 vacc ans rem = ((ans, vacc), rem)
        parse1 n vacc ans rem = parse1 (n-1) (v+vacc) ([a]++ans) r
            where
                r = snd $ pans
                a = fst $ fst $ pans
                v = snd $ fst $ pans
                pans = parse rem


tupleAppend :: Int -> (Int, String) -> ((Int, Int), String)
tupleAppend c (a, b) = ((a,c),b)

parse b | (length b) < 7            = ((0, 0), "")
        | typ == 4                  = tupleAppend vers $
                                      parseLit $
                                      snd $ hb
        | (head $ snd $ hb) == '0'  = apply1 vers typ $
                                      parseWhile $
                                      tail $ snd $ hb
        | (head $ snd $ hb) == '1'  = apply1 vers typ $
                                      parseFor $
                                      tail $ snd $ hb
    where
        vers = fst $ fst $ hb
        typ  = snd $ fst $ hb
        hb = header b

parseFull :: String -> (Int, Int)
parseFull hex = fst $ parse $ tobin $ head $ lines $ hex

solveDay16A :: String -> Int
solveDay16A hex = snd $ parseFull hex
solveDay16B :: String -> Int
solveDay16B hex = fst $ parseFull hex

main = do
    contents <- readFile "input16.txt"
    print $ solveDay16A contents
    print $ solveDay16B contents
