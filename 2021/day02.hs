-- This converts a single String into an Integer
toint :: String -> Integer
toint a = read a :: Integer

-- This takes a ["forward", "20"] and returns ('f', 20)
parseInstruction :: [[Char]] -> (Char, Integer)
parseInstruction [op, s] = (head op, toint s)

-- This takes a tuple and multiplies it
times2 :: (Integer, Integer) -> Integer
times2 (a,b) = a * b
times3 :: (Integer, Integer, Integer) -> Integer
times3 (a,b,c) = a * b


-- This takes an instruction ('f'|'u'|'d', stepszie) and a state
-- (curried!) and returns the new state
applyA :: (Char, Integer) -> (Integer, Integer) -> (Integer, Integer)
applyA ('f', s) (x, y) = (x+s, y)
applyA ('u', s) (x, y) = (x, y-s)
applyA ('d', s) (x, y) = (x, y+s)

solveDay2A ::  [String] -> Integer
solveDay2A instr = times2 $
                   foldr applyA (0,0) $
                   reverse $
                   map (parseInstruction . words) $
                   instr

applyB :: (Char, Integer) -> (Integer, Integer, Integer) -> (Integer, Integer, Integer)
applyB ('f', s) (x, y, a) = (x+s, y-a*s, a)
applyB ('u', s) (x, y, a) = (x, y, a+s)
applyB ('d', s) (x, y, a) = (x, y, a-s)

solveDay2B ::  [String] -> Integer
solveDay2B instr = times3 $
                   foldr applyB (0,0,0) $
                   reverse $
                   map (parseInstruction . words) $
                   instr
main = do
    contents <- readFile "input02.txt"
    print $ solveDay2A $ lines $ contents
    print $ solveDay2B $ lines $ contents
