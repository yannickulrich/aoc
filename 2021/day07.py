txt="16,1,2,0,4,2,7,1,2,14"
txt = open('input07.txt').read().strip()
dat = [int(i) for i in txt.split(',')]

def cost(d):
    return abs(d)
minf = 1000000
mini = 0
for i in range(min(dat), max(dat)+1):
    fuel = sum(cost(i-j) for j in dat)
    if fuel < minf:
        mini = i
        minf = fuel

print(minf)


def cost(d):
    return abs(d)*(abs(d)+1)//2
minf = 1000000000
mini = 0
for i in range(min(dat), max(dat)+1):
    fuel = sum(cost(i-j) for j in dat)
    if fuel < minf:
        mini = i
        minf = fuel

print(minf)
