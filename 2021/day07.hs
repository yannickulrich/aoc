-- This converts a single String into an Integer
toint a = read a :: Integer

loadLine :: String -> [Integer]
loadLine l = map toint $
             words $
             map repl l
    where
        repl ',' = ' '
        repl c = c


costA :: Integer -> Integer
costA d = abs d

fuelCostA :: Integer -> [Integer] -> Integer
fuelCostA trialX origX = sum $
                         map (costA.(\j->trialX - j)) $
                         origX

fuelCostsA :: [Integer] -> [Integer] -> [Integer]
fuelCostsA trials orig = map (\x -> fuelCostA x orig) $
                         trials

solveDay7A :: String -> Integer
solveDay7A line = minimum $
                  fuelCostsA [(minimum t)..(maximum t)] t
    where t = loadLine $ line



costB :: Integer -> Integer
costB d = (\a -> a*(a+1)`div`2) $ abs d

fuelCostB :: Integer -> [Integer] -> Integer
fuelCostB trialX origX = sum $
                         map (costB.(\j->trialX - j)) $
                         origX

fuelCostsB :: [Integer] -> [Integer] -> [Integer]
fuelCostsB trials orig = map (\x -> fuelCostB x orig) $
                         trials

solveDay7B :: String -> Integer
solveDay7B line = minimum $
                  fuelCostsB [(minimum t)..(maximum t)] t
    where t = loadLine $ line


main = do
    contents <- readFile "input07.txt"
    print $ solveDay7A $ head $ lines $ contents
    print $ solveDay7B $ head $ lines $ contents
