import string
import re

txt = open('input13.txt').read()

dots = [
    (int(x), int(y))
    for x,y in re.findall("(\d*),(\d*)", txt)
]
folds = [
    (c, int(x))
    for c,x in re.findall("fold along ([xy])=(\d*)", txt)
]

sizeX = max(x for x,y in dots)+1
sizeY = max(y for x,y in dots)+1

if sizeX % 2 == 0: sizeX += 1
if sizeY % 2 == 0: sizeY += 1

def pprint(m):
    for y in m:
        print("".join(
            "#" if x else "."
            for x in y
        ))


def yfold(mat, v):
    down = mat[:v]
    up = list(reversed(mat[v+1:]))
    z = [list(zip(*i)) for i in zip(down, up)]
    return [[a | b for a, b in y] for y in z]

def xfold(mat, v):
    left = [x[:v] for x in mat]
    right = [list(reversed(x[v+1:])) for x in mat]
    z = [list(zip(*i)) for i in zip(left, right)]
    return [[a | b for a, b in y] for y in z]

def fold(mat, f):
    d, v = f
    if d == 'y':
        mat = yfold(mat, v)
    elif d == 'x':
        mat = xfold(mat, v)
    return mat


mat=[[0 for x in range(sizeX)] for y in range(sizeY)];
for x,y in dots:
    mat[y][x] = True


print(sum(sum(i) for i in fold(mat, folds[0])))

for f in folds:
    mat = fold(mat, f)




txt = open('alphabet.txt').read()
table = [
    [[j == '#' for j in i] for i in l.splitlines()]
    for l in txt.split('\n\n')
]


def decode(mat):
    def decode1(sm):
        return string.ascii_uppercase[table.index(sm)]
    def decoden(n):
        return decode1([i[5*n:5*n+5] for i in mat])
    return ''.join(decoden(i) for i in range(len(mat[0])//5))

print(decode(mat))
