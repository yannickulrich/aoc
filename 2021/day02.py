import re


txt = open('input02.txt').read()

y = 0
x = 0
dat = [(d, int(s)) for d,s in re.findall('([udf]).* (\d+)', txt)]

for d, s in dat:
    if d == 'f':
        x += s
    elif d == 'u':
        y -= s
    elif d == 'd':
        y += s

print(x*y)

# part2
x = 0
y = 0
a = 0

for d, s in dat:
    if d == 'f':
        x += s
        y -= a*s
    elif d == 'u':
        a += s
    elif d == 'd':
        a -= s
print(x*y)
