import Data.List


type State = [((Char,Char),Int)]
type Rule  = ((Char,Char),Char)
parseInsrt :: String -> Rule--(Int, Int, Int)
parseInsrt l = ((co $ l!!0, co $ l!!1), co $ l !! 6)
    where co a = a--fromEnum a - fromEnum 'A'

letters :: String -> [Char]
letters contents = tail $
                   map head $
                   group $
                   sort $
                   map repl $
                   contents
    where
        repl '\n' = ' '
        repl '-' = ' '
        repl '>' = ' '
        repl a = a


countN :: (Eq a) => [a] -> a -> Int
countN as a = fst $ go (0,as)
    where
        go (n,[]) = (n,[])
        go (n,as') | (head $ as') == a  = go (n+1, tail as')
                   | otherwise          = go (n  , tail as')

flatten arr = [y | x<- arr, y <- x]

power :: [Char] -> [(Char,Char)]
power l = flatten $ map (\y -> map (\x -> (x, y)) l) $ l

str2couples :: [Char] -> String -> State
str2couples l templ = zip (power $ l) count
    where count = map (countN all_couples) $ power $ l
          all_couples = sort $ zip (init $ templ) (tail $ templ)


addTo ::(Char,Char) -> Int -> State -> State
addTo key v state = map addTo1 $ state
    where
        addTo1 (kk, vv)  | kk == key  = (kk, vv+v)
                         | otherwise  = (kk, vv)

getInd :: [((Char,Char), a)] -> (Char,Char) -> a
getInd lst k  | (fst $ head lst) == k  = snd $ head lst
              | otherwise              = getInd (tail lst) k

evolve1 :: [Rule] -> State -> ((Char,Char), Int) -> State -> State
evolve1 rules c ((a,b),v) ans = addTo (a,n)   old  $
                                addTo (n,b)   old  $
                                addTo (a,b) (-old) $
                                ans
    where
        n = getInd rules (a,b)
        old = getInd c (a,b)

evolve :: [Rule] -> State -> State
evolve rules state = foldr (evolve1 rules state) state $ state


addToOcc :: Char -> Int -> [(Char, Int)] -> [(Char, Int)]
addToOcc key v state = map addTo1 $ state
    where
        addTo1 (kk, vv)  | kk == key  = (kk, vv+v)
                         | otherwise  = (kk, vv)

score1 :: ((Char,Char), Int) -> [(Char, Int)] -> [(Char, Int)]
score1 ((a,b),v) state = addToOcc a v $
                         addToOcc b v $
                         state

score0 :: [Char] -> (Char, Char) -> [(Char, Int)]
score0 l (i, f) = zip l occ
    where occ = map occIF $ l
          occIF a  | a == i  = 1
                   | a == f  = 1
                   | otherwise = 0

scoren :: [Char] -> [Char] -> State -> [Int]
scoren l templ state = map (\x -> x `div` 2) $
                       map snd $
                       foldr score1 ini $
                       state
    where
        ini = score0 l (head $ templ, last $ templ)

score :: [Char] -> [Char] -> State -> Int
score l templ state = (maximum occ) - (minimum occ)
    where occ = scoren l templ state


solveN :: Int -> String -> Int
solveN n contents = score l templ $
                    (iterate (evolve rules) $
                    str2couples l templ) !! n
    where
        templ = head $ lines $ contents
        l = letters $ contents
        rules = map parseInsrt  $ tail $ tail $ lines $ contents


solveDay14A = solveN 10
solveDay14B = solveN 40

main = do
    contents <- readFile "input14.txt"
    print $ solveDay14A $ contents
    print $ solveDay14B $ contents
