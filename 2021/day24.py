# This implements the ALU. Turns out we won't need it but still cool
# :)
def apply(cmd, args, state):
    if cmd == 'inp':
        state[args[0]] = state['stdin'][state['s']]
        state['s'] += 1
    elif cmd == 'add':
        if type(args[1]) == int:
            state[args[0]] = state[args[0]] + args[1]
        else:
            state[args[0]] = state[args[0]] + state[args[1]]
    elif cmd == 'mul':
        if type(args[1]) == int:
            state[args[0]] = state[args[0]] * args[1]
        else:
            state[args[0]] = state[args[0]] * state[args[1]]
    elif cmd == 'div':
        if type(args[1]) == int:
            state[args[0]] = int(state[args[0]] / args[1])
        else:
            state[args[0]] = int(state[args[0]] / state[args[1]])
    elif cmd == 'mod':
        if type(args[1]) == int:
            state[args[0]] = state[args[0]] % args[1]
        else:
            state[args[0]] = state[args[0]] % state[args[1]]
    elif cmd == 'eql':
        if type(args[1]) == int:
            state[args[0]] = 1 if state[args[0]] == args[1] else 0
        else:
            state[args[0]] = 1 if state[args[0]] == state[args[1]] else 0
    return state


def parse(txt):
    for l in txt:
        ll = l.split(' ')
        args = []
        for i in ll[1:]:
            try:
                args.append(int(i))
            except:
                args.append(i)
        yield (ll[0], args)


def run_program(txt, stdin):
    state = {
        'w': 0, 'x': 0, 'y': 0, 'z': 0,
        's': 0, 'stdin': stdin
    }
    for cmd, args in parse(txt):
        state = apply(cmd, args, state)
    return state


txt = open('input24.txt').read().splitlines()

# The program is the more or less the same block repeated fourteen
# times:
# ```
# inp w
# mul x 0
# add x z
# mod x 26
# div z {a}
# add x {b}
# eql x w
# eql x 0
# mul y 0
# add y 25
# mul y x
# add y 1
# mul z y
# mul y 0
# add y w
# add y {c}
# mul y x
# add z y
# ```
# where `a`, `b`, and `c` are different in each block.
differentLines = [
    i for i in range(18)
    if len(set(txt[i::18])) > 1
]
As=[int(j.split(' ')[-1]) for j in txt[differentLines[0]::18]]
Bs=[int(j.split(' ')[-1]) for j in txt[differentLines[1]::18]]
Cs=[int(j.split(' ')[-1]) for j in txt[differentLines[2]::18]]
# Let's break down the code
# ```python
# inp w
# 
# mul x 0
# add x z
# mod x 26   # x=z%26
# div z {a}  # z //= a
# add x {b}
# eql x w
# eql x 0    # w!=x  -> w != (z%26 + b)
# 
# mul y 0
# add y 25
# mul y x    # y = 25 if w != (z%26 + b) else 0
# add y 1
# mul z y    # z *= 26 if w != (z%26 + b) else 1
# 
# mul y 0
# add y w
# add y {c}  # y = w+c
# mul y x
# add z y    # z += (w+c) if w != (z%26 + b) else 0
# ```
# In other words,
# ```python
# zold = z
# w = input()
# z //= a
# z *= 26 if w != (zold%26 + b) else 1
# z += (w+c) if w != (zold%26 + b) else 0
# ```
# $a$ can have two values, $a=1$ and $a=26$. If $a=1$, we have $10\le
# b\le 14$. Hence, $(z_{\rm odl} {\rm mod} 26) + b > 10$. With $1\le
# w\le 9$ this `w != (zold%26 + b) = True`. Hence,
# ```python
# # if a = 1
# w = input()
# z *= 26
# z += (w+c)
# ```
# This means we can view $z$ as a base-26 number. The $a=1$ blocks add
# a new digit:
# \begin{align}
# z_{\rm old} &= z_3 z_2 z_1 z_0       \\
# z           &= z_3 z_2 z_1 z_0(w+c)
# \end{align}
# To arrive at $z=0$, we need to undo this. This is what the $a=26$
# block is for. Here, $-13\le b\le 0$. Again with
# \begin{align}
# z_{\rm old}   &= z_3 z_2 z_1 z_0
# \end{align}
# The first line
# ```python
# z //= 26
# ```
# removes the least-significant digit
# \begin{align}
# z {\tt //} 26  &= z_3 z_2 z_1
# \end{align}
# The next two lines
# ```python
# z *= 26 if w != (zold%26 + b) else 1
# z += (w+c) if w != (zold%26 + b) else 0
# ```
# re-add a contribution
# \begin{align}
# z\to\begin{cases}
#    z_3z_2z_1(w+c)   & z_0 + b \neq w\\
#    z_3z_2z_1        & \text{otherwise}
# \end{cases}
# \end{align}
# We need to ensure that we are always in the `otherwise` case, i.e.
# $w=z_0+b$.
#
# We now have a number of conditions of the form
# \begin{align}
# w_i + \delta = w_j
# \end{align}
# We now brute-force the remaining seven combinations. Naively, we
# expect $1\le w_i\le 9$. But since $1\le w_j \le 9$, this is reduced
# to
# \begin{align}
# \max(1, 1-\delta) &\le w_i \le \min(9-\delta, 9)
# \end{align}
ind = 0
wj = 0
z = []
ds = {}
ranges = {}
for a,b,c, in zip(As,Bs,Cs):
    if a == 1:
        z.append((wj, c))
    else:
        wi, cold = z.pop()
        d = cold + b
        #print("w[%d] + %d + %d == w[%d]," % (
        #    wi, cold, b, wj
        #))
        ranges[wi] = range(max(1,1-d), min(9-d,9)+1)
        ds[wi] = (wj, d)
    wj += 1


def nest(its):
    if len(its) == 1:
        for i in its[0]:
            yield [i]
    else:
        for i in its[0]:
            for j in nest(its[1:]):
                yield [i] + j


minv = +1e15
maxv = -1e15

for ws in nest(list(ranges.values())):
    inp = 0
    for i, w in zip(ranges.keys(), ws):
        inp += 10**(13-i) * w

        i, d = ds[i]
        inp += 10**(13-i) * (w + d)

    if inp > maxv:
        maxv = inp
    if inp < minv:
        minv = inp

print(maxv)
print(minv)
