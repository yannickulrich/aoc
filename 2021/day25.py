def evolve(dat):
    ok = []

    w = len(dat[0])
    h = len(dat)

    ans = [[i for i in j] for j in dat]

    y = 0
    for x in range(w-1,-1,-1):
        if dat[y][x] == '>':
            if dat[y][(x+1)%w] == '.':
                ok.append((x,y))

    for y in range(h-1,-1,-1):
        for x in range(w-1,-1,-1):
            if dat[y][x] == '>':
                if dat[y][(x+1)%w] == '.':
                    ok.append((x,y))
                    if ans[y][x] == '>':
                        ans[y][x] = '.'
                    ans[y][(x+1)%w] = '>'
            elif dat[y][x] == 'v':
                if dat[(y+1)%h][x] == '.':
                    if ((x-1)%w,(y+1)%h) in ok:
                        if dat[(y+1)%h][(x-1)%w] == '>':
                            continue
                    ok.append((x,y))
                    ans[y][x] = '.'
                    ans[(y+1)%h][x] = 'v'
                elif dat[(y+1)%h][x] == '>':
                    if (x, (y+1)%h) in ok:
                        ok.append((x,y))
                        ans[y][x] = '.'
                        ans[(y+1)%h][x] = 'v'
    return ok,ans


def eEast(e, s, w,h):
    for y,x in e:
        n = ( y, (x+1)%w )
        if n not in e and n not in s:
            yield n
        else:
            yield (y,x)


def eSouth(e, s, w,h):
    for y,x in s:
        n = ( (y+1)%h , x )
        if n not in e and n not in s:
            yield n
        else:
            yield (y,x)


def evolve(state, w,h):
    e,s = state
    e = set(eEast (e,s,w,h))
    s = set(eSouth(e,s,w,h))
    return (e,s) != state, (e,s)


def build_state(dat):
    return (
        set(
            (i,j)
            for i in range(len(dat))
            for j in range(len(dat[0]))
            if dat[i][j] == '>'
        ),
        set(
            (i,j)
            for i in range(len(dat))
            for j in range(len(dat[0]))
            if dat[i][j] == 'v'
        )
    )


def test_easy():
    txts=["""...>...
.......
......>
v.....>
......>
.......
..vvv..""",

"""..vv>..
.......
>......
v.....>
>......
.......
....v..""",

"""....v>.
..vv...
.>.....
......>
v>.....
.......
.......""",
"""......>
..v.v..
..>v...
>......
..>....
v......
.......""",
""">......
..v....
..>.v..
.>.v...
...>...
.......
v......"""]
    txts = [i.splitlines() for i in txts]
    dats = [build_state(t) for t in txts]

    assert evolve(dats[0], len(txts[0][0]), len(txts[0]))[1]==dats[1]
    assert evolve(dats[1], len(txts[0][0]), len(txts[0]))[1]==dats[2]
    assert evolve(dats[2], len(txts[0][0]), len(txts[0]))[1]==dats[3]
    assert evolve(dats[3], len(txts[0][0]), len(txts[0]))[1]==dats[4]


def evolveWhile(dat, *args):
    ok = 1
    c = 0
    while ok:
        #if c%10 == 0: print(c)
        ok, dat = evolve(dat, *args)
        c += 1
    return dat, c


def test_hard():
    txt = """v...>>.vv>
.vv>>.vv..
>>.>v>...v
>>v>>.>.v.
v>v.vv.v..
>.>>..v...
.vv..>.>v.
v.v..>>v.v
....v..v.>"""
    dat = [list(i) for i in txt.splitlines()]
    h = len(dat) ; w = len(dat[0])
    dat = build_state(dat)
    for i in range(50):
        ok, dat = evolve(dat, w, h)
    txt2 = """..>>v>vv.v
..v.>>vv..
v.>>v>>v..
..>>>>>vv.
vvv....>vv
..v....>>>
v>.......>
.vv>....v>
.>v.vv.v.."""
    assert dat == build_state([list(i) for i in txt2.splitlines()])

    dat = [list(i) for i in txt.splitlines()]
    dat = build_state(dat)
    dat, c = evolveWhile(dat, w, h)
    assert c == 58


txt = open('input25.txt').read()
dat = [list(i) for i in txt.splitlines()]
dat, c = evolveWhile(build_state(dat), len(dat[0]), len(dat))
print(c)
