def subsets(s):
    n = len(s)
    for i in range(1<<n):
        yield [s[j] for j in range(n) if i & (1<<j)]

def part1(inp):
    tot = 150
    sizes = [int(i) for i in inp.strip().split("\n")]
    tally = 0
    for subset in subsets(sizes):
        if tot == sum(subset):
            tally += 1
    return tally


def part2(inp):
    tot = 150
    sizes = [int(i) for i in inp.strip().split("\n")]
    dic = {}
    for subset in subsets(sizes):
        if tot == sum(subset):
            try:
                dic[len(subset)] += 1
            except KeyError:
                dic[len(subset)] = 1
    return dic[min(dic.keys())]


if __name__ == "__main__":
    with open('input17.txt') as fp:
        inp = fp.read()

    print(part1(inp))
    print(part2(inp))
