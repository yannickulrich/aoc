import re
# With a bit of fiddling one can work out the closed form
# for the entry at (x,y) as
#    y^2/2 + (x - 3/2) y + 1/2 (x^2 - x + 2)
#
# The inc operator has the form
#    a_n = a_(n-1) * A mod B
# Or in other words
#    a_n = a_0 * A**n mod B
# we can now use the square-multiply algorithm to solve the problem

def part1(inp):
    y, x = [
        int(i)
        for i in re.findall(r"at row (\d*), column (\d*)\.", inp)[0]
    ]

    v = int(y**2/2 + (x - 3/2)*y + 1/2*(x**2 - x + 2)) - 1
    code = 20151125
    base = 252533
    while v:
        if v & 1:
            code = (code * base) % 33554393
        v >>= 1
        base = (base * base) % 33554393

    return code


if __name__ == "__main__":
    with open('input25.txt') as fp:
        inp = fp.read().strip()

    print(part1(inp))
