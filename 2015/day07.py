def parse1(instr):
    lhs, rhs = instr.split(' -> ')
    ops = lhs.split(' ')

    if len(ops) == 1:
        return '=', [ops[0]], rhs
    elif len(ops) == 2 and ops[0] == 'NOT':
        return '!', [ops[1]], rhs
    elif len(ops) == 3:
        return ops[1], [ops[0], ops[2]], rhs


def simulate(inp, bval=None):
    instrs = list(map(parse1, inp.split('\n')))

    wires = {
        i: None
        for _, _, i in instrs
    }
    wires['b'] = bval

    def get(n):
        try:
            return wires[n]
        except KeyError:
            return int(n)


    while instrs:
        for n, (op, lhs, rhs) in enumerate(instrs):
            if all(get(i) is not None for i in lhs):
                if op == '=':
                    wires[rhs] = get(lhs[0])
                elif op == '!' :
                    wires[rhs] = 65535 - get(lhs[0])
                elif op == 'AND':
                    wires[rhs] = get(lhs[0]) & get(lhs[1])
                elif op == 'OR':
                    wires[rhs] = get(lhs[0]) | get(lhs[1])
                elif op == 'RSHIFT':
                    wires[rhs] = get(lhs[0]) >> get(lhs[1])
                elif op == 'LSHIFT':
                    wires[rhs] = get(lhs[0]) << get(lhs[1])
                break
        else:
            raise ValueError()

        instrs.pop(n)

    return wires


def part1(inp):
    return simulate(inp)['a']


def part2(inp):
    aval = simulate(inp)['a']
    return simulate(inp, aval)['a']


if __name__ == "__main__":
    with open('input07.txt') as fp:
        inp = fp.read().strip()

    print(part1(inp))
    print(part2(inp))

