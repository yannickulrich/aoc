def parse(inp):
    return [
        int(line.split(": ")[-1])
        for line in inp.split('\n')
    ]


def play(player, boss):
    player_damage = max(player[1] - boss[2],1)
    boss_damage = max(boss[1] - player[2],1)
    player_hp = player[0]
    boss_hp = boss[0]

    while True:
        boss_hp -= player_damage
        if boss_hp <= 0:
            return True

        player_hp -= boss_damage
        if player_hp <= 0:
            return False


def try_all(boss, op):
    weapons = [
        (4,  8),
        (5, 10),
        (6, 25),
        (7, 40),
        (8, 74),
    ]
    armor = [
        (0,  0),
        (1, 13),
        (2, 31),
        (3, 53),
        (4, 75),
        (5,102),
    ]
    rings = [
        (1, 0,  25),
        (2, 0,  50),
        (3, 0, 100),
        (0, 1,  20),
        (0, 2,  40),
        (0, 3,  80),
    ]

    for w, wc in weapons:
        for a, ac in armor:
            # No rings
            if op(play((100, w, a), boss)):
                yield ac + wc

            # One ring
            for rw, ra, rc in rings:
                if op(play((100, w+rw, a+ra), boss)):
                    yield ac + wc + rc

            # Two rings
            for rw1, ra1, rc1 in rings:
                for rw2, ra2, rc2 in rings:
                    if rc1 == rc2: continue

                    if op(play((100, w+rw1+rw2, a+ra1+ra2), boss)):
                        yield ac + wc + rc1 + rc2


def part1(inp):
    return min(try_all(parse(inp), lambda a: a))

def part2(inp):
    return max(try_all(parse(inp), lambda a: not a))


if __name__ == "__main__":
    with open('input21.txt') as fp:
        inp = fp.read().strip()

    print(part1(inp))
    print(part2(inp))
