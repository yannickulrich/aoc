def parse1(line):
    line = line.strip(".").split(" ")
    A = line[0][0]
    B = line[-1][0]
    if line[2] == "gain":
        return (A, B), int(line[3])
    else:
        return (A, B), -int(line[3])

def parse(inp):
    table = dict(parse1(line) for line in inp.strip().split('\n'))
    people = list(set(i for i,_ in table.keys()))
    return table, people


def permute(head, tail=[]):
    if len(head) == 0:
        yield tail
    else:
        for i in range(len(head)):
            yield from permute(head[:i] + head[i+1:], tail + [head[i]])


def score(table, p):
    tally = 0
    for a, b in zip(p, p[1:] + [p[0]]):
        tally += table[a,b] + table[b,a]
    return tally


def part1(inp):
    table, people = parse(inp)
    return max(score(table, p) for p in permute(people))


def part2(inp):
    table, people = parse(inp)
    for i in people:
        table['X',i] = 0
        table[i,'X'] = 0
    people.append('X')
    return max(score(table, p) for p in permute(people))


if __name__ == "__main__":
    with open('input13.txt') as fp:
        inp = fp.read()

    print(part1(inp))
    print(part2(inp))
