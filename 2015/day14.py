import re
def parse(inp):
    r = (
        r"can fly (\d*) km/s for (\d*) seconds, but then"
        r" must rest for (\d*) seconds."
    )
    return [
        (int(v), int(t), int(r))
        for v, t, r in re.findall(r, inp)
    ]


def dist(v, tr, tp, tmax):
    t = 0
    d = 0
    running = True
    while t < tmax:
        if running:
            t += tr
            d += v*tr
        else:
            t += tp
        running = not running

    if not running:
        d -= (t-tmax)*v
    return d


def part1(inp):
    return max(dist(*i, 2503) for i in parse(inp))


def part2(inp):
    dat = parse(inp)
    state = [[0, dat[i][1], 0] for i in range(len(dat))]
    for t in range(2503):
        leadI = [0]
        leadV = 0
        for i, (v,tr,tp) in zip(range(len(state)), dat):
            if state[i][1] > 0:
                state[i][0] += v
                state[i][1] -= 1

                if state[i][1] == 0:
                    state[i][1] = -tp
            elif state[i][1] < 0:
                state[i][1] += 1
                if state[i][1] == 0:
                    state[i][1] = tr

            if state[i][0] > leadV:
                leadV = state[i][0]
                leadI = [i]
            elif state[i][0] == leadV:
                leadI.append(i)

        for i in leadI:
            state[i][2] += 1

    return max(p for _, _, p in state)


if __name__ == "__main__":
    with open('input14.txt') as fp:
        inp = fp.read()

    print(part1(inp))
    print(part2(inp))
