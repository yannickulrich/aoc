def part1(inp):
    nc = 0
    nr = 0
    for line in inp.split("\n"):
        assert line[0] == "\""
        assert line[-1] == "\""
        it = iter(line[1:-1])
        nc += len(line)
        try:
            while True:
                c = next(it)
                if c == '\\':
                    c = next(it)
                    if c == '\\':
                        pass
                    elif c == '\"':
                        pass
                    elif c == 'x':
                        h = [next(it), next(it)]
                    else:
                        print(c)
                    nr += 1
                else:
                    nr += 1
        except StopIteration:
            pass
    return nc-nr


def part2(inp):
    nc = 0
    nr = 0
    for line in inp.split("\n"):
        assert line[0] == "\""
        assert line[-1] == "\""
        it = iter(line)
        nc += len(line)
        nr += 2
        try:
            while True:
                c = next(it)
                if c == '\\':
                    nr += 2
                elif c == '"':
                    nr += 2
                else:
                    nr += 1
        except StopIteration:
            pass
    return nr-nc

if __name__ == "__main__":
    with open('input08.txt') as fp:
        inp = fp.read().strip()

    print(part1(inp))
    print(part2(inp))

