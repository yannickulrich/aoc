import re
import random

def parse_molecule(str):
    return re.findall("[A-Z][a-z]*", str)

def parse(inp):
    def parse1(s, t):
        return s, parse_molecule(t)

    repl, tgt = inp.strip().split('\n\n')
    tgt = parse_molecule(tgt)
    repl = [
        parse1(*i.split(' => '))
        for i in repl.split("\n")
    ]
    return repl, tgt


def part1(inp):
    repl, tgt = parse(inp)
    ans = set()
    for i in range(len(tgt)):
        # attempting to replace the ith element
        for s, t in repl:
            if s == tgt[i]:
                ans.add(tuple(tgt[:i] + t + tgt[i+1:]))
    return len(ans)


def myindex(hay, needle):
    for i in range(len(hay) - len(needle) + 1):
        if hay[i:i+len(needle)] == needle:
            return i
    return -1


def part2(inp):
    repl, tgt0 = parse(inp)
    tgt = "".join(tgt0)
    ans = 0
    while len(tgt) > 1:
        tmp = tgt
        for s, t in repl:
            if "".join(t) in tgt:
                tgt = tgt.replace("".join(t), s, 1)
                ans += 1
        if tmp == tgt:
            ans = 0
            tgt = "".join(tgt0)
            random.shuffle(repl)
    return ans


if __name__ == "__main__":
    with open('input19.txt') as fp:
        inp = fp.read()

    print(part1(inp))
    print(part2(inp))
