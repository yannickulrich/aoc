def parse(inp):
    for i in inp.replace(',', '').split('\n'):
        instr = i.split(' ')
        if len(instr) == 3:
            yield ((instr[0], int(instr[2])), instr[1])
        elif instr[0] == 'jmp':
            yield ((instr[0], int(instr[1])), None)
        else:
            yield (instr[0], instr[1])


def run_program(instrs, regs):
    ip = 0

    while ip < len(instrs):
        instr, reg = instrs[ip]
        if instr == 'hlf':
            regs[reg] //= 2
            ip += 1
        elif instr == 'tpl':
            regs[reg] *= 3
            ip += 1
        elif instr == 'inc':
            regs[reg] += 1
            ip += 1
        elif instr[0] == 'jmp':
            ip += instr[1]
        elif instr[0] == 'jie':
            if regs[reg] % 2 == 0:
                ip += instr[1]
            else:
                ip += 1
        elif instr[0] == 'jio':
            if regs[reg] == 1:
                ip += instr[1]
            else:
                ip += 1
        else:
            print(instr)
            assert False

    return regs


def part1(inp):
    return run_program(list(parse(inp)), {'a': 0, 'b': 0})['b']
def part2(inp):
    return run_program(list(parse(inp)), {'a': 1, 'b': 0})['b']


if __name__ == "__main__":
    with open('input23.txt') as fp:
        inp = fp.read().strip()

    print(part1(inp))
    print(part2(inp))
