import re


def test_string1(inp):
    bad = set(['ab', 'cd', 'pq', 'xy'])
    vowels = set('aeiou')
    inp2 = inp + ' '
    vowel = 0
    twin = False
    for i, j in zip(inp2, inp2[1:]):
        if i+j in bad:
            return False
        if i == j:
            twin = True
        if i in vowels:
            vowel += 1

    return twin and vowel >= 3


def part1(inp):
    return sum(map(test_string1, inp.split('\n')))


r1 = re.compile(r'(..).*\1')
r2 = re.compile(r'(.).\1')
def test_string2(inp):
    if r1.findall(inp) and r2.findall(inp):
        return True
    else:
        return False


def part2(inp):
    return sum(map(test_string2, inp.split('\n')))


if __name__ == "__main__":
    with open('input05.txt') as fp:
        inp = fp.read().strip()

    print(part1(inp))
    print(part2(inp))
