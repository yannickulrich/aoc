def look_and_say(seq):
    ans = ""
    n = 1
    cur = seq[0]
    for i in (seq[1:] + " "):
        if i == cur:
            n += 1
        else:
            ans += str(n) + cur
            n = 1
            cur = i
    return ans


def part1(inp):
    for i in range(40):
        inp = look_and_say(inp)
    return len(inp)


def part2(inp):
    for i in range(50):
        inp = look_and_say(inp)
    return len(inp)


if __name__ == "__main__":
    with open('input10.txt') as fp:
        inp = fp.read().strip()

    print(part1(inp))
    print(part2(inp))
