def parse_coord(s):
    return (int(i) for i in s.split(','))


def solve(inp, ini, funcs):
    grid = [[ini for _ in range(1000)] for _ in range(1000)]

    for line in inp.split('\n'):
        words = line.split(' ')
        l, t = parse_coord(words[-3])
        r, b = parse_coord(words[-1])
        if line.startswith("turn on"):
            func = funcs[0]
        elif line.startswith("turn off"):
            func = funcs[1]
        elif line.startswith("toggle"):
            func = funcs[2]

        for y in range(t, b+1):
            for x in range(l, r+1):
                grid[y][x] = func(grid[y][x])

    return sum(sum(i) for i in grid)


def part1(inp):
    return solve(inp, False, [
        lambda a: True,
        lambda a: False,
        lambda a: not a
    ])


def part2(inp):
    return solve(inp, 0, [
        lambda a: a+1,
        lambda a: max(a-1, 0),
        lambda a: a+2
    ])


if __name__ == "__main__":
    with open('input06.txt') as fp:
        inp = fp.read().strip()

    print(part1(inp))
    print(part2(inp))
