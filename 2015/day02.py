def parse(line):
    return tuple([int(i) for i in line.strip().split('x')])


def area(l, w, h):
    sides = [2*l*w, 2*w*h, 2*h*l]
    return sum(sides) + min(sides) // 2


def part1(inp):
    return sum(
        area(*parse(line))
        for line in inp.strip().split('\n')
    )


def ribbon(l, w, h):
    sides = [l, w, h]
    sides.sort()
    return 2*(sides[0] + sides[1]) + l*w*h


def part2(inp):
    return sum(
        ribbon(*parse(line))
        for line in inp.strip().split('\n')
    )


if __name__ == "__main__":
    with open('input02.txt') as fp:
        inp = fp.read()

    print(part1(inp))
    print(part2(inp))

