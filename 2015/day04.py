import hashlib


def solve(inp, n):
    i = 0
    while True:
        h = hashlib.md5(f"{inp}{i}".encode()).hexdigest()
        if h[:n] == '0'*n:
            return i
        i += 1


def part1(inp):
    return solve(inp, 5)


def part2(inp):
    return solve(inp, 6)


if __name__ == "__main__":
    with open('input04.txt') as fp:
        inp = fp.read().strip()

    print(part1(inp))
    print(part2(inp))

