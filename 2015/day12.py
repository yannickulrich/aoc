import json

def part1(inp):
    def func(x):
        if isinstance(x, dict):
            return sum(func(i) for i in x.values())
        elif isinstance(x, list):
            return sum(func(i) for i in x)
        elif isinstance(x, int):
            return x
        elif isinstance(x, str):
            return 0

    dat = json.loads(inp)
    return func(dat)


def part2(inp):
    def func(x):
        if isinstance(x, dict):
            if "red" in x.keys() or "red" in x.values():
                return 0
            return sum(func(i) for i in x.values())
        elif isinstance(x, list):
            return sum(func(i) for i in x)
        elif isinstance(x, int):
            return x
        elif isinstance(x, str):
            return 0

    dat = json.loads(inp)
    return func(dat)


if __name__ == "__main__":
    with open('input12.txt') as fp:
        inp = fp.read().strip()

    print(part1(inp))
    print(part2(inp))
