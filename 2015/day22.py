import sys
from copy import deepcopy as copy

def parse(inp):
    return [int(i.split(": ")[1]) for i in inp.split("\n")]


class Effect:
    cost: int
    duration: int
    time: int

    def __init__(self):
        self.time = self.duration

    def finish(self, state):
        pass

    def __repr__(self):
        return f"<{self.__class__.__name__} {self.time} of {self.duration}>"


class Missile(Effect):
    cost = 53
    duration = 0

    def apply(self, state):
        self.time -= 1
        state.boss_hp -= 4
        return self


class Drain(Effect):
    cost = 73
    duration = 0

    def apply(self, state):
        self.time -= 1
        state.boss_hp -= 2
        state.player_hp += 2
        return self


class Shield(Effect):
    cost = 113
    duration = 6

    def apply(self, state):
        if self.time == self.duration:
            state.boss_damage -= 7
        self.time -= 1
        return self

    def finish(self, state):
        state.boss_damage += 7


class Poison(Effect):
    cost = 173
    duration = 6

    def apply(self, state):
        self.time -= 1
        state.boss_hp -= 3
        return self


class Recharge(Effect):
    cost = 229
    duration = 5

    def apply(self, state):
        self.time -= 1
        state.mana += 101
        return self


class State:
    time: int
    player_hp: int
    boss_hp: int
    mana: int
    boss_damage: int
    active: list[Effect]
    tally: int

    def __init__(self, player_hp, boss_hp, boss_damage, mana):
        self.time = 0
        self.tally = 0
        self.player_hp = player_hp
        self.boss_hp = boss_hp
        self.boss_damage = boss_damage
        self.mana = mana
        self.active = []

    def __repr__(self):
        return f"<State t={self.time}, player: {self.player_hp}/{self.mana}, boss:{self.boss_hp}>"


def search(state, penalty):
    stack = [state]
    best = 1000000

    while stack:
        state = stack.pop()
        if state.tally > best:
            continue

        state.time += 1
        if state.time % 2 == 1:
            state.player_hp -= penalty

            if state.player_hp <= 0:
                continue

        new_active = []
        for effect in state.active:
            effect.apply(state)
            if effect.time > 0:
                new_active.append(effect)
            else:
                effect.finish(state)
        state.active = new_active

        if state.boss_hp <= 0:
            if state.tally < best:
                best = state.tally
                continue

        if state.time % 2 == 1:
            # player's turn
            for effect in Effect.__subclasses__():
                if effect in [e.__class__ for e in state.active if e.time > 0]:
                    continue

                new = copy(state)
                new.active.append(effect())
                new.mana -= effect.cost
                if new.mana <= 0:
                    continue
                new.tally += effect.cost
                stack.append(new)
        else:
            # boss' turn
            state.player_hp -= state.boss_damage
            if state.player_hp <= 0:
                continue
            stack.append(state)

    return best


def part1(inp):
    state = State(50, *parse(inp), 500)
    # state = State(10, 14, 8, 250)
    return search(state, 0)


def part2(inp):
    state = State(50, *parse(inp), 500)
    # state = State(10, 14, 8, 250)
    return search(state, 1)


if __name__ == "__main__":
    with open("input22.txt") as fp:
        inp = fp.read().strip()

    print(part1(inp))
    print(part2(inp))
