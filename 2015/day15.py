import re
import functools

def parse(inp, func):
    paras = re.findall("capacity ([\d-]*), durability ([\d-]*), flavor ([\d-]*), texture ([\d-]*), calories ([\d-]*)", inp)
    return [tuple(int(i) for i in func(j)) for j in paras]


def mult1(a: tuple, b: int):
    return tuple(i*b for i in a)

def nsum(a):
    s = sum(a)
    return s if s > 0 else 0

def part1(inp):
    paras = parse(inp, lambda j: j[:-1])

    max = 0
    for a in range(100):
        for b in range(100-a):
            for c in range(100-a-b):
                d = 100-a-b-c

                scores = map(nsum, zip(*(mult1(i,j) for i,j in zip(paras,[a,b,c,d]))))
                score = functools.reduce(lambda x,y: x*y, scores)
                if score > max:
                    max = score
    return max

def part2(inp):
    paras = parse(inp, lambda j: reversed(j))

    max = 0
    for a in range(100):
        for b in range(100-a):
            for c in range(100-a-b):
                d = 100-a-b-c

                scores = map(nsum, zip(*(mult1(i,j) for i,j in zip(paras,[a,b,c,d]))))
                cal = next(scores)
                if cal != 500:
                    continue

                score = functools.reduce(lambda x,y: x*y, scores)
                if score > max:
                    max = score
    return max


if __name__ == "__main__":
    with open('input15.txt') as fp:
        inp = fp.read()

    print(part1(inp))
    print(part2(inp))
