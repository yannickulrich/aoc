def str2int(p):
    return sum(
        26**(7-n) * (ord(i)-ord('a'))
        for n, i in enumerate(p)
    )

def int2str(i):
    ans = ""
    while i > 0:
        ans = chr(i%26 + ord('a')) + ans
        i //= 26
    return ans


def valid(p):
    i = j = -1
    pair = None
    seq = False

    k = p % 26
    while p > 0:
        if k == 8 or k == 11 or k == 14:
            return False
        if j == k:
            if pair is None:
                pair = j
            elif pair == j:
                pass
            else:
                break
        if i-1 == j and j-1 == k:
            seq = True

        p //= 26
        i, j, k = j, k, p % 26
    else:
        return False

    return seq


def next_password(p):
    while not valid(p):
        p += 1
    return p


def part1(inp):
    return int2str(next_password(str2int(inp)))


def part2(inp):
    return int2str(next_password(1+next_password(str2int(inp))))


if __name__ == "__main__":
    with open('input11.txt') as fp:
        inp = fp.read().strip()

    print(part1(inp))
    print(part2(inp))

