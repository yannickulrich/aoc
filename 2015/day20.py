def sum_divisors(n):
    ans = 0
    i = 1
    while i*i <= n:
        if n % i == 0:
            ans += i
            j = n//i
            if j != i:
                ans += j
        i += 1

    return ans


def part1(inp):
    v = int(inp) // 10
    vvmax = 0
    h = 1
    while True:
        vv = sum_divisors(h)
        if vv > vvmax:
            print(h, vv)
            vvmax = vv
        if v <= vv:
            return h
        h += 1


def part1_b(inp):
    v = int(inp) // 10
    arr = [0 for i in range(v)]
    for i in range(1, v+1):
        for j in range(i, v+1, i):
            arr[j-1] += i
        if arr[i-1] >= v:
            return i


def part2(inp):
    v = int(inp) // 11
    arr = [0 for i in range(v)]
    for i in range(1, v+1):
        for n,j in enumerate(range(i, v+1, i)):
            if n >= 50: break
            arr[j-1] += i
        if arr[i-1] >= v:
            return i


if __name__ == "__main__":
    with open('input20.txt') as fp:
        inp = fp.read().strip()

    # print(part1(inp))
    print(part1_b(inp))
    print(part2(inp))
