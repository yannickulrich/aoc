def parse(inp):
    return [
        [
            i == '#'
            for i in line
        ] for line in inp.split('\n')
    ]


def evolve(dat, part):
    w = len(dat[0])
    h = len(dat)

    def get(i, j):
        if i < 0: return False
        if j < 0: return False
        if i >= w: return False
        if j >= h: return False
        return dat[j][i]

    def neighbours(i, j):
        yield get(i-1, j-1)
        yield get(i-1, j  )
        yield get(i-1, j+1)
        yield get(i  , j-1)
        yield get(i  , j+1)
        yield get(i+1, j-1)
        yield get(i+1, j  )
        yield get(i+1, j+1)

    def evolve1(i, j):
        n = sum(neighbours(i, j))
        if part == 2:
            if i == 0 and j == 0:
                return True
            if i == w-1 and j == 0:
                return True
            if i == 0 and j == h-1:
                return True
            if i == w-1 and j == h-1:
                return True

        if dat[j][i]:
            if n == 2 or n == 3:
                return True
            else:
                return False
        else:
            if n == 3:
                return True
            else:
                return False

    return [[evolve1(i, j) for i in range(w)] for j in range(h)]



def pprint(dat):
    return "\n".join(
        "".join('#' if i else '.' for i in j)
        for j in dat
    )


def calc(inp, part):
    dat = parse(inp)
    for i in range(100):
        dat = evolve(dat, part)

    return sum(sum(i) for i in dat)


def part1(inp):
    return calc(inp, 1)

def part2(inp):
    return calc(inp, 2)

if __name__ == "__main__":
    with open('input18.txt') as fp:
        inp = fp.read().strip()

    print(part1(inp))
    print(part2(inp))
