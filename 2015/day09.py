def parse(inp):
    for i in inp.split('\n'):
        points, d = i.split(' = ')
        f, t = points.split(' to ')
        yield (f, t), int(d)
        yield (t, f), int(d)


def permute(head, tail=[]):
    if len(head) == 0:
        yield tail
    else:
        for i in range(len(head)):
            yield from permute(head[:i] + head[i+1:], tail + [head[i]])


def distance(inp, op, d0):
    map = dict(parse(inp))
    places = list(set(i for i, _ in map.keys()))

    df = d0
    for opts in permute(places):
        # check only one ordering
        if id(opts[0]) > id(opts[-1]):
            continue
        d = sum(map[i,j] for i, j in zip(opts, opts[1:]))
        df = op(df, d)

    return df


def part1(inp):
    return distance(inp, min, 1<<100)


def part2(inp):
    return distance(inp, max, 0)


if __name__ == "__main__":
    with open('input09.txt') as fp:
        inp = fp.read().strip()

    print(part1(inp))
    print(part2(inp))
