ops = {
    "^": (0,+1),
    "v": (0,-1),
    "<": (-1,0),
    ">": (+1,0),
}


def part1(inp):
    visited = set([(0,0)])
    x = y = 0
    for o in inp:
        dx, dy = ops[o]
        x += dx ; y += dy
        visited.add((x,y))

    return len(visited)


def part2(inp):
    visited = set([(0,0)])
    x1 = y1 = 0
    x2 = y2 = 0
    for o1, o2 in zip(inp[::2], inp[1::2]):
        dx1, dy1 = ops[o1]
        x1 += dx1 ; y1 += dy1
        visited.add((x1,y1))

        dx2, dy2 = ops[o2]
        x2 += dx2 ; y2 += dy2
        visited.add((x2,y2))

    return len(visited)


if __name__ == "__main__":
    with open('input03.txt') as fp:
        inp = fp.read().strip()

    print(part1(inp))
    print(part2(inp))
