# 2015

This is (hopefully) educational commentary for the 2015 problems

## Day 1
 * topics: tallying
 * algorithms: -
 * tips: play with `enumerate`

## Day 2
 * topics: tallying
 * algorithms: can use `list.sort` for a three-element list
 * tips: play with `func(*args)`

## Day 3
 * topic: unique sets
 * algorithms: hashset
 * tips: use `set` and `set.add`

## Day 4
 * topics: MD5 hash colissions
 * algorithms: brute force
 * tips: `hashlib.md5.hexdigest`

## Day 5
 * topics: string pattern matching
 * algorithms: nothing special needed but
 * tips: regular expression & hashsets

## Day 6
 * topics: applying functions over grid
 * algorithms: nothing special
 * tips: use a function pointer for the action

## Day 7
 * topics: logic gates
 * algorithms: solve system of equations
 * tips: find a good symbolic representation of the instructions and eliminate solved equations until you're done

## Day 8
 * topics: string encoding & escape sequences
 * algorithms: nothing special
 * tips: play with consuming iterators

## Day 9
 * topic: travelling salesman
 * algorithms: many options, somewhat clever brute force possible
 * tips: use `yield` and `yield from` to generate options and check only `d(a,b)` and not `d(b,a)`

## Day 10
 * topic: look and say sequence
 * algorithms: brute force
 * tips: you need to be a bit clever as the length grows as *L*<sub>*n*</sub> ~ 1.3<sup>*n*<sup>.

## Day 11
 * topic: string pattern matching
 * algorithms: clever brute force solution with modular arithmetic
 * tips: don't work on strings, convert the password to a number that you can increment. To look at individual characters, use the `%` and `//` operators

## Day 12
 * topic: map over a JSON objects
 * algorithms: recursion
 * tips: recurse and reduce with a clever function over the list

## Day 13
 * topic: permutations
 * algorithms: brute force
 * tips: you have to check 8! = 40320 combinations which is fairly quick

## Day 14
 * topic: time evolutions
 * algorithm: fiddly stuff
 * tips: define a state and evolve it through time. go in steps for part a

## Day 15
 * topic: constrained optimisation
 * algorithm: brute force
 * tips: this is pretty straightforward, have fun with iterators and folding

## Day 16
 * topic: solve constraint problem
 * algorithm: filtering
 * tips: make a list of all options and filter those that don't work

## Day 17
 * topic: knapsack problem
 * algorithms: size is small enough for brute force, maybe explore dynamic programming
 * tips: calculate the powerset using binary masks, check every element (use dictionaries for part 2)

## Day 18
 * topic: cellular automata
 * algorithms: nothing special
 * tips: build a time-evolution function and tweak it for part 2

## Day 19
 * topic: iterative string replacement
 * algorithms: working backwards & non-deterministic programming
 * tips: part 1 is trivial, for part 2 work backwards and shuffle the replacements whenever you're stuck

## Day 20
 * topic: divisors & totient functions
 * algorithms: nested loops with `i in range(1, v)` and `j in range(i, v)`
 * tips: don't think about divisors, just walk along the street until you find a match, abort early

## Day 21
 * topics: D&D
 * algorithms: nested loops & generators
 * tips: build a generator to try efficiently generate all moves, split by number of rings

## Day 23
 * topic: Turing machines
 * algorithms: simulate a Turing machine
 * tips: dictionary for register, move along a list with an instruction pointer

## Day 25
 * topic: completing a numeric pattern
 * algorithms: square-multiply
 * tips: find a closed form for number at position `x,y`, use square-multiply to find the answer
