def part1(inp):
    c = 0
    for i in inp:
        if i == '(':
            c += 1
        elif i == ')':
            c -= 1
    return c


def part2(inp):
    c = 0
    for n, i in enumerate(inp):
        if i == '(':
            c += 1
        elif i == ')':
            c -= 1

        if c < 0:
            return n + 1


if __name__ == "__main__":
    with open('input01.txt') as fp:
        inp = fp.read()

    print(part1(inp))
    print(part2(inp))
