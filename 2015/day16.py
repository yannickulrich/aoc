extra = """children: 3
cats: 7
samoyeds: 2
pomeranians: 3
akitas: 0
vizslas: 0
goldfish: 5
trees: 3
cars: 2
perfumes: 1"""


def parse(inp):
    for line in inp.split('\n'):
        _, a = line.split(": ", 1)
        ans = {}
        for i in a.split(", "):
            k, v = i.split(': ')
            ans[k] = int(v)
        yield ans


def part1(inp):
    q = {}
    for i in extra.split('\n'):
        k, v = i.split(': ')
        q[k] = int(v)

    for n, d in enumerate(parse(inp)):
        for k, v in d.items():
            if q[k] != v:
                break
        else:
            return n + 1


def calc(inp, cmp):
    q = {}
    for i in extra.split('\n'):
        k, v = i.split(': ')
        q[k] = int(v)

    for n, d in enumerate(parse(inp)):
        for k, v in d.items():
            if not cmp[k](q[k], v):
                break
        else:
            return n + 1


def part1(inp):
    return calc(
        inp, {
            "children": lambda a, b: a==b,
            "cats": lambda a, b: a==b,
            "samoyeds": lambda a, b: a==b,
            "pomeranians": lambda a, b: a==b,
            "akitas": lambda a, b: a==b,
            "vizslas": lambda a, b: a==b,
            "goldfish": lambda a, b: a==b,
            "trees": lambda a, b: a==b,
            "cars": lambda a, b: a==b,
            "perfumes": lambda a, b: a==b,
        }
    )


def part2(inp):
    return calc(
        inp, {
            "trees": lambda a, b: a < b,
            "cats": lambda a, b: a < b,
            "pomeranians": lambda a, b: a > b,
            "goldfish": lambda a, b: a > b,
            "children": lambda a, b: a==b,
            "samoyeds": lambda a, b: a==b,
            "akitas": lambda a, b: a==b,
            "vizslas": lambda a, b: a==b,
            "cars": lambda a, b: a==b,
            "perfumes": lambda a, b: a==b,
        }
    )


if __name__ == "__main__":
    with open('input16.txt') as fp:
        inp = fp.read().strip()

    print(part1(inp))
    print(part2(inp))
