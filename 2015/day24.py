import math
def parse(inp):
    return [int(i) for i in inp.strip().split("\n")]


def combinations(lst, n):
    if n == 0:
        yield ()
    else:
        for i in range(len(lst)):
            sublst = lst[:i]
            for sub in combinations(sublst, n-1):
                yield sub + (lst[i],)


def solve(inp, n):
    # we need to find the smallest subset of the input that gives the correct
    # sum
    dat = parse(inp)
    tgt_sum = sum(dat) // n
    for n in range(1, len(dat) + 1):
        # make a list of combinations with n elements that add to the target sum
        qe = 1000000000000000000
        for attempt in combinations(dat, n):
            if sum(attempt) == tgt_sum:
                new_qe = math.prod(attempt)
                if new_qe < qe:
                    qe = new_qe

        if qe < 1000000000000000000:
            return qe


def part1(inp):
    return solve(inp, 3)

def part2(inp):
    return solve(inp, 4)


if __name__ == "__main__":
    with open('input24.txt') as fp:
        inp = fp.read()

    print(part1(inp))
    print(part2(inp))
