package main

import (
	"fmt"
	"os"
	"slices"
	"strconv"
	"strings"
)

type Operation struct {
	op1, op2, tgt string
	operand rune
}

func (o Operation) String() string {
	return fmt.Sprintf("%s %c %s -> %s", o.op1, o.operand, o.op2, o.tgt)
}

func (o Operation) Exec(a, b bool) bool {
	if o.operand == '^' {
		return a != b
	} else if o.operand == '&' {
		return a && b
	} else if o.operand == '|' {
		return a || b
	} else {
		panic("invalid operand")
	}
}


func parse() (map[string]bool, []Operation, error) {
	readFile, err := os.ReadFile("input24.txt")
	if err != nil {
		return nil, nil, err
	}

	blocks := strings.Split(string(readFile), "\n\n")

	initials := strings.Split(blocks[0],"\n")
	state := make(map[string]bool, len(initials))
	for _, line := range initials {
		l := strings.Split(line, ": ")
		state[l[0]] = (l[1] == "1")
	}

	ops := strings.Split(blocks[1], "\n")
	operations := make([]Operation, len(ops)-1)
	for i, line := range ops[:len(operations)] {
		l := strings.Split(line, " ")
		op := '?'
		if l[1] == "XOR" {
			op = '^'
		} else if l[1] == "AND" {
			op = '&'
		} else if l[1] == "OR" {
			op = '|'
		}
		operations[i] = Operation{l[0], l[2], l[4], op}
	}

	return state, operations, nil
}

func simulate(extstate map[string]bool, operations []Operation) map[string]bool {
	state := make(map[string]bool, len(extstate))
	for k,v := range extstate {
		state[k] = v
	}

	has_changed := true

	for has_changed {
		has_changed = false
		for _, op := range operations {
			if v1, ok := state[op.op1] ; ok {
				if v2, ok := state[op.op2] ; ok {
					if _, ok := state[op.tgt] ; !ok {
						state[op.tgt] = op.Exec(v1, v2)
						has_changed = true
					}
				}
			}
		}
	}

	return state
}

func bits2int(state map[string]bool, prefix byte) int {
	ans := 0
	for k, v := range state {
		if k[0] == prefix {
			num, err := strconv.Atoi(k[1:])
			if err != nil {
				return 0
			}
			if v {
				ans |= (1<<num)
			}
		}
	}

	return ans
}

func int2bits(state *map[string]bool, prefix byte, val int) {
	for i := 0 ; i < 50; i++ {
		(*state)[fmt.Sprintf("%c%02d", prefix, i)] = (val & 1) == 1
		val = val >> 1
	}
}

func part1() (int, error) {
	state, operations, err := parse()
	if err != nil {
		return 0, err
	}

	state = simulate(state, operations)
	return bits2int(state, 'z'), nil
}

func tograph(operations []Operation) {
	fmt.Fprintln(os.Stderr, "Digraph G{")
	for _, op := range operations {
		c := "black"
		if op.operand == '&' {
			c = "red"
		} else if op.operand == '|' {
			c = "blue"
		} else if op.operand == '^' {
			c = "green"
		}
		fmt.Fprintf(os.Stderr, "%s -> %s [name = \"%c\", color=%s]\n", op.op1, op.tgt, op.operand, c)
		fmt.Fprintf(os.Stderr, "%s -> %s [name = \"%c\", color=%s]\n", op.op2, op.tgt, op.operand, c)
	}
	fmt.Fprintln(os.Stderr,"}")
}

func swap(swaps *[]string, operations *[]Operation, a string, b string) {
	*swaps = append(*swaps, a, b)

	for i, v := range *operations {
		if v.tgt == a {
			for j, w := range *operations {
				if w.tgt == b {
					(*operations)[i].tgt = b
					(*operations)[j].tgt = a
					return
				}
			}
		}
	}
}

func part2() (string, error) {
	state, operations, err := parse()
	if err != nil {
		return "", err
	}

	swaps := make([]string, 0, 8)

	// int2bits(&state, 'x', 433930452433)
	// int2bits(&state, 'y', 295830849545)

	swap(&swaps, &operations, "svm", "nbc")
	swap(&swaps, &operations, "kqk", "z15")
	swap(&swaps, &operations, "cgq", "z23")
	swap(&swaps, &operations, "fnr", "z39")
	tograph(operations)

	x := bits2int(state, 'x')
	y := bits2int(state, 'y')
	tgt := x+y
	if tgt != bits2int(simulate(state, operations), 'z') {
		fmt.Printf("%d + %d = %d != %d\n", x,y, tgt, bits2int(simulate(state, operations), 'z'))
		panic("not good enough")
	}
	slices.SortFunc(swaps, strings.Compare)
	return strings.Join(swaps, ","), nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}

