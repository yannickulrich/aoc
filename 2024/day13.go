package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
)

func solve(shift int) (int, error) {
	readFile, err := os.ReadFile("input13.txt")
	if err != nil {
		return 0, err
	}
	content := string(readFile)
	la := regexp.MustCompile(`Button A: X\+(\d*), Y\+(\d*)`).FindAllStringSubmatch(content, -1)
	lb := regexp.MustCompile(`Button B: X\+(\d*), Y\+(\d*)`).FindAllStringSubmatch(content, -1)
	lp := regexp.MustCompile(`Prize: X=(\d*), Y=(\d*)`).FindAllStringSubmatch(content, -1)

	tally := 0
	for i := range la {
		xa, err := strconv.Atoi(la[i][1])
		if err != nil {
			return 0, err
		}
		ya, err := strconv.Atoi(la[i][2])
		if err != nil {
			return 0, err
		}
		xb, err := strconv.Atoi(lb[i][1])
		if err != nil {
			return 0, err
		}
		yb, err := strconv.Atoi(lb[i][2])
		if err != nil {
			return 0, err
		}
		xp, err := strconv.Atoi(lp[i][1])
		if err != nil {
			return 0, err
		}
		yp, err := strconv.Atoi(lp[i][2])
		if err != nil {
			return 0, err
		}

		xp += shift
		yp += shift

		numa := xp*yb - xb*yp
		dena := xa*yb - xb*ya
		numb := xp*ya - xa*yp
		denb := xb*ya - xa*yb

		if numa % dena == 0 && numb % denb == 0 {
			a := numa/dena
			b := numb/denb
			tally += 3*a+b
		}
	}
	return tally, nil
}

func part1() (int, error) {
	v, err := solve(0)
	return v, err
}

func part2() (int, error) {
	v, err := solve(10000000000000)
	return v, err
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}
