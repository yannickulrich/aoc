package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"reflect"
)

func parse() ([]int, [][]int, error) {
	readFile, err := os.ReadFile("input17.txt")
	if err != nil {
		return nil, nil, err
	}
	blocks := strings.Split(string(readFile), "\n\n")
	registers := strings.Split(blocks[0],"\n")

	regs := make([]int, 3)
	for i, v := range registers {
		regs[i], err = strconv.Atoi(strings.Split(v, ": ")[1])
		if err != nil {
			return nil, nil, err
		}
	}

	operations := strings.Split(strings.TrimSpace(blocks[1]), "\n")
	progs := make([][]int, len(operations))
	for j, ops := range operations {
		o := strings.Split(ops[9:], ",")
		progs[j] = make([]int, len(o))
		for i, v := range o {
			progs[j][i], err = strconv.Atoi(v)
			if err != nil {
				return nil, nil, err
			}
		}
	}

	return regs, progs, nil
}

func Run(regs, prog []int) []int {
	var combo = func(data int) int {
		if 0 <= data && data <= 3 {
			return data
		} else if 4 <= data && data <= 6 {
			return regs[data-4]
		} else {
			panic("invalid data")
		}
	}

	pc := 0
	output := make([]int, 0)
	for pc < len(prog) {
		opcode := prog[pc]
		data := prog[pc+1]

		if opcode == 0 {
			// adv
			regs[0] = regs[0] / (1 << combo(data));
			pc += 2
		} else if opcode == 1 {
			// bxl
			regs[1] = regs[1] ^ data
			pc += 2
		} else if opcode == 2 {
			// bst
			regs[1] = combo(data) % 8
			pc += 2
		} else if opcode == 3 {
			// jnz
			if regs[0] == 0 {
				pc += 2
			} else {
				pc = data
			}
		} else if opcode == 4 {
			// bxc
			regs[1] = regs[1] ^ regs[2]
			pc += 2
		} else if opcode == 5 {
			// out
			output = append(output, combo(data) % 8)
			pc += 2
		} else if opcode == 6 {
			// bdv
			regs[1] = regs[0] / (1 << combo(data));
			pc += 2
		} else if opcode == 7 {
			// cdv
			regs[2] = regs[0] / (1 << combo(data));
			pc += 2
		}
	}
	return output
}

func part1() (string, error) {
	regs, prog, err := parse()
	if err != nil {
		return "", err
	}

	output := Run(regs, prog[0])
	sout := ""
	for _, v := range output {
		sout = fmt.Sprintf("%s,%d", sout, v)
	}

	return sout[1:], nil
}

func solve1(prog []int, i int, A int) []int {
	if i == -1 {
		return []int{A/8}
	}

	var candidates []int
	for m := 0; m < 8; m++ {
		// ans := (m ^ ((8*n+m)/(1<<(7-m)))) % 8
		ans := Run([]int{A + m,0,0}, prog[0:len(prog)-2])[0]
		if prog[i] == ans {
			// potential
			candidates = append(candidates, solve1(prog, i-1, 8*(A + m))...)
		}
	}
	return candidates
}

func part2() (int, error) {
	_, progs, err := parse()
	if err != nil {
		return 0, err
	}
	var mina int
	for _, prog := range progs {
		if len(prog) != 16 {
			panic("incorrect programme")
		}
		if prog[0] != 2 || prog[1] != 4 {
			panic("incorrect first")
		}
		if prog[12] != 5 || prog[13] != 5 {
			panic("incorrect out")
		}
		if prog[14] != 3 || prog[15] != 0 {
			panic("incorrect jump")
		}

		candidates := solve1(prog, len(prog) - 1, 0)
		mina = 9223372036854775807
		for _, A := range candidates {
			if !reflect.DeepEqual(Run([]int{A,0,0}, prog), prog) {
				panic("invalid")
			}
			if A < mina { mina = A }
		}
	}

	return mina, nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}

