package main

import (
	"fmt"
	"os"
	// "slices"
	// "strconv"
	"strings"
)

type Point struct {
	x, y int
}

func (p Point) String() string {
	return fmt.Sprintf("{%d,%d}", p.y+1, p.x+1)
}


func parse() ([][]rune, Point, []rune, error) {
	readFile, err := os.ReadFile("input15.txt")
	if err != nil {
		return nil, Point{}, nil, err
	}
	blocks := strings.Split(string(readFile), "\n\n")
	mats := strings.Split(blocks[0],"\n")
	operations := blocks[1]

	bot := Point{0,0}

	mat := make([][]rune, len(mats))
	for y, line := range mats {
		mat[y] = []rune(line)
		for x, c := range line {
			if c == '@' {
				bot.x = x
				bot.y = y
			}
		}
	}
	return mat, bot, []rune(operations), nil
}

func Copy(mat [][]rune) [][]rune {
	ans := make([][]rune, len(mat))
	for y, line := range mat {
		ans[y] = make([]rune, len(line))
		copy(ans[y], line)
	}
	return ans
}

func move(mat *[][]rune, p Point, dx, dy int) bool {
	if (*mat)[p.y+dy][p.x+dx] == '#' {
		return false
	}
	if (*mat)[p.y+dy][p.x+dx] == 'O' {
		if !move(mat, Point{p.x+dx, p.y+dy}, dx, dy) {
			return false
		}
	}
	if (*mat)[p.y+dy][p.x+dx] == '[' {
		if !move(mat, Point{p.x+dx+1, p.y+dy}, dx, dy) {
			return false
		}
		if !move(mat, Point{p.x+dx, p.y+dy}, dx, dy) {
			return false
		}
	} else if (*mat)[p.y+dy][p.x+dx] == ']' {
		if !move(mat, Point{p.x+dx-1, p.y+dy}, dx, dy) {
			return false
		}
		if !move(mat, Point{p.x+dx, p.y+dy}, dx, dy) {
			return false
		}
	}
	(*mat)[p.y+dy][p.x+dx], (*mat)[p.y][p.x] = (*mat)[p.y][p.x], (*mat)[p.y+dy][p.x+dx]
	return true
}

func GPS(mat *[][]rune, ch rune) int {
	tally := 0
	for y, line := range *mat {
		for x, c := range line {
			if c == ch {
				tally += 100*y + x
			}
		}
	}
	return tally
}

func play(mat [][]rune, bot Point, ops []rune, ch rune) int {
	for _, op := range ops {
		if op == '^' {
			mat2 := Copy(mat)
			if move(&mat, bot, 0, -1) {
				bot.y -= 1
			} else {
				mat = mat2
			}
		} else if op == 'v' {
			mat2 := Copy(mat)
			if move(&mat, bot, 0, +1) {
				bot.y += 1
			} else {
				mat = mat2
			}
		} else if op == '<' {
			mat2 := Copy(mat)
			if move(&mat, bot, -1, 0) {
				bot.x -= 1
			} else {
				mat = mat2
			}
		} else if op == '>' {
			mat2 := Copy(mat)
			if move(&mat, bot, +1, 0) {
				bot.x += 1
			} else {
				mat = mat2
			}
		}
	}

	return GPS(&mat, ch)
}

func part1() (int, error) {
	mat, bot, ops, err := parse()
	if err != nil {
		return 0, err
	}

	return play(mat, bot, ops, 'O'), nil
}

func part2() (int, error) {
	mat0, bot, ops, err := parse()
	if err != nil {
		return 0, err
	}

	mat := make([][]rune, len(mat0))
	for y, line := range mat0 {
		mat[y] = make([]rune, 2*len(line))
		for x, c := range line {
			if c == '@' {
				mat[y][2*x] = '@'
				mat[y][2*x+1] = '.'
			} else if c == 'O' {
				mat[y][2*x] = '['
				mat[y][2*x+1] = ']'
			} else {
				mat[y][2*x] = c
				mat[y][2*x+1] = c
			}
		}
	}

	bot.x *= 2

	return play(mat, bot, ops, '['), nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}

