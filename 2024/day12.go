package main

import (
	"fmt"
	"bufio"
	"os"
)

type Point struct {
	x int
	y int
}

func (p Point) North() Point {
	return Point{p.x, p.y-1}
}
func (p Point) East() Point {
	return Point{p.x+1, p.y}
}
func (p Point) South() Point {
	return Point{p.x, p.y+1}
}
func (p Point) West() Point {
	return Point{p.x-1, p.y}
}
func (p Point) At(mat [][]rune) rune {
	if p.y >= len(mat) || p.y < 0 || p.x >= len(mat[0]) || p.x < 0 {
		return '\x00'
	} else {
		return mat[p.y][p.x]
	}
}

func (p Point) String() string {
	return fmt.Sprintf("{%d,%d}", p.y+1, p.x+1)
}

type Edge struct {
	loc int
	dir rune
}

func parse() ([][]rune, error) {
	readFile, err := os.Open("input12.txt")
	if err != nil {
		return nil, err
	}
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	var mat [][]rune
	for fileScanner.Scan() {
		mat = append(mat, []rune(fileScanner.Text()))
	}

	return mat, nil
}

func part1() (int, error) {
	mat, err := parse()
	if err != nil {
		return 0, err
	}

	seen := make(map[Point]bool, len(mat)*len(mat[0]))

	cost := 0
	for y := range mat {
		for x := range mat[0] {
			p := Point{x,y}
			if _, ok := seen[p]; ! ok {
				seen[p] = true

				area := 0
				perimeter := 0

				current := p.At(mat)

				// begin flood fill from here
				queue := []Point{p}
				for len(queue) > 0 {
					// pop
					v := queue[0] ; queue = queue[1:]
					area++

					points1 := []Point{v.South(), v.North(), v.West(), v.East()}

					for _, p1 := range points1 {
						if current == p1.At(mat) {
							if _, ok := seen[p1]; ! ok {
								seen[p1] = true
								queue = append(queue, p1)
							}
						} else {
							perimeter++
						}
					}
				}
				cost += area * perimeter
			}
		}
	}

	return cost, nil
}

func part2() (int, error) {
	mat, err := parse()
	if err != nil {
		return 0, err
	}

	seen := make(map[Point]bool, len(mat)*len(mat[0]))

	cost := 0
	for y := range mat {
		for x := range mat[0] {
			p := Point{x,y}
			if _, ok := seen[p]; ! ok {
				seen[p] = true

				area := 0
				edges := 0

				current := p.At(mat)

				// begin flood fill from here
				queue := []Point{p}
				for len(queue) > 0 {
					// pop
					v := queue[0] ; queue = queue[1:]
					area++

					points1 := []Point{v.South(), v.North(), v.West (), v.East ()}
					points2 := []Point{v.West (), v.East (), v.North(), v.South()}
					points3 := []Point{v.South().West(), v.North().East(), v.West().North(), v.East().South()}

					for i, p1 := range points1 {
						p2 := points2[i]
						p3 := points3[i]
						is_edge1 := current != p1.At(mat)
						is_edge2 := current != p2.At(mat)
						is_edge3 := current != p3.At(mat)

						if is_edge1 && is_edge2 {
							edges++
						} else if !is_edge1 && !is_edge2 && is_edge3 {
							edges++
						}
						if !is_edge1 {
							if _, ok := seen[p1]; ! ok {
								seen[p1] = true
								queue = append(queue, p1)
							}
						}
					}
				}
				cost += area * edges
			}
		}
	}
	return cost, nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}
