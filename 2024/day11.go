package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func parse() ([]int, error) {
	readFile, err := os.ReadFile("input11.txt")
	if err != nil {
		return nil, err
	}

	fields := strings.Fields(string(readFile))
	data := make([]int, len(fields))

	for i, v := range fields {
		data[i], err = strconv.Atoi(v)
		if err != nil {
			return nil, err
		}
	}

	return data, nil
}

func step(stones map[int]int) map[int]int {
	ans := make(map[int]int, len(stones))
	for v, n := range stones {
		digits := strconv.Itoa(v)
		ndigits := len(digits)

		if v == 0 {
			ans[1] += n
		} else if ndigits % 2 == 0 {
			a, _ := strconv.Atoi(digits[:ndigits/2])
			b, _ := strconv.Atoi(digits[ndigits/2:])
			ans[a] += n
			ans[b] += n
		} else {
			ans[2024*v] += n
		}
	}
	return ans
}

func solve(n int, initial []int) int {
	stones := make(map[int]int, len(initial))

	for _, s := range initial {
		stones[s] = 1
	}

	for i := 0; i < n; i++ {
		stones = step(stones)
	}

	tally := 0
	for _, v := range stones {
		tally += v
	}
	return tally
}

func part1() (int, error) {
	initial, err := parse()
	if err != nil {
		return 0, err
	}

	return solve(25, initial), nil
}

func part2() (int, error) {
	initial, err := parse()
	if err != nil {
		return 0, err
	}

	return solve(75, initial), nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}
