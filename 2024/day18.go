package main

import (
	"fmt"
	"bufio"
	"container/heap"
	"os"
	"strconv"
	"strings"
)

const WIDTH = 70
const HEIGHT = 70

type Point struct {
	x, y int
}

func (p Point) String() string {
	return fmt.Sprintf("{%d,%d}", p.y+1, p.x+1)
}

// Implement priority queue based on godocs
type Item struct {
	value    Point
	priority int
	index    int
}

type PriorityQueue []*Item

func (pq PriorityQueue) Len() int { return len(pq) }
func (pq PriorityQueue) Less(i, j int) bool { return pq[i].priority > pq[j].priority }

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq *PriorityQueue) Push(x any) {
	n := len(*pq)
	item := x.(*Item)
	item.index = n
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() any {
	old := *pq
	n := len(old)
	item := old[n-1]
	old[n-1] = nil  // don't stop the GC from reclaiming the item eventually
	item.index = -1 // for safety
	*pq = old[0 : n-1]
	return item
}

// update modifies the priority and value of an Item in the queue.
func (pq *PriorityQueue) update(item *Item, value Point, priority int) {
	item.value = value
	item.priority = priority
	heap.Fix(pq, item.index)
}

func parse() ([]Point, error) {
	readFile, err := os.Open("input18.txt")
	if err != nil {
		return nil, err
	}
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	var points []Point

	for fileScanner.Scan() {
		cols := strings.Split(fileScanner.Text(), ",")

		x, err := strconv.Atoi(cols[0])
		if err != nil {
			return nil, err
		}
		y, err := strconv.Atoi(cols[1])
		if err != nil {
			return nil, err
		}
		points = append(points, Point{x,y})
	}
	return points, nil
}

const INF = 9223372036854775807

func Dijkstra(points map[Point]bool, src Point, tgt Point) (map[Point]int, map[Point]Point) {
	dist := make(map[Point]int, WIDTH * HEIGHT)
	dist[src] = 0

	prev := make(map[Point]Point, WIDTH * HEIGHT)
	prev[src] = Point{-1,-1}

	Q := make(PriorityQueue, 1, WIDTH * HEIGHT)
	Q[0] = &Item{src, 0, 0}

	i := 1

	// Fill priority queue
	for y := 0; y <= HEIGHT; y++ {
		for x := 0; x <= WIDTH; x++ {
			if x == src.x && y == src.y { continue }
			p := Point{x,y}
			dist[p] = INF
			prev[p] = Point{-1,-1}
			i++
		}
	}

	heap.Init(&Q)

	// run Dijkstra
	for Q.Len() > 0 {
		pp := heap.Pop(&Q).(*Item)
		p := pp.value

		if p == tgt {
			break
		}

		var check = func(dx, dy int) {
			p2 := Point{p.x + dx, p.y + dy}

			if p2.x < 0 || p2.x > WIDTH { return; }
			if p2.y < 0 || p2.y > HEIGHT { return; }
			if _, ok := points[p2]; !ok {
				newdist := dist[p] + 1
				if newdist < dist[p2] {
					prev[p2] = p
					dist[p2] = newdist
					Q.Push(&Item{p2, -newdist, -1})
				}
			}
		}

		check(0, -1)
		check(0, +1)
		check(-1, 0)
		check(+1, 0)
	}

	return dist, prev
}

func part1() (int, error) {
	points, err := parse()
	if err != nil {
		return 0, err
	}

	pointsmap := make(map[Point]bool, 1024)
	for _, p := range points[:1024] {
		pointsmap[p] = true
	}

	dist, _ := Dijkstra(pointsmap, Point{0,0}, Point{WIDTH, HEIGHT})

	return dist[Point{WIDTH, HEIGHT}], nil
}

func part2() (string, error) {
	_, err := parse()
	if err != nil {
		return "", err
	}

	points, err := parse()
	if err != nil {
		return "", err
	}


	lower := 1024
	upper := len(points)
	for lower <= upper {
		m := (lower+upper)/2
		pointsmap := make(map[Point]bool, m)
		for _, p := range points[:m] {
			pointsmap[p] = true
		}

		dist, _ := Dijkstra(pointsmap, Point{0,0}, Point{WIDTH, HEIGHT})
		if dist[Point{WIDTH, HEIGHT}] == INF {
			upper = m - 1
		} else {
			lower = m + 1
		}
	}
	return fmt.Sprintf("%d,%d", points[lower-1].x, points[lower-1].y), nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}
