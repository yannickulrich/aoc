package main

import (
	"fmt"
	"bufio"
	"os"
)

type Point struct {
	x int
	y int
}

func (p Point) Add(a Point) Point {
	return Point{p.x + a.x, p.y + a.y}
}

func (p Point) Mul(s int) Point {
	return Point{s * p.x, s * p.y}
}

func (p Point) Sub(a Point) Point {
	return Point{p.x - a.x, p.y - a.y}
}

func (p *Point) InRange(corner Point) bool {
	if p.x < 0 || p.y < 0 {
		return false
	}
	if p.x > corner.x || p.y > corner.y {
		return false
	}
	return true
}

func (p Point) String() string {
	return fmt.Sprintf("{%d,%d}", p.y+1, p.x+1)
}

type Map = map[rune][]Point

func parse() (Map, Point, error) {
	readFile, err := os.Open("input08.txt")
	if err != nil {
		return nil, Point{}, err
	}
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	m := Map{}
	var corner Point

	y := 0
	for fileScanner.Scan() {
		for x, c := range fileScanner.Text() {
			corner.x = x
			if c == '.' || c == '#' {
				continue
			} else {
				if v, ok := m[c]; ok {
					m[c] = append(v, Point{x,y})
				} else {
					m[c] = []Point{Point{x,y}}
				}
			}
		}
		corner.y = y
		y++
	}
	return m, corner, nil
}

func part1() (int, error) {
	m, corner, err := parse()
	if err != nil {
		return 0, err
	}

	seen := map[Point]bool{}
	for _, locs := range m {
		for i, l1 := range locs {
			for j, l2 := range locs {
				if i == j { continue }

				tgt := l2.Mul(2).Sub(l1)
				if tgt.InRange(corner) {
					seen[tgt] = true
				}
			}
		}
	}

	return len(seen), nil
}

func part2() (int, error) {
	m, corner, err := parse()
	if err != nil {
		return 0, err
	}

	seen := map[Point]bool{}
	for _, locs := range m {
		for i, l1 := range locs {
			for j, l2 := range locs {
				if i == j { continue }

				for s := 0; true; s++ {
					tgt := l1.Add(l2.Sub(l1).Mul(s))
					if tgt.InRange(corner) {
						seen[tgt] = true
					} else {
						break
					}
				}
			}
		}
	}

	return len(seen), nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}
