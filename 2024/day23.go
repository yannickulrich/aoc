package main

import (
	"fmt"
	"os"
	"slices"
	"strings"
)

func parse() (map[string][]string, error) {
	readFile, err := os.ReadFile("input23.txt")
	if err != nil {
		return nil, err
	}
	sinputs := strings.Split(string(readFile), "\n")
	connections := make(map[string][]string, len(sinputs)-1)

	for _, s := range sinputs[:len(sinputs)-1] {
		a := s[0:2]
		b := s[3:5]

		if v, ok := connections[a] ; ok {
			connections[a] = append(v, b)
		} else {
			connections[a] = []string{b}
		}

		if v, ok := connections[b] ; ok {
			connections[b] = append(v, a)
		} else {
			connections[b] = []string{a}
		}
	}
	return connections, nil
}

type Tuple struct {
	A, B, C string
}

func part1() (int, error) {
	network, err := parse()
	if err != nil {
		return 0, err
	}

	seen := make(map[Tuple]bool)

	tally := 0
	for pc, friends := range network {
		// testing pc, see if all friends are also connected to each other
		if pc[0] != 't' { continue }
		for _, friendA := range friends {
			for _, friendB := range friends {
				if friendA == friendB { continue }

				if slices.Contains(network[friendA], friendB) {
					cluster := []string{pc, friendA, friendB}
					slices.SortFunc(cluster, strings.Compare)
					t := Tuple{cluster[0], cluster[1], cluster[2]}
					if _, ok := seen[t] ; !ok {
						seen[t] = true
						tally += 1
					}
				}
			}
		}
	}
	return tally, nil
}

// Bron-Kerbosch
func BuildClique(network map[string][]string, clique []string, potentials []string, excluded []string) []string {
	if len(potentials) == 0 && len(excluded) == 0 {
		slices.SortFunc(clique, strings.Compare)
		return []string{strings.Join(clique, ",")}
	}

	var ans []string
	for i, v := range potentials {
		new_potentials := make([]string, 0, len(potentials))
		new_excluded := make([]string, 0, len(excluded))
		for _, n := range network[v] {
			if slices.Contains(potentials[i:], n) {
				new_potentials = append(new_potentials, n)
			}
			if slices.Contains(excluded, n) {
				new_excluded = append(new_excluded, n)
			}
		}

		ans = append(ans, BuildClique(network, append(clique, v), new_potentials, new_excluded)...)
		excluded = append(excluded, v)
	}
	return ans
}

func part2() (string, error) {
	network, err := parse()
	if err != nil {
		return "", err
	}

	keys := make([]string, len(network))
	i := 0
	for k := range network {
		keys[i] = k
		i++
	}

	ans := BuildClique(network, []string{}, keys, []string{})
	longest := ""
	for _, s := range ans {
		if len(s) > len(longest) {
			longest = s
		}
	}
	return longest, nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}
