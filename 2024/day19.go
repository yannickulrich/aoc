package main

import (
	"fmt"
	"os"
	"strings"
)

func parse() ([]string, []string, error) {
	readFile, err := os.ReadFile("input19.txt")
	if err != nil {
		return nil, nil, err
	}
	blocks := strings.Split(string(readFile), "\n\n")

	available0 := strings.Split(blocks[0], ", ")
	available := make([]string, len(available0))
	for i, v := range available0 {
		available[i] = v
	}

	tgts0 := strings.Split(blocks[1], "\n")
	tgts := make([]string, len(tgts0)-1)
	for i, v := range tgts0[:len(tgts0)-1] {
		tgts[i] = v
	}

	return available, tgts, nil
}

var cache = make(map[string]int)

func attempt(avail []string, tgt string) int {
	if len(tgt) == 0 {
		return 1
	}
	if v, ok := cache[tgt] ; ok {
		return v
	}

	good := 0
	for _, t := range avail {
		if len(t) > len(tgt) {
			continue
		}
		if tgt[0:len(t)] == t {
			good += attempt(avail, tgt[len(t):])
		}
	}
	cache[tgt] = good
	return good
}

func part1() (int, error) {
	avail, tgts, err := parse()
	if err != nil {
		return 0, err
	}

	tally := 0
	for _, tgt := range tgts {
		if attempt(avail, tgt) > 0 {
			tally += 1
		}
	}

	return tally, nil
}

func part2() (int, error) {
	_, _, err := parse()
	if err != nil {
		return 0, err
	}

	avail, tgts, err := parse()
	if err != nil {
		return 0, err
	}

	tally := 0
	for _, tgt := range tgts {
		tally += attempt(avail, tgt)
	}

	return tally, nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}

