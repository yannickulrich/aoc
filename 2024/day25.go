package main

import (
	"fmt"
	"os"
	"strings"
)

type Tuple struct {
	a,b,c,d,e int
}

func (t1 Tuple) Fits(t2 Tuple) bool {
	return t1.a+t2.a <= 5 && t1.b+t2.b <= 5 && t1.c+t2.c <= 5 && t1.d+t2.d <= 5 && t1.e+t2.e <= 5
}

func parse() ([]Tuple, []Tuple, error) {
	readFile, err := os.ReadFile("input25.txt")
	if err != nil {
		return nil, nil, err
	}

	blocks := strings.Split(string(readFile), "\n\n")
	keys := make([]Tuple, 0, len(blocks)/2)
	locks:= make([]Tuple, 0, len(blocks)/2)
	for _, block := range blocks {
		lines := strings.Split(block, "\n")[:7]
		entry := Tuple{-1,-1,-1,-1,-1}

		if lines[0] == "....." {
			// key
			for i, line := range lines {
				if line[0] == '#' && entry.a == -1 {
					entry.a = 6-i
				}
				if line[1] == '#' && entry.b == -1 {
					entry.b = 6-i
				}
				if line[2] == '#' && entry.c == -1 {
					entry.c = 6-i
				}
				if line[3] == '#' && entry.d == -1 {
					entry.d = 6-i
				}
				if line[4] == '#' && entry.e == -1 {
					entry.e = 6-i
				}
			}
			keys = append(keys, entry)
		} else {
			// lock
			for i, line := range lines {
				if line[0] == '.' && entry.a == -1 {
					entry.a = i-1
				}
				if line[1] == '.' && entry.b == -1 {
					entry.b = i-1
				}
				if line[2] == '.' && entry.c == -1 {
					entry.c = i-1
				}
				if line[3] == '.' && entry.d == -1 {
					entry.d = i-1
				}
				if line[4] == '.' && entry.e == -1 {
					entry.e = i-1
				}
			}
			locks = append(locks, entry)
		}
	}
	return keys, locks, nil
}


func part1() (int, error) {
	keys, locks, err := parse()
	if err != nil {
		return 0, err
	}

	tally := 0
	for _, lock := range locks {
		for _, key := range keys {
			if key.Fits(lock) {
				tally++
			}
		}
	}

	return tally, nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}
