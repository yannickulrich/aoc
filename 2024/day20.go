package main

import (
	"fmt"
	"os"
	"strings"
)

type Point struct {
	x, y int
}

func (p Point) String() string {
	return fmt.Sprintf("{%d,%d}", p.y+1, p.x+1)
}

func parse() ([][]rune, Point, Point, error) {
	readFile, err := os.ReadFile("input20.txt")
	if err != nil {
		return nil, Point{}, Point{}, err
	}
	lines := strings.Split(string(readFile), "\n")

	start := Point{}
	end := Point{}
	mat := make([][]rune, len(lines)-1)
	for y, line := range lines[:len(lines)-1] {
		mat[y] = []rune(line)
		for x, c := range line {
			if c == 'S' {
				start.x = x ; start.y = y
			} else if c == 'E' {
				end.x = x ; end.y = y
			}
		}
	}
	return mat, start, end, nil
}

const INF = 9223372036854775807

func BFS(mat [][]rune, src Point) map[Point]int {
	queue := []Point{src}
	dist := make(map[Point]int, len(mat[0]) * len(mat))

	for y, row := range mat {
		for x, c := range row {
			if c != '#' {
				dist[Point{x,y}] = INF
			}
		}
	}

	dist[src] = 0

	for len(queue) > 0 {
		// pop
		p := queue[0] ; queue = queue[1:]

		var check = func(dx, dy int) {
			p2 := Point{p.x + dx, p.y + dy}

			if p2.x < 0 || p2.x >= len(mat[0]) { return; }
			if p2.y < 0 || p2.y >= len(mat) { return; }
			if mat[p2.y][p2.x] != '#' {
				if dist[p] + 1 < dist[p2] {
					dist[p2] = dist[p] + 1
					queue = append(queue, p2)
				}
			}
		}

		check( 0, -1)
		check(+1,  0)
		check(-1,  0)
		check( 0, +1)
	}
	return dist
}

func solve(cheatlength int) (int, error) {
	mat, src, tgt, err := parse()
	if err != nil {
		return 0, err
	}

	distSrc := BFS(mat, src)
	worst := distSrc[tgt]
	distTgt := BFS(mat, tgt)

	tally := 0
	for y, row := range mat {
		for x := range row {
			if tocheat, ok := distSrc[Point{x,y}] ; ok {
				for dx := -cheatlength ; dx <= cheatlength; dx++ {
					adx := dx
					if adx < 0 { adx = -adx }
					for dy := -cheatlength + adx ; dy <= cheatlength - adx; dy++ {
						ady := dy
						if ady < 0 { ady = -ady }
						if fromcheat, ok := distTgt[Point{x+dx,y+dy}] ; ok {
							newdist := tocheat + adx + ady + fromcheat
							if worst - newdist >= 100 {
								tally++
							}
						}
					}
				}
			}
		}
	}

	return tally, nil
}

func part1() (int, error) {
	n, err := solve(2)
	return n, err
}

func part2() (int, error) {
	n, err := solve(20)
	return n, err
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}

