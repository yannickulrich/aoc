package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func parse() ([]string, error) {
	readFile, err := os.ReadFile("input21.txt")
	if err != nil {
		return nil, err
	}
	codes := strings.Split(string(readFile), "\n")
	return codes[:len(codes)-1], nil
}

type Point struct {
	x, y int
}

func (p Point) String() string {
	return fmt.Sprintf("{%d,%d}", p.y+1, p.x+1)
}

type Keypad func(rune) Point

// Final panel
// +---+---+---+
// | 7 | 8 | 9 |
// +---+---+---+
// | 4 | 5 | 6 |
// +---+---+---+
// | 1 | 2 | 3 |
// +---+---+---+
//     | 0 | A |
//     +---+---+

func Number2Point(c rune) Point {
	if c == '0' {
		return Point{1, 3}
	} else if c == 'A' {
		return Point{2, 3}
	} else if c == '1' {
		return Point{0, 2}
	} else if c == '2' {
		return Point{1, 2}
	} else if c == '3' {
		return Point{2, 2}
	} else if c == '4' {
		return Point{0, 1}
	} else if c == '5' {
		return Point{1, 1}
	} else if c == '6' {
		return Point{2, 1}
	} else if c == '7' {
		return Point{0, 0}
	} else if c == '8' {
		return Point{1, 0}
	} else if c == '9' {
		return Point{2, 0}
	} else {
		panic("invalid code")
	}
}

// Direction panel
//     +---+---+
//     | ^ | A |
// +---+---+---+
// | < | v | > |
// +---+---+---+

func Instr2Point(c rune) Point {
	if c == '^' {
		return Point{1,0}
	} else if c == 'A' {
		return Point{2,0}
	} else if c == '<' {
		return Point{0,1}
	} else if c == 'v' {
		return Point{1,1}
	} else if c == '>' {
		return Point{2,1}
	} else {
		panic("invalid code")
	}
}

func Move(src Point, tgt Point, dead Point) string {
	dx := tgt.x - src.x
	dy := tgt.y - src.y

	var instrY string
	var instrX string

	if dy < 0 {
		instrY = strings.Repeat("^", -dy)
	} else if dy > 0 {
		instrY = strings.Repeat("v", dy)
	}

	if dx < 0 {
		instrX = strings.Repeat("<", -dx)
	} else if dx > 0 {
		instrX = strings.Repeat(">", dx)
	}

	// we prefer ^>, v>, <^, <v unless this leads to a blank

	if dx > 0 {
		if src.x == dead.x && tgt.y == dead.y {
			return instrX + instrY + "A"
		} else {
			return instrY + instrX + "A"
		}
	} else if dx < 0 {
		if tgt.x == dead.x && src.y == dead.y {
			return instrY + instrX + "A"
		} else {
			return instrX + instrY + "A"
		}
	} else {
		return instrX + instrY + "A"
	}
}

func Press(code string, keypad Keypad, pos Point, dead Point) string {
	instr := ""
	for _, c := range code {
		tgt := keypad(c)
		instr += Move(pos, tgt, dead)
		pos = tgt
	}
	return instr
}

type Item struct {
	code string
	depth int
}

var cache = make(map[Item]int)

func step(code string, depth int) int {
	if depth == 0 {
		return len(code)
	}

	if v, ok := cache[Item{code, depth}] ; ok {
		return v
	}

	tally := 0
	for i, c := range code {
		prev := 'A'
		if i > 0 {
			prev = rune(code[i-1])
		}
		tally += step(Move(Instr2Point(prev), Instr2Point(c), Point{0,0}), depth-1)
	}
	cache[Item{code, depth}] = tally

	return tally
}

func part1() (int, error) {
	cache = make(map[Item]int)
	codes, err := parse()
	if err != nil {
		return 0, err
	}
	tally := 0
	for _, code := range codes {
		keypadinstr := Press(code, Number2Point, Point{2,3}, Point{0,3})
		// dirpadinstr := Press(keypadinstr, Instr2Point, Point{2,0}, Point{0,0})
		// dirpadinstr2 := Press(dirpadinstr, Instr2Point, Point{2,0}, Point{0,0})

		ans, err := strconv.Atoi(code[:len(code)-1])
		if err != nil {
			return 0, err
		}
		// tally += ans * len(dirpadinstr2)
		tally += ans * step(keypadinstr, 2)
	}
	return tally, nil
}

func part2() (int, error) {
	cache = make(map[Item]int)
	codes, err := parse()
	if err != nil {
		return 0, err
	}
	tally := 0
	for _, code := range codes {
		keypadinstr := Press(code, Number2Point, Point{2,3}, Point{0,3})
		ans, err := strconv.Atoi(code[:len(code)-1])
		if err != nil {
			return 0, err
		}
		tally += ans * step(keypadinstr, 25)
	}
	return tally, nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}

