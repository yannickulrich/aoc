package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func parse() ([]int, error) {
	readFile, err := os.ReadFile("input22.txt")
	if err != nil {
		return nil, err
	}
	sinputs := strings.Split(string(readFile), "\n")
	inputs := make([]int, len(sinputs)-1)
	for i, s := range sinputs[:len(sinputs)-1] {
		inputs[i], err = strconv.Atoi(s)
		if err != nil {
			return nil, err
		}
	}
	return inputs, nil
}

const mod = 16777216

func step(n int) int {
	n = ((64 * n) ^ n) % mod
	n = ((n / 32) ^ n) % mod
	n = ((2048*n) ^ n) % mod
	return n
}

func part1() (int, error) {
	nums, err := parse()
	if err != nil {
		return 0, err
	}

	tally := 0
	for _, n := range nums {
		for i := 0; i < 2000; i++ {
			n = step(n)
		}
		tally += n
	}
	return tally, nil
}

type Tuple struct {
	a,b,c,d int
}

func part2() (int, error) {
	nums, err := parse()
	if err != nil {
		return 0, err
	}

	sequences := make(map[Tuple]int, 0)

	for _, n := range nums {
		seen := make(map[Tuple]bool, 0)

		tup := Tuple{0,0,0,0}
		last_price := n % 10

		for i := 0; i < 2000; i++ {
			n = step(n)
			price := n % 10

			tup.a = tup.b
			tup.b = tup.c
			tup.c = tup.d
			tup.d = price - last_price

			if i > 3 {
				if _, ok := seen[tup] ; !ok {
					seen[tup] = true
					sequences[tup] += price
				}
			}
			last_price = price
		}
	}

	best_tally := 0
	for _, n := range sequences {
		if n > best_tally {
			best_tally = n
		}
	}

	return best_tally, nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}

