package main

import (
	"fmt"
	"bufio"
	"os"
)

func parse() ([][]rune, error) {
	readFile, err := os.Open("input04.txt")
	if err != nil {
		return nil, err
	}
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	var mat [][]rune
	for fileScanner.Scan() {
		mat = append(mat, []rune(fileScanner.Text()))
	}

	return mat, nil
}

func test(x rune, m rune, a rune, s rune) bool {
	needle := []rune("XMAS")

	if x != needle[0] {
		return false
	}
	if m != needle[1] {
		return false
	}
	if a != needle[2] {
		return false
	}
	if s != needle[3] {
		return false
	}

	return true
}

func part1() (int, error) {
	mat, err := parse()
	if err != nil {
		return 0, err
	}

	tally := 0
	for y := range(len(mat)) {
		for x := range(len(mat[y])) {
			// vert backwards
			if y >= 3 {
				if test(mat[y][x], mat[y-1][x], mat[y-2][x], mat[y-3][x]) {
					tally += 1
				}
			}
			// hori backwards
			if x >= 3 {
				if test(mat[y][x], mat[y][x-1], mat[y][x-2], mat[y][x-3]) {
					tally += 1
				}
			}
			// hori backwards
			if y <= len(mat) - 4 {
				if test(mat[y][x], mat[y+1][x], mat[y+2][x], mat[y+3][x]) {
					tally += 1
				}
			}
			// hori forwards
			if x <= len(mat[y]) - 4 {
				if test(mat[y][x], mat[y][x+1], mat[y][x+2], mat[y][x+3]) {
					tally += 1
				}
			}

			// diagonal up left
			if y >= 3 && x >= 3 {
				if test(mat[y][x], mat[y-1][x-1], mat[y-2][x-2], mat[y-3][x-3]) {
					tally += 1
				}
			}
			// diagonal up right
			if y >= 3 && x <= len(mat[y]) - 4 {
				if test(mat[y][x], mat[y-1][x+1], mat[y-2][x+2], mat[y-3][x+3]) {
					tally += 1
				}
			}
			// diagonal down left
			if y <= len(mat) - 4 && x >= 3 {
				if test(mat[y][x], mat[y+1][x-1], mat[y+2][x-2], mat[y+3][x-3]) {
					tally += 1
				}
			}
			// diagonal down right
			if y <= len(mat) - 4 && x <= len(mat[y]) - 4 {
				if test(mat[y][x], mat[y+1][x+1], mat[y+2][x+2], mat[y+3][x+3]) {
					tally += 1
				}
			}
		}
	}

	return tally, nil
}

func part2() (int, error) {
	mat, err := parse()
	if err != nil {
		return 0, err
	}

	tally := 0
	for y := 1; y < len(mat) - 1; y++ {
		for x := 1; x < len(mat[y]) - 1; x++ {
			if mat[y][x] == rune('A') {
				// M S
				//  A
				// M S
				if mat[y-1][x-1] == rune('M') && mat[y+1][x-1] == rune('M') {
					if mat[y-1][x+1] == rune('S') && mat[y+1][x+1] == rune('S') {
						tally += 1
					}
				}

				// S S
				//  A
				// M M
				if mat[y+1][x-1] == rune('M') && mat[y+1][x+1] == rune('M') {
					if mat[y-1][x-1] == rune('S') && mat[y-1][x+1] == rune('S') {
						tally += 1
					}
				}

				// S M
				//  A
				// S M
				if mat[y-1][x+1] == rune('M') && mat[y+1][x+1] == rune('M') {
					if mat[y-1][x-1] == rune('S') && mat[y+1][x-1] == rune('S') {
						tally += 1
					}
				}

				// M M
				//  A
				// S S
				if mat[y-1][x-1] == rune('M') && mat[y-1][x+1] == rune('M') {
					if mat[y+1][x-1] == rune('S') && mat[y+1][x+1] == rune('S') {
						tally += 1
					}
				}
			}
		}
	}
	return tally, nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}
