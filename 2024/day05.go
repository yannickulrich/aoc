package main

import (
	"fmt"
	"os"
	"slices"
	"strconv"
	"strings"
)

type Rules struct {
	n int
	firsts []int
	seconds []int
}

func (r *Rules) Cmp(i, j int) int {
	for n := 0; n < r.n; n++ {
		if i == r.firsts[n] && j == r.seconds[n] {
			return -1
		} else if j == r.firsts[n] && i == r.seconds[n] {
			return +1
		}
	}
	return 0
}

func parse() (Rules, [][]int, error) {
	readFile, err := os.ReadFile("input05.txt")
	if err != nil {
		return Rules{}, nil, err
	}
	blocks := strings.Split(string(readFile), "\n\n")
	rules := blocks[0]
	allpages := blocks[1]


	sp := strings.Split(rules, "\n")
	parsedRules := Rules{len(sp), make([]int, len(sp)), make([]int, len(sp))}

	for i, rule := range sp {
		rule := strings.Split(rule, "|")

		parsedRules.firsts[i], err = strconv.Atoi(rule[0])
		if err != nil {
			return Rules{}, nil, err
		}
		parsedRules.seconds[i], err = strconv.Atoi(rule[1])
		if err != nil {
			return Rules{}, nil, err
		}
	}

	var parsedPages [][]int
	for _, pages := range strings.Split(allpages, "\n") {
		if pages == "" { continue }
		pages := strings.Split(pages, ",")
		parsedPage := make([]int, len(pages))
		for i, page := range pages {
			parsedPage[i], err = strconv.Atoi(page)
			if err != nil {
				return Rules{}, nil, err
			}
		}
		parsedPages = append(parsedPages, parsedPage)
	}

	return parsedRules, parsedPages, nil
}

func part1() (int, error) {
	rules, book, err := parse()
	if err != nil {
		return 0, err
	}
	tally := 0
	for _, pages := range book {
		if slices.IsSortedFunc(pages, rules.Cmp) {
			tally += pages[len(pages)/2]
		}
	}
	return tally, nil
}

func part2() (int, error) {
	rules, book, err := parse()
	if err != nil {
		return 0, err
	}
	tally := 0
	for _, pages := range book {
		if slices.IsSortedFunc(pages, rules.Cmp) {
			continue
		}
		slices.SortFunc(pages, rules.Cmp)
		tally += pages[len(pages)/2]
	}
	return tally, nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}

