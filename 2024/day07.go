package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

func parse() ([]int, [][]int, error) {
	readFile, err := os.Open("input07.txt")
	if err != nil {
		return nil, nil, err
	}
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	var answers []int
	var numbers [][]int
	for fileScanner.Scan() {
		line := strings.Split(fileScanner.Text(), ": ")

		ans, err := strconv.Atoi(line[0])
		if err != nil {
			return nil, nil, err
		}
		answers = append(answers, ans)

		line = strings.Split(line[1]," ")
		nums := make([]int, len(line))
		for i, v := range line {
			nums[i], err = strconv.Atoi(v)
			if err != nil {
				return nil, nil, err
			}
		}

		numbers = append(numbers, nums)
	}

	return answers, numbers, nil
}

func concat(a, b int) int {
	// c(a,b) = a*10^{⌈log10(b+1)⌉}+b
	return int(float64(a) * math.Pow(10., math.Ceil(math.Log10(float64(b+1)))) + float64(b))
}

func attempt(numbers []int, ans int, c bool) bool {
	if len(numbers) == 1 {
		return numbers[0] == ans
	} else {
		if attempt(append([]int{numbers[0] + numbers[1]}, numbers[2:]...), ans, c) {
			return true
		}
		if attempt(append([]int{numbers[0] * numbers[1]}, numbers[2:]...), ans, c) {
			return true
		}
		if c {
			if attempt(append([]int{concat(numbers[0], numbers[1])}, numbers[2:]...), ans, c) {
				return true
			}
		}
		return false
	}
}

func part1() (int, error) {
	answers, numbers, err := parse()
	if err != nil {
		return 0, err
	}

	tally := 0
	for i, nums := range numbers {
		if attempt(nums, answers[i], false) {
			tally += answers[i]
		}
	}
	return tally, nil
}

func part2() (int, error) {
	answers, numbers, err := parse()
	if err != nil {
		return 0, err
	}

	tally := 0
	for i, nums := range numbers {
		if attempt(nums, answers[i], true) {
			tally += answers[i]
		}
	}
	return tally, nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}
