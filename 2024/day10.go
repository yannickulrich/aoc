package main

import (
	"fmt"
	"bufio"
	"os"
)

type Point struct {
	x int
	y int
}

func (p Point) North() Point {
	return Point{p.x, p.y-1}
}
func (p Point) East() Point {
	return Point{p.x+1, p.y}
}
func (p Point) South() Point {
	return Point{p.x, p.y+1}
}
func (p Point) West() Point {
	return Point{p.x-1, p.y}
}
func (p *Point) InRange(corner Point) bool {
	if p.x < 0 || p.y < 0 {
		return false
	}
	if p.x > corner.x || p.y > corner.y {
		return false
	}
	return true
}
func (p Point) At(mat [][]rune) rune {
	if p.y >= len(mat) || p.y < 0 || p.x >= len(mat[0]) || p.x < 0 {
		return '\x00'
	} else {
		return mat[p.y][p.x]
	}
}

func (p Point) String() string {
	return fmt.Sprintf("{%d,%d}", p.y+1, p.x+1)
}

func parse() ([][]rune, error) {
	readFile, err := os.Open("input10.txt")
	if err != nil {
		return nil, err
	}
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	var mat [][]rune
	for fileScanner.Scan() {
		mat = append(mat, []rune(fileScanner.Text()))
	}

	return mat, nil
}

func bfs(mat [][]rune, p Point, part int) int {
	queue := []Point{p}
	seen := map[Point]int{p: 1}

	tally := 0

	for len(queue) > 0 {
		// pop
		v := queue[0] ; queue = queue[1:]

		current := v.At(mat)
		if current == '9' {
			// reached a goal
			if part == 1 {
				tally += 1
			} else {
				tally += seen[v]
			}
		} else {
			if current + 1 == v.South().At(mat) {
				if _, ok := seen[v.South()]; ! ok {
					seen[v.South()] = seen[v]
					queue = append(queue, v.South())
				} else {
					seen[v.South()] += seen[v]
				}
			}
			if current + 1 == v.North().At(mat) {
				if _, ok := seen[v.North()]; ! ok {
					seen[v.North()] = seen[v]
					queue = append(queue, v.North())
				} else {
					seen[v.North()] += seen[v]
				}
			}
			if current + 1 == v.West().At(mat) {
				if _, ok := seen[v.West()]; ! ok {
					seen[v.West()] = seen[v]
					queue = append(queue, v.West())
				} else {
					seen[v.West()] += seen[v]
				}
			}
			if current + 1 == v.East().At(mat) {
				if _, ok := seen[v.East()]; ! ok {
					seen[v.East()] = seen[v]
					queue = append(queue, v.East())
				} else {
					seen[v.East()] += seen[v]
				}
			}
		}
	}

	return tally
}

func part1() (int, error) {
	mat, err := parse()
	if err != nil {
		return 0, err
	}

	tally := 0
	for y, r := range mat {
		for x, c := range r {
			if c == '0' {
				tally += bfs(mat, Point{x,y}, 1)
			}
		}
	}

	return tally, nil
}

func part2() (int, error) {
	mat, err := parse()
	if err != nil {
		return 0, err
	}

	tally := 0
	for y, r := range mat {
		for x, c := range r {
			if c == '0' {
				tally += bfs(mat, Point{x,y}, 2)
			}
		}
	}

	return tally, nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}
