package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
)

const WIDTH = 101
const HEIGHT = 103

type Bot struct {
	x,y,vx,vy int
}

func (p *Bot) Evolve(t int) {
	p.x = (p.x + t * p.vx + t * WIDTH) % WIDTH
	p.y = (p.y + t * p.vy + t * HEIGHT) % HEIGHT
}

func parse() ([]Bot, error) {
	readFile, err := os.ReadFile("input14.txt")
	if err != nil {
		return nil, err
	}
	content := string(readFile)
	ms := regexp.MustCompile(`p=(\d*),(\d*) v=([-\d]*),([-\d]*)`).FindAllStringSubmatch(content, -1)

	ans := make([]Bot, len(ms))

	for i, m := range ms {
		ans[i].x, err = strconv.Atoi(m[1])
		if err != nil {
			return nil, err
		}
		ans[i].y, err = strconv.Atoi(m[2])
		if err != nil {
			return nil, err
		}
		ans[i].vx, err = strconv.Atoi(m[3])
		if err != nil {
			return nil, err
		}
		ans[i].vy, err = strconv.Atoi(m[4])
		if err != nil {
			return nil, err
		}
	}
	return ans, nil

}

func part1() (int, error) {
	bots, err := parse()
	if err != nil {
		return 0, err
	}

	for j := range bots {
		bots[j].Evolve(100)
	}
	tallyNW := 0
	tallyNE := 0
	tallySW := 0
	tallySE := 0
	for _, b := range bots {
		if b.x == (WIDTH - 1)/2 { continue; }
		if b.y == (HEIGHT - 1)/2 { continue; }

		if 2*b.x/WIDTH == 0 && 2*b.y/HEIGHT == 0 {
			tallyNW++
		} else if 2*b.x/WIDTH == 1 && 2*b.y/HEIGHT == 0 {
			tallyNE++
		} else if 2*b.x/WIDTH == 0 && 2*b.y/HEIGHT == 1 {
			tallySW++
		} else if 2*b.x/WIDTH == 1 && 2*b.y/HEIGHT == 1 {
			tallySE++
		}
	}
	return tallyNW*tallyNE*tallySW*tallySE, nil
}

func part2() (int, error) {
	bots, err := parse()
	if err != nil {
		return 0, err
	}

	minvar := 100000000000
	mini := 0
	for i := 1; i < 10000; i++ {
		sumx := 0
		sumxsq := 0
		sumy := 0
		sumysq := 0
		for j := range bots {
			bots[j].Evolve(1)
			sumx += bots[j].x
			sumy += bots[j].y
			sumxsq += bots[j].x * bots[j].x
			sumysq += bots[j].y * bots[j].y
		}
		variance := sumxsq*len(bots) - sumx*sumx + sumysq*len(bots) - sumy*sumy
		if variance < minvar {
			mini = i
			minvar = variance
		}
	}

	return mini, nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}
