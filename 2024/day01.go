package main

import (
	"fmt"
	"sort"
	"bufio"
	"os"
	"strconv"
	"strings"
)

func parse() ([]int, []int, error) {
	readFile, err := os.Open("input01.txt")
	if err != nil {
		return nil, nil, err
	}
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	var col1 []int
	var col2 []int
	for fileScanner.Scan() {
		cols := strings.Fields(fileScanner.Text())

		i, err := strconv.Atoi(cols[0])
		if err != nil {
			return nil, nil, err
		}
		col1 = append(col1, i)

		i, err = strconv.Atoi(cols[1])
		if err != nil {
			return nil, nil, err
		}
		col2 = append(col2, i)
	}

	return col1, col2, nil
}

// Total[Abs[Subtract@@Sort/@Thread[Import["input01.txt","Table"]]]]
func part1() (int, error) {
	col1, col2, err := parse()
	if err != nil {
		return 0, err
	}

	sort.Slice(col1, func(i, j int) bool {
		return col1[i] < col1[j]
	})

	sort.Slice(col2, func(i, j int) bool {
		return col2[i] < col2[j]
	})

	tally := 0
	for i := range col1 {
		if col2[i] < col1[i] {
			tally += (col1[i] - col2[i])
		} else {
			tally += (col2[i] - col1[i])
		}
	}

	return tally, nil
}

func part2() (int, error) {
	col1, col2, err := parse()
	if err != nil {
		return 0, err
	}

	dict :=  make(map[int]int)
    for _ , num :=  range col2 {
		dict[num] = dict[num]+1
    }

	tally := 0
	for _, x := range col1 {
		tally += x * dict[x]
	}

	return tally, nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}
