package main

import (
	"fmt"
	"bufio"
	"os"
)

type Point struct {
	x int
	y int
}
type Set = []bool

const WIDTH = 130
const HEIGHT = 130

func parse() (Set, Point, Point, error) {
	readFile, err := os.Open("input06.txt")
	if err != nil {
		return nil, Point{}, Point{}, err
	}
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	walls := make([]bool, WIDTH * HEIGHT)
	var guard Point
	var corner Point

	y := 0
	for fileScanner.Scan() {
		for x, c := range fileScanner.Text() {
			if c == '#' {
				walls[y * WIDTH + x] = true
			} else if c == '^' {
				guard.x = x ; guard.y = y
			}
			corner.x = x
		}
		corner.y = y
		y++
	}

	return walls, guard, corner, nil
}

const (
	NORTH = 1
	EAST = 2
	SOUTH = 4
	WEST = 8
)

func attempt(walls Set, guard Point, corner Point) ([]int, bool) {
	dir := NORTH

	visited := make([]int, WIDTH * HEIGHT)

	for guard.x <= corner.x && guard.y <= corner.y && guard.x >= 0 && guard.y >= 0 {

		if visited[guard.y * WIDTH + guard.x] & dir != 0 {
			return visited, true
		}
		visited[guard.y * WIDTH + guard.x] |= dir

		if dir == NORTH {
			if guard.y > 0 && walls[(guard.y-1) * WIDTH + guard.x] {
				dir = EAST
			} else {
				guard.y--
			}
		} else if dir == EAST {
			if guard.x < WIDTH-1 && walls[guard.y * WIDTH + guard.x+1] {
				dir = SOUTH
			} else {
				guard.x++
			}
		} else if dir == SOUTH {
			if guard.y < HEIGHT-1 && walls[(guard.y+1) * WIDTH + guard.x] {
				dir = WEST
			} else {
				guard.y++
			}
		} else if dir == WEST {
			if guard.x > 0 && walls[guard.y * WIDTH + guard.x-1] {
				dir = NORTH
			} else {
				guard.x--
			}
		}
	}

	return visited, false
}

func part1() (int, error) {
	walls, guard, corner, err := parse()
	if err != nil {
		return 0, err
	}
	visited, _ := attempt(walls, guard, corner)
	tally := 0
	for _, cell := range visited {
		if cell > 0 { tally++ }
	}

	return tally, nil
}

func part2() (int, error) {
	walls, guard, corner, err := parse()
	if err != nil {
		return 0, err
	}
	visited, _ := attempt(walls, guard, corner)

	tally := 0
	for i, cell := range visited {
		if cell == 0 { continue }

		walls2 := make([]bool, len(walls))
		copy(walls2, walls)

		walls2[i] = true
		if _, looped := attempt(walls2, guard, corner) ; looped {
			tally++
		}
	}
	return tally, nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}
