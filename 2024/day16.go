package main

import (
	"fmt"
	"container/heap"
	"os"
	"strings"
)

type Point struct {
	x, y int
	dir rune
}

func (p Point) String() string {
	return fmt.Sprintf("{%d,%d:%c}", p.y+1, p.x+1, p.dir)
}

// Implement priority queue based on godocs
type Item struct {
	value    Point
	priority int
	index    int
}

type PriorityQueue []*Item

func (pq PriorityQueue) Len() int { return len(pq) }
func (pq PriorityQueue) Less(i, j int) bool { return pq[i].priority > pq[j].priority }

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq *PriorityQueue) Push(x any) {
	n := len(*pq)
	item := x.(*Item)
	item.index = n
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() any {
	old := *pq
	n := len(old)
	item := old[n-1]
	old[n-1] = nil  // don't stop the GC from reclaiming the item eventually
	item.index = -1 // for safety
	*pq = old[0 : n-1]
	return item
}

// update modifies the priority and value of an Item in the queue.
func (pq *PriorityQueue) update(item *Item, value Point, priority int) {
	item.value = value
	item.priority = priority
	heap.Fix(pq, item.index)
}

func (pq *PriorityQueue) updateByValue(p Point, priority int) {
	for i := range *pq {
		if (*pq)[i].value == p {
			(*pq)[i].priority = priority
			heap.Fix(pq, (*pq)[i].index)
			break
		}
	}
}



func parse() ([][]rune, error) {
	readFile, err := os.ReadFile("input16.txt")
	if err != nil {
		return nil, err
	}
	lines := strings.Split(string(readFile), "\n")

	mat := make([][]rune, len(lines)-1)
	for y, line := range lines[:len(lines)-1] {
		mat[y] = []rune(line)
	}
	return mat, nil
}

const INF = 9223372036854775807

func Dijkstra(mat [][]rune, src Point, tgt *Point) (map[Point]int, map[Point]map[Point]bool) {
	dist := make(map[Point]int, len(mat) * len(mat[0]))
	dist[src] = 0

	prev := make(map[Point]map[Point]bool, len(mat) * len(mat[0]))
	prev[src] = map[Point]bool{Point{src.x-1, src.y, 'E'}: true}

	Q := make(PriorityQueue, 1, len(mat) * len(mat[0]) * 4 - 3)
	Q[0] = &Item{src, 0, 0}

	i := 1

	// Fill priority queue
	dirs := []rune{'N','W','S','E'}
	for y := 0; y < len(mat); y++ {
		for x := 0; x < len(mat[0]); x++ {
			if x == src.x && y == src.y { continue }
			for _, d := range dirs {
				p := Point{x,y, d}
				dist[p] = INF
				prev[p] = map[Point]bool{}
				i++
			}
		}
	}

	heap.Init(&Q)

	// run Dijkstra
	for Q.Len() > 0 {
		pp := heap.Pop(&Q).(*Item)
		p := pp.value

		if p.x == (*tgt).x && p.y == (*tgt).y {
			(*tgt).dir = p.dir
			break
		}

		var check = func(dx, dy int, dir rune, cost int) {
			p2 := Point{p.x + dx, p.y + dy, dir}

			if p2.x < 0 || p2.x >= len(mat[0]) { return; }
			if p2.y < 0 || p2.y >= len(mat) { return; }
			if mat[p2.y][p2.x] != '#' {
				newdist := dist[p] + cost
				if newdist == dist[p2] {
					prev[p2][p] = true
					dist[p2] = newdist
					Q.Push(&Item{p2, -newdist, -1})
				} else if newdist < dist[p2] {
					prev[p2] = map[Point]bool{p: true}
					dist[p2] = newdist
					Q.Push(&Item{p2, -newdist, -1})
				}
			}
		}

		if p.dir == 'N' {
			check(0, -1, 'N', 1)
			check(0, 0, 'E', 1000)
			check(0, 0, 'W', 1000)
		} else if p.dir == 'S' {
			check(0, 1, 'S', 1)
			check(0, 0, 'E', 1000)
			check(0, 0, 'W', 1000)
		} else if p.dir == 'E' {
			check(1, 0, 'E', 1)
			check(0, 0, 'N', 1000)
			check(0, 0, 'S', 1000)
		} else if p.dir == 'W' {
			check(-1, 0, 'W', 1)
			check(0, 0, 'N', 1000)
			check(0, 0, 'S', 1000)
		}
	}

	return dist, prev
}

func part1() (int, error) {
	mat, err := parse()
	if err != nil {
		return 0, err
	}

	src := Point{1, len(mat)-2, 'E'}
	tgt := Point{len(mat[0])-2, 1, '*'}
	dist, _ := Dijkstra(mat, src, &tgt)

	return dist[tgt], nil
}

func Backtrace(prev *map[Point]map[Point]bool, seen *map[Point]bool, p Point) {
	(*seen)[Point{p.x,p.y,'*'}] = true
	for p0, _ := range (*prev)[p] {
		Backtrace(prev, seen, p0)
	}
}

func part2() (int, error) {
	mat, err := parse()
	if err != nil {
		return 0, err
	}

	src := Point{1, len(mat)-2, 'E'}
	tgt := Point{len(mat[0])-2, 1, '*'}
	_, prev := Dijkstra(mat, src, &tgt)

	seen := make(map[Point]bool, 1)
	Backtrace(&prev, &seen, tgt)

	return len(seen) - 1, nil
}


func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}

