package main

import (
	"fmt"
	"os"
	"strings"
	"slices"
)

type Block struct {
	length int
	id int
	file bool
}

func (b Block) String() string {
	if b.file {
		return strings.Repeat(fmt.Sprintf("%d", b.id), b.length)
	} else {
		return strings.Repeat(".", b.length)
	}
}

func parse() ([]Block, error) {
	readFile, err := os.ReadFile("input09.txt")
	if err != nil {
		return nil, err
	}
	content := strings.TrimSpace(string(readFile))

	disk := make([]Block, len(content))

	for i, c := range content {
		disk[i].length = int(c - '0')
		if i % 2 == 0 {
			disk[i].file = true
			disk[i].id = i / 2
		} else {
			disk[i].file = false
		}
	}

	return disk, nil
}

func flatten(disk []Block) []Block {
	l := 0
	for _, v := range disk {
		l += v.length
	}

	d := make([]Block, l)

	loc := 0
	for _, v := range disk {
		if v.file {
			for i := loc; i < loc + v.length; i++ {
				d[i].length = 1
				d[i].file = true
				d[i].id = v.id
			}
		} else {
			for i := loc; i < loc + v.length; i++ {
				d[i].length = 1
				d[i].file = false
				d[i].id = -1
			}
		}
		loc += v.length
	}
	return d
}

func checksum(disk []Block) int {
	tally := 0
	i := 0
	for _, v := range disk {
		if v.file {
			tally += v.id * v.length * (2*i+v.length-1)/2
		}
		i += v.length
	}
	return tally
}

func defrag(disk []Block) int {
	for i := 0; i < len(disk); i++ {
		if !disk[i].file {
			for j := len(disk)-1; j > i; j-- {
				if disk[j].file && disk[j].length <= disk[i].length {
					file := disk[j]
					free := disk[i]

					disk[j] = Block{file.length, -1, false}
					if free.length - file.length == 0 {
						disk[i] = file
					} else {
						disk[i] = Block{free.length - file.length, -1, false}
						disk = slices.Insert(disk, i, file)
					}
					break
				}
			}
		}
	}

	return checksum(disk)
}

func part1() (int, error) {
	disk, err := parse()
	if err != nil {
		return 0, err
	}

	fdisk := flatten(disk)
	return defrag(fdisk), nil
}

func part2() (int, error) {
	disk, err := parse()
	if err != nil {
		return 0, err
	}

	return defrag(disk), nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}
