package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
)

func part1() (int, error) {
	readFile, err := os.ReadFile("input03.txt")
	if err != nil {
		return 0, err
	}
	content := string(readFile)

	tally := 0

	var validID = regexp.MustCompile(`mul\((\d*),(\d*)\)`)
	for _, x := range validID.FindAllStringSubmatch(content, -1) {
		v1, err := strconv.Atoi(x[1])
		if err != nil {
			return 0, err
		}
		v2, err := strconv.Atoi(x[2])
		if err != nil {
			return 0, err
		}

		tally += v1*v2
	}

	return tally, nil
}

func part2() (int, error) {
	readFile, err := os.ReadFile("input03.txt")
	if err != nil {
		return 0, err
	}
	content := string(readFile)
	enabled := true
	tally := 0

	var validID = regexp.MustCompile(`(mul\((\d*),(\d*)\)|do\(\)|don't\(\))`)
	for _, x := range validID.FindAllStringSubmatch(content, -1) {
		if x[1] == "do()" {
			enabled = true
		} else if x[1] == "don't()" {
			enabled = false
		} else {
			if enabled {
				v1, err := strconv.Atoi(x[2])
				if err != nil {
					return 0, err
				}
				v2, err := strconv.Atoi(x[3])
				if err != nil {
					return 0, err
				}

				tally += v1*v2
			}
		}
	}
	return tally, nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}
