package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"strconv"
)

func parse() ([][]int, error) {
	readFile, err := os.Open("input02.txt")
	if err != nil {
		return nil, err
	}
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	var matrix [][]int
	for fileScanner.Scan() {
		var row []int
		cols := strings.Fields(fileScanner.Text())
		for _, s := range cols {
			i, err := strconv.Atoi(s)
			if err != nil {
				return nil, err
			}
			row = append(row, i)
		}
		matrix = append(matrix, row)
	}
	return matrix, nil
}

const (
	ASCENDING = iota
	DESCENDING = iota
	UNKNOWN = iota
)

// diffs=Differences/@Import["input02.txt","Table"];
// Total@Thread[And[Equal@@@Sign[diffs],And@@@Map[1<=#<=3&,Abs[diffs],{2}]]]

func IsSafe(report []int) bool {
	prev := report[0]
	mode := UNKNOWN

	for j, level := range report {
		if level > prev {
			if mode == DESCENDING {
				return false
			}
			mode = ASCENDING

			if level - prev > 3 {
				return false
			}
		} else if level < prev {
			if mode == ASCENDING {
				return false
			}
			mode = DESCENDING

			if prev - level > 3 {
				return false
			}
		} else if j != 0 {
			return false
		}
		prev = level
	}

	return true
}

func part1() (int, error) {
	matrix, err := parse()
	if err != nil {
		return 0, err
	}

	tally := 0
	for _, report := range matrix {
		if IsSafe(report) {
			tally += 1
		}
	}
	return tally, nil
}

func remove(s []int, index int) []int {
    ret := make([]int, 0)
    ret = append(ret, s[:index]...)
    return append(ret, s[index+1:]...)
}

func part2() (int, error) {
	matrix, err := parse()
	if err != nil {
		return 0, err
	}

	tally := 0
	for _, report := range matrix {
		for i := range report {
			if IsSafe(remove(report, i)) {
				// fmt.Println("Report ", j, " is safe for ", i);
				tally += 1
				break
			}
		}
	}
	return tally, nil
}

func main() {
	if v, err := part1() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
	if v, err := part2() ; err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(v)
	}
}
