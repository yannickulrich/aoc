function searchlist(lst)
    for j in length(lst):-1:1
        if lst[j] == '.'
            continue
        else
            return j+1
        end
    end
    return 1
end
function searchlistInv(lst)
    for j in 1:length(lst)
        if lst[j] == '.'
            continue
        else
            return j-1
        end
    end
    return length(lst)
end

function slidenorth(matrix)
    for y=2:size(matrix,1)
        for x=1:size(matrix,2)
            if matrix[y,x] == 'O'
                ynew = searchlist(matrix[1:y-1,x])
                if y > ynew
                    matrix[y,x] = '.'
                    matrix[ynew,x] = 'O'
                end
            end
        end
    end
end
function slidewest(matrix)
    for x=2:size(matrix,2)
        for y=1:size(matrix,1)
            if matrix[y,x] == 'O'
                xnew = searchlist(matrix[y,1:x-1])
                if x > xnew
                    matrix[y,x] = '.'
                    matrix[y,xnew] = 'O'
                end
            end
        end
    end
end
function slidesouth(matrix)
    for y=size(matrix,1)-1:-1:1
        for x=1:size(matrix,2)
            if matrix[y,x] == 'O'
                ynew = searchlistInv(matrix[y+1:size(matrix,1),x]) + y
                if y < ynew
                    matrix[y,x] = '.'
                    matrix[ynew,x] = 'O'
                end
            end
        end
    end
end
function slideeast(matrix)
    for x=size(matrix,2)-1:-1:1
        for y=1:size(matrix,1)
            if matrix[y,x] == 'O'
                xnew = searchlistInv(matrix[y,x+1:size(matrix,1)]) + x
                if x < xnew
                    matrix[y,x] = '.'
                    matrix[y,xnew] = 'O'
                end
            end
        end
    end
end

function spin(matrix)
    slidenorth(matrix)
    slidewest(matrix)
    slidesouth(matrix)
    slideeast(matrix)
end

function score(matrix)
    tally = 0
    for y=1:size(matrix,1)
        tally += count(x->x=='O',matrix[y,:]) * (size(matrix,1)-y+1)
    end
    tally
end

function part1(inp)
    matrix = mapreduce(permutedims, vcat, map(collect,eachsplit(inp,"\n")))
    slidenorth(matrix)
    matrix |> score
end

function search_cycle(matrix)
    seen = Dict()
    i = 0
    while true
        try
            return i,i-seen[hash(matrix)]
        catch e
        end
        push!(seen, hash(matrix) => i)
        spin(matrix)
        i += 1
    end
end

function part2(inp)
    matrix = mapreduce(permutedims, vcat, map(collect,eachsplit(inp,"\n")))
    n0,cycle = search_cycle(matrix)
    for i in 1:mod(1000000000 - n0, cycle)
        spin(matrix)
    end
    return score(matrix)
end

inp = strip(open(io->read(io, String), "input14.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
