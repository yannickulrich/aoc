mutable struct Block
    x0::Int
    y0::Int
    z0::Int
    x1::Int
    y1::Int
    z1::Int

    function Block(line)
        x0,y0,z0,x1,y1,z1 = map(x->parse(Int,x),split(line, r"[,~]"))
        new(x0+1,y0+1,z0,x1+1,y1+1,z1)
    end
end

function drop(blocks)
    blocks = deepcopy(blocks)
    changed = true
    fallen = []

    xmi = Inf
    ymi = Inf
    xma =-Inf
    yma =-Inf
    for block in blocks
        if block.x0 < xmi
            xmi = block.x0
        end
        if block.x1 > xma
            xma = block.x1
        end
        if block.y0 < ymi
            ymi = block.y0
        end
        if block.y1 > yma
            yma = block.y1
        end
    end

    @assert xmi == 1
    @assert ymi == 1

    tallest = zeros(Int, yma, xma)
    hasfallen = 0

    for block in blocks
        zfin = maximum(tallest[block.y0:block.y1,block.x0:block.x1])

        dz = max(block.z0 - zfin - 1, 0)
        if dz > 0
            hasfallen += 1
        end

        block.z1 -= dz
        block.z0 -= dz

        tallest[block.y0:block.y1,block.x0:block.x1] .= block.z1

        push!(fallen, block)
    end
    hasfallen, fallen
end

function couldremove(b0, blocks)
    hasfallen, _ = drop(filter(x->x!=b0, blocks))
    hasfallen == 0
end

function part1(inp)
    blocks = map(Block, eachsplit(inp, "\n"))
    blocks = sort(blocks, by=x->x.z0)
    _, blocks = drop(blocks)
    count(b->couldremove(b, blocks), blocks)
end

function countfallen(b0, blocks)
    hasfallen, _ = drop(filter(x->x!=b0, blocks))
    hasfallen
end

function part2(inp)
    blocks = map(Block, eachsplit(inp, "\n"))
    blocks = sort(blocks, by=x->x.z0)
    _, blocks = drop(blocks)
    map(b->countfallen(b, blocks), blocks) |> sum
end

inp = strip(open(io->read(io, String), "input22.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
