function parse(inp)
    matrix = mapreduce(permutedims, vcat, map(collect,eachsplit(inp,"\n")))

    src = (1,1)
    for y in 1:size(matrix,1)
        for x in 1:size(matrix,2)
            if matrix[y,x] == 'S'
                src = (y,x)
                matrix[y,x] = '.'
            end
        end
    end

    return matrix, src
end

function solven(matrix, src, steps, nd,no)
    reachable = zeros(Bool, nd*size(matrix, 1), nd*size(matrix, 2))
    visited = Set()

    function search((y,x), n)
        if ((y,x), n) in visited
            return
        end

        push!(visited, ((y,x),n))

        if n == 0
            reachable[no+y,no+x] = true
            return
        end

        clip(v) = (v + nd*size(matrix,1) - 1)%size(matrix,1) + 1

        options = [(-1,0),(+1,0),(0,+1),(0,-1)]
        for (dy,dx) in options
            if matrix[clip(y+dy),clip(x+dx)] == '.'
                search((y+dy,x+dx), n-1)
            end
        end
    end

    search(src, steps)
    count(reachable)
end

function part1(inp)
    matrix, src = parse(inp)
    solven(matrix, src, 64, 1,0)
end

function part2(inp)
    matrix, src = parse(inp)
    mod = size(matrix, 1)
    offset = 26501365 % mod
    tgt = div(26501365, mod)

    y0, y1, y2 = [solven(matrix, src, offset + mod*i, 2*i+1,i*mod) for i in 0:2]

    # we make a quadratic ansatz and solve
    a = div(y0-2*y1+y2, 2)
    b = div(-3*y0+4*y1-y2,2)
    c = y0

    a*tgt*tgt + b * tgt + c
end

inp = strip(open(io->read(io, String), "input21.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
