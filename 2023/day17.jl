function dijkstra(matrix, mind, maxd)
    target = (size(matrix,1), size(matrix,2))

    dist = 2^30 * ones(Int, size(matrix, 1), size(matrix, 2), 4)

    Q = [(1,1,k) for k in [1,2,3,4]]
    dist[1,1,1:end] .= 0

    while length(Q) > 0
        u = argmin([dist[i,j,k] for (i,j,k) in Q])
        y,x,d = popat!(Q, u)
        if (y,x) == target
            return minimum(dist[target[1],target[2],1:end])
        end

        options = [(-1,0),(+1,0),(0,+1),(0,-1)]
        for (di,(dy,dx)) in enumerate(options)
            if di == d
                continue
            end
            if d > 0
                if options[d][1] == -dy && options[d][2] == -dx
                    continue
                end
            end

            alt = dist[y,x,d]
            for i in 1:maxd
                if y + i*dy < 1 || x + i*dx < 1
                    continue
                end
                if y + i*dy > size(matrix,1) || x + i*dx > size(matrix,2)
                    continue
                end
                alt += matrix[y+i*dy,x+i*dx]
                if i < mind
                    continue
                end
                if alt < dist[y+i*dy,x+i*dx,di]
                    dist[y+i*dy,x+i*dx,di] = alt
                    push!(Q, (y+i*dy,x+i*dx,di))
                end
            end
        end
    end

    return -1
end

function part1(inp)
    matrix = mapreduce(permutedims, vcat, map(collect,eachsplit(inp,"\n")))
    matrix = map(x->parse(Int,x),matrix)
    dijkstra(matrix, 1, 3)
end

function part2(inp)
    matrix = mapreduce(permutedims, vcat, map(collect,eachsplit(inp,"\n")))
    matrix = map(x->parse(Int,x),matrix)
    dijkstra(matrix, 4, 10)
end

inp = strip(open(io->read(io, String), "input17.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
