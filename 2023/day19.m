input = StringSplit[Import["input19.txt", "Text"], "\n\n"] // First;

CloseBracket[str_] := str <> "[x,m,a,s]" <> StringRepeat["]", StringCount[str, "if"]]

ToExpression[
  CloseBracket /@ StringReplace[StringSplit[input, "\n"], {
    "{" -> "[x_,m_,a_,s_] := ",
    "}" -> "",
    RegularExpression["([xmas][><]\\d*):([a-zAR]*),"] :> "if[$1, $2[x,m,a,s], "
  }]
]

cond = LogicalExpand[
  in[x, m, a, s] /. A[__] -> A /. R[__] -> R //. {
  if[cond_, t_, f_] :> (cond && t) || (Not[cond] && f)
}];

cond2 = Map[Reduce, cond /. R -> False /. A -> True];

cond3 = Reduce[And[
  #,
  4000 >= x >= 1,
  4000 >= m >= 1,
  4000 >= a >= 1,
  4000 >= s >= 1
]] & /@ cond2;

cond3 /. Or -> or /. And -> and /. {
  Inequality[n_, LessEqual, _, Less, m_] :> m - n,
  Inequality[n_, Less, _, LessEqual, m_] :> m - n,
  Inequality[n_, LessEqual, _, LessEqual, m_] :> m - n + 1,
  Inequality[n_, Less, _, Less, m_] :> m - n - 1
} /. or -> Plus /. and -> Times
