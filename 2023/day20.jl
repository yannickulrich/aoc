function parse(inp)
    graph = Dict{String, Tuple{Char, Vector{String}}}()
    for line in eachsplit(inp, "\n")
        lhs, rhs = split(replace(line, "," => ""), " -> ")
        rhs = split(rhs)
        if lhs[1] == '%'
            type = '%'
            lhs = lhs[2:end]
        elseif lhs[1] == '&'
            type = '&'
            lhs = lhs[2:end]
        else
            type = ' '
        end
        graph[lhs] = (type, rhs)
    end
    graph["output"] = (' ', [])
    graph["rx"] = (' ', [])
    graph
end

function connected(graph, n)
    ans = []
    for (lhs, (_, rhs)) in graph
        if n in rhs
            push!(ans, lhs)
        end
    end
    ans
end

function hashState(stateff, statec)
    hash((collect(values(stateff)), [collect(values(i)) for i in values(statec)]))
end

function propagate(graph, stateff, statec, watch)
    queue = []
    tally = [1, 0]

    function appendtos(src, rhs, state)
        tally[Int(state)+1] += length(rhs)
        if state
            if src in keys(watch)
                watch[src] = true
            end
        end
        for r in rhs
            # println(src, " -", state, "> ", r)
            push!(queue, (r, src, state))
        end
    end

    # broadcast
    _, rhs = graph["broadcaster"]
    appendtos("broadcaster", rhs, false)

    # rest
    while length(queue) > 0
        name, src, inc = popfirst!(queue)
        type, rhs = graph[name]
        if type == '%'
            if !inc
                stateff[name] = !stateff[name]
                appendtos(name, rhs, stateff[name])
            end
        elseif type == '&'
            statec[name][src] = inc
            modified = true
            if all(values(statec[name]))
                appendtos(name, rhs, false)
            else
                appendtos(name, rhs, true)
            end
        end
    end
    tally
end

function part1(inp)
    graph = parse(inp)

    stateff = Dict([(i, false) for (i, (type, _)) in graph if type == '%'])
    statec = Dict([(i, Dict([(j, false) for j in connected(graph, i)])) for (i, (type, _)) in graph if type == '&'])

    tally = [0,0]
    for i in 1:1000
        tally += propagate(graph, stateff, statec, Dict())
    end
    tally[1] * tally[2]
end

function part2(inp)
    graph = parse(inp)

    rxinp = connected(graph, "rx")
    @assert length(rxinp) == 1
    rxinp = connected(graph, rxinp[1])

    stateff = Dict([(i, false) for (i, (type, _)) in graph if type == '%'])
    statec = Dict([(i, Dict([(j, false) for j in connected(graph, i)])) for (i, (type, _)) in graph if type == '&'])
    watch = Dict([(i, false) for i in rxinp])
    watchI = Dict{String, Int}()

    n = 0
    while !all(values(watch))
        n += 1
        propagate(graph, stateff, statec, watch)
        for (k, v) in watch
            if v && !(k in keys(watchI))
                watchI[k] = n
            end
        end
    end
    lcm(collect(values(watchI)))
end

inp = strip(open(io->read(io, String), "input20.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
