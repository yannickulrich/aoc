function parser(inp)
    function parse1(line)
        src, l, r = split(line)
        return (src, (l,r))
    end

    instrs, graph = collect(eachsplit(inp, "\n\n"))
    graph = Dict(map(parse1, eachsplit(replace(graph, r"[=,()]" => ""), "\n")))

    return instrs, graph
end

function part1(inp)
    instrs, graph = parser(inp)
    instrs = Iterators.cycle(instrs)

    location = "AAA"
    for (n, instr) in enumerate(instrs)
        tgtL, tgtR = graph[location]
        if instr == 'L'
            location = tgtL
        else
            location = tgtR
        end
        if location == "ZZZ"
            return n
        end
    end
end

function part2_bf(inp)
    instrs, graph = parser(inp)
    instrs = Iterators.cycle(instrs)
    locations = collect(filter(x->x[3] == 'A', keys(graph)))
    for (n, instr) in enumerate(instrs)
        for i in 1:length(locations)
            tgtL, tgtR = graph[locations[i]]
            if instr == 'L'
                locations[i] = tgtL
            else
                locations[i] = tgtR
            end
        end
        if n % 100000 == 0
            println(n)
        end
        if count(x -> x[3] == 'Z', locations) == length(locations)
            return n
        end
    end
end

function cycle_length(location, graph, instrs)
    l0 = location
    seen = Dict()
    n = 0
    while true
        instr = instrs[n%length(instrs)+1]

        try
            return n - seen[(location, n%length(instrs))]
        catch e

        end

        push!(seen, (location, n%length(instrs)) => n)
        tgtL, tgtR = graph[location]
        if instr == 'L'
            location = tgtL
        else
            location = tgtR
        end
        n += 1
    end
end


function part2(inp)
    instrs, graph = parser(inp)
    locations = collect(filter(x->x[3] == 'A', keys(graph)))
    cycles = map(l -> cycle_length(l, graph, instrs), locations)
    return lcm(cycles)
end



inp = strip(open(io->read(io, String), "input08.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
