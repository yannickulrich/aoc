using LinearAlgebra
function parsemat(inp)
    function parse1(line)
        map(x->parse(Int,x), split(line))
    end
    mat = reduce(hcat,map(parse1,eachsplit(replace(inp, r"[,@]" => ""),"\n")))
    mat[1:3,:]', mat[4:6,:]'
end

function part1(inp)
    # We have a parametrix funcion
    #   x = x0 + vx t, y = y0 + vy t
    # Solving for t
    #   t = (x-x0)/vx
    # we have
    #   y = y0 + vy (x-x0)/vx
    #     = (y0 - vy/vx x0) + (vy/vx) x
    #     = a + b x
    # We are looking for intersection
    # between two lines
    #   y = a + b x  and  y = a' + b' x
    # These meet at
    #   x = (a'-a)/(b-b')
    #   y = (a'b - ab') / (b-b')
    # The time of intersection is
    #   t = (x-x0)/vx

    lower = 200000000000000
    upper = 400000000000000

    pos, vel = parsemat(inp)
    A = pos[:, 2] - vel[:,2]./vel[:,1] .* pos[:,1]
    B = vel[:,2]./vel[:,1]

    tally = 0
    for (n,(a, b, vx, x0)) in enumerate(zip(A,B, vel[:,1], pos[:,1]))
        for (m,(aa, bb, vvx, xx0)) in enumerate(zip(A,B, vel[:,1], pos[:,1]))
            x = (aa-a)/(b-bb)
            y = (aa*b-a*bb) / (b-bb)
            t1 = (x-x0)/vx
            t2 = (x-xx0)/vvx

            if lower <= x <= upper && lower <= y <= upper && t1 >= t2 > 0
                tally += 1
            end
        end
    end
    tally
end

function part2(inp)
    # We are looking to intersect one line
    #   {x,y,z} + t {vx,vy,vz} = p + t v
    # with n other lines
    #   {xi,yi,zi} + t {vxi,vyi,vzi} = pi + t vi
    # A solution exists if
    #   p + t v == pi + t vi
    #   (p-pi) = (vi-v) t
    # Crossing with (vi-v)
    #   (pi-p)*(vi-v) = 0
    #   pi*vi - p*vi - v*pi + p*v = 0
    # Taking the difference of two terms
    #   pi*vi - p*vi - v*pi + p*v
    #  -pj*vj + p*vj + v*pj - p*v = 0
    #
    #   pi*vi-pj*vj + p*(vj-vi) + v*(pj-pi) = 0
    #
    # With two combinations (i,j) we have enough
    # equations to solve everything
    #
    #   A[i,j] + p*b[i,j] + v*c[i,j] = 0
    #
    # This is a linear system in {px,py,pz,vx,vy,vz}
    #   M.{px,py,pz,vx,vy,vz} = A[i,j]

    M(b, c) = [0   ; b[3];-b[2]; 0   ; c[3];-c[2];;
              -b[3]; 0   ; b[1];-c[3]; 0   ; c[1];;
               b[2];-b[1]; 0   ; c[2];-c[1]; 0]

    A(i,j) = cross(pos[i,:],vel[i,:]) - cross(pos[j,:],vel[j,:])

    pos, vel = parsemat(inp)

    i1 = 1
    j1 = 2
    i2 = 1
    j2 = 3

    matrix = vcat(M(vel[j1,:]-vel[i1,:], pos[j1,:]-pos[i1,:])',
                  M(vel[j2,:]-vel[i2,:], pos[j2,:]-pos[i2,:])')

    inh = -vcat(A(i1,j1), A(i2,j2))
    px,py,pz,vx,vy,vz = inv(matrix)*inh
    Int(round(px+py+pz))
end

inp = strip(open(io->read(io, String), "input24.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
