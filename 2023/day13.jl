function ndiff(a, b)
    function sameq((x,y))
        return Int(x!=y)
    end
    sum(map(sameq, zip(a,b)))
end

function find_reflection(matrix, d)
    n = div(size(matrix,1),2)
    for i in 1:n
        if ndiff(matrix[1:i,:], matrix[2*i:-1:i+1,:]) == d
            return i
        end
    end
    for i in n+1:size(matrix,1)-1
        if ndiff(matrix[i+1:end,:], matrix[i:-1:i-(size(matrix,1)-i-1),:]) == d
            return i
        end
    end
    return -1
end

function solve1(block, d)
    matrix = mapreduce(permutedims, vcat, map(collect,eachsplit(block,"\n")))
    ans = find_reflection(matrix, d)
    if ans > 0
        100*ans
    else
        find_reflection(permutedims(matrix), d)
    end
end
function part1(inp)
    map(x -> solve1(x,0), eachsplit(inp, "\n\n")) |> sum
end
function part2(inp)
    map(x -> solve1(x,1), eachsplit(inp, "\n\n")) |> sum
end

inp = strip(open(io->read(io, String), "input13.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
