function part1(inp)
    filter_digits = filter(isdigit)
    digits_to_int = x -> 10 * parse(Int, first(x)) + parse(Int, last(x))
    map(x -> x|>filter_digits|>digits_to_int, eachsplit(inp, "\n"))|>sum
end

function part2(inp)
    numbers = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
    repl1(str, (n, s)) = replace(str, s => string(s,n,s))
    inp_new = foldl(repl1, enumerate(numbers); init=inp)
    part1(inp_new)
end


inp = strip(open(io->read(io, String), "input01.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
