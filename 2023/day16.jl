const NORTH::UInt8 = 1
const EAST::UInt8 = 2
const SOUTH::UInt8 = 4
const WEST::UInt8 = 8

function play(map, visited, x, y, direction::UInt8)
    if direction == NORTH
        y -= 1
    elseif direction == EAST
        x += 1
    elseif direction == SOUTH
        y += 1
    elseif direction == WEST
        x -= 1
    end
    if x < 1 || y < 1 || x > size(map, 2) || y > size(map, 1)
        return visited
    end

    if (visited[y, x] & direction) != 0
        return visited
    end
    visited[y,x] |= direction

    if map[y,x] == '|'
        if direction == EAST || direction == WEST
            visited = play(map, visited, x, y, NORTH)
            visited = play(map, visited, x, y, SOUTH)
        else
            visited = play(map, visited, x, y, direction)
        end
    elseif map[y,x] == '-'
        if direction == NORTH || direction == SOUTH
            visited = play(map, visited, x, y, EAST)
            visited = play(map, visited, x, y, WEST)
        else
            visited = play(map, visited, x, y, direction)
        end
    elseif map[y,x] == '/'
        if direction == NORTH
            visited = play(map, visited, x, y, EAST)
        elseif direction == EAST
            visited = play(map, visited, x, y, NORTH)
        elseif direction == SOUTH
            visited = play(map, visited, x, y, WEST)
        elseif direction == WEST
            visited = play(map, visited, x, y, SOUTH)
        end
    elseif map[y,x] == '\\'
        if direction == NORTH
            visited = play(map, visited, x, y, WEST)
        elseif direction == EAST
            visited = play(map, visited, x, y, SOUTH)
        elseif direction == SOUTH
            visited = play(map, visited, x, y, EAST)
        elseif direction == WEST
            visited = play(map, visited, x, y, NORTH)
        end
    elseif map[y,x] == '.'
        visited = play(map, visited, x, y, direction)
    end
    return visited
end

function playfrom(matrix, x, y)
    if x == 0
        dir = EAST
    elseif x == size(matrix, 2)+1
        dir = WEST
    elseif y == 0
        dir = SOUTH
    elseif y == size(matrix, 1)+1
        dir = NORTH
    end

    visited = Matrix{UInt8}(undef, size(matrix,1), size(matrix,2))
    visited .= 0
    visited = play(matrix, visited, x, y, dir)
    count(x -> x!=0, visited)
end


function part1(inp)
    matrix = mapreduce(permutedims, vcat, map(collect,eachsplit(inp,"\n")))
    playfrom(matrix, 0,1)
end

function part2(inp)
    matrix = mapreduce(permutedims, vcat, map(collect,eachsplit(inp,"\n")))
    n = size(matrix,2)+1
    start_west = map(y -> playfrom(matrix, 0,y), 1:size(matrix,1)) |> maximum
    start_east = map(y -> playfrom(matrix, n,y), 1:size(matrix,1)) |> maximum
    n = size(matrix,1)+1
    start_north = map(x -> playfrom(matrix, x,0), 1:size(matrix,2)) |> maximum
    start_south = map(x -> playfrom(matrix, x,n), 1:size(matrix,2)) |> maximum
    maximum([start_west, start_east, start_north, start_south])
end

inp = strip(open(io->read(io, String), "input16.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
