function expansion(galaxies, n, h0)
    y = n
    while y > 0
        if count(g -> g[1] == y, galaxies) == 0
            function bumpy(galaxy)
                if galaxy[1] > y
                    return (galaxy[1]+h0, galaxy[2])
                else
                    return galaxy
                end
            end
            galaxies = map(bumpy, galaxies)
        end
        y -= 1
    end

    x = n
    while x > 0
        if count(g -> g[2] == x, galaxies) == 0
            function bumpx(galaxy)
                if galaxy[2] > x
                    return (galaxy[1], galaxy[2]+h0)
                else
                    return galaxy
                end
            end
            galaxies = map(bumpx, galaxies)
        end
        x -= 1
    end
    return galaxies
end

function dist((x1,y1), (x2,y2))
    return abs(x1-x2) + abs(y1-y2)
end

function dists(galaxies)
    acc = 0
    for i = 1:length(galaxies)
        for j = 1:i-1
            acc += dist(galaxies[i], galaxies[j])
        end
    end
    acc
end

function parse(inp)
    matrix = mapreduce(permutedims, vcat, map(collect,eachsplit(inp,"\n")))
    galaxies = map(x->x.I,findall(map(x->x=='#', matrix)))
    return galaxies, size(matrix)[1]
end

function part1(inp)
    galaxies, n = parse(inp)
    galaxies = expansion(galaxies, n, 1)
    dists(galaxies)
end

function part2(inp)
    galaxies, n = parse(inp)
    galaxies = expansion(galaxies, n, 10^6-1)
    dists(galaxies)
end

inp = strip(open(io->read(io, String), "input11.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
