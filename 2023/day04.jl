function part1(inp)
    function n2points(n)
        if n == 0
            return 0
        else
            return 1<<(n-1)
        end
    end

    function doLine(line)
        pref, hav, win = map(split,collect(eachsplit(line,r"[|:]")))
        intersect(Set(hav), Set(win)) |> length |> n2points
    end

    map(doLine, eachsplit(inp, "\n")) |> sum
end

function part2(inp)
    tally = collect(0 for i=1:300)

    for (n, line) in enumerate(eachsplit(inp, "\n"))
        pref, hav, win = map(split,collect(eachsplit(line,r"[|:]")))

        tally[n] += 1
        wins = length(intersect(Set(hav), Set(win)))
        for j in n+1:n+wins
            tally[j] += tally[n]
        end
    end
    tally |> sum
end

inp = strip(open(io->read(io, String), "input04.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")

