function part1(inp)
    symbols = "%#=*+/&-@\$"
    mat = reduce(hcat,map(collect,split(inp)))
    n = length(mat[1,:])
    function processNumber(i, j0, j1)
        number = parse(Int, join(mat[j0:j1,i], ""))

        start = j0 > 1 ? j0-1 : j0
        finish = j1 == n ? j1 : j1 + 1
        if i > 1
            if any(map(x -> x in symbols, mat[start:finish,i-1]))
                return number
            end
        end
        if any(map(x -> x in symbols, mat[start:finish,i]))
            return number
        end
        if i < n
            if any(map(x -> x in symbols, mat[start:finish,i+1]))
                return number
            end
        end
        return 0
    end

    tally = 0
    for i in 1:n
        numberStart = missing
        for j in 1:n
            if isdigit(mat[j,i])
                if numberStart === missing
                    numberStart = j
                end
            else
                if !isequal(numberStart, missing)
                    tally += processNumber(i, numberStart, j-1)
                    numberStart = missing
                end
            end
        end
        if !isequal(numberStart, missing)
            tally += processNumber(i, numberStart, n)
        end
    end
    return tally
end


function part2(inp)
    mat = reduce(hcat,map(collect,split(inp)))
    n = length(mat[1,:])
    mat2 = Matrix{Int64}(undef, n,n)
    mat2 .= 0

    for i in 1:n
        numberStart = missing
        for j in 1:n
            if isdigit(mat[j,i])
                if numberStart === missing
                    numberStart = j
                end
            else
                if !isequal(numberStart, missing)
                    mat2[numberStart:j-1,i] .= parse(Int, join(mat[numberStart:j-1,i], ""))
                    numberStart = missing
                end
            end
        end
        if !isequal(numberStart, missing)
            mat2[numberStart:n,i] .= parse(Int, join(mat[numberStart:n,i], ""))
        end
    end
    tally = 0
    for i in 1:n
        for j in 1:n
            if mat[j,i] == '*'
                start = j > 1 ? j-1 : j
                finish = j == n ? j : j + 1

                adj = []
                if i > 1
                    for n in mat2[start:finish, i-1]
                        adj = push!(adj, (n,i-1))
                    end
                end
                for n in mat2[start:finish, i]
                    adj = push!(adj, (n,i))
                end
                if i < n
                    for n in mat2[start:finish, i+1]
                        adj = push!(adj, (n,i+1))
                    end
                end
                adj2 = filter(x->x[1]>0,unique(adj))
                if length(adj2) == 2
                    tally += adj2[1][1]*adj2[2][1]
                end
            end
        end
    end
    tally
end


inp = strip(open(io->read(io, String), "input03.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
