function shoelace(inp, parser)
    area = 0 ; peri = 0
    x = 1 ; y = 1
    for op in eachsplit(inp, "\n")
        dx, dy, steps = parser(op)
        # Using the shoelace formula, we have
        #  2A = \sum (x[i] y[i+1] - x[i+1] y[i])

        peri += steps
        area += x * (y+dy) - (x+dx) * y
        x += dx ; y += dy
    end
    div(area + peri, 2)+1
end

function part1(inp)
    function parser(op)
        a, steps = split(op)
        steps = parse(Int, steps)

        dx = 0 ; dy = 0
        if a == "R"
            dx = steps
        elseif a == "L"
            dx = -steps
        elseif a == "D"
            dy = steps
        elseif a == "U"
            dy = -steps
        end
        return dx,dy,steps
    end

    shoelace(inp, parser)
end

function part2(inp)
    function parser(op)
        _, _, c = split(replace(op,r"[()#]" => ""))
        steps = parse(Int, "0x" * c[1:end-1])
        dx = 0; dy = 0
        if c[end] == '0'
            dx = steps
        elseif c[end] == '1'
            dy = steps
        elseif c[end] == '2'
            dx = -steps
        elseif c[end] == '3'
            dy = -steps
        end
        return dx,dy,steps
    end

    shoelace(inp, parser)
end

inp = strip(open(io->read(io, String), "input18.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
