# We are given the result of a polynomial in x = 0, .., 20
# and find its coefficients.
#   f(x1) = a0 + a1 x1^1 + a2 x1^2 = y1
#   f(x2) = a0 + a1 x2^1 + a2 x2^2 = y2
#   f(x3) = a0 + a1 x3^1 + a2 x3^2 = y3
#   ...
# In other words
#   a_j x_i^j = X_{ij} a_j = y_i
#   X.a = y
# with the matrix X_{ij} = x_i^j. To solve this,
# left-multiply with X^-1
#   a = X^-1.y
function solve(inp, xnew)
    n = 21
    poly(x) = [convert(Int128,x)^i // 1 for i in 0:n-1]
    Xmat = reduce(vcat, [poly(x)' for x in 1:n])
    Xinv = inv(Xmat)

    parse1(x) = map(y -> parse(Int128, y), split(x))
    coeff(y) = Xinv * y
    solve1(y) = coeff(y)' * poly(xnew)

    ans = map(x -> solve1(parse1(x)), eachsplit(inp, "\n")) |> sum
    convert(Int128, ans)
end

function part1(inp)
    solve(inp, 22)
end
function part2(inp)
    solve(inp, 0)
end

inp = strip(open(io->read(io, String), "input09.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
