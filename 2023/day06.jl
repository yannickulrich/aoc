import Base.parse
toint = x->parse(Int, x)
function play((t, dbest))
    count(v -> v*(t-v) > dbest, 1:t)
end

function part1(inp)
    function parse1(line)
        map(toint,Base.Iterators.drop(split(line), 1))
    end
    times, dists = map(parse1, eachsplit(inp, "\n"))
    map(play, zip(times, dists)) |> prod
end

function part2(inp)
    function parse1(line)
        toint(collect(split(replace(line, " " => ""), ":"))[end])
    end
    time, dist = map(parse1, eachsplit(inp, "\n"))
    play((time, dist))
end

inp = strip(open(io->read(io, String), "input06.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
