toint = x->parse(Int, x)

Springs = Vector{Char}
Groups = Vector{Int}

const solution_memory = Dict{Tuple{Springs,Groups,Int}, Int}()
function count_solutions(springs::Springs, sizes::Groups, size::Int)::Int
    get!(solution_memory, (springs, sizes, size)) do

    if length(springs) == 0
        if length(sizes) == 0 && size == 0
            return 1
        else
            return 0
        end
    end

    function branch(next::Char)::Int
        if next == '#'
            # extending block
            return count_solutions(springs[2:end], sizes, size + 1)
        elseif size > 0
            if length(sizes) > 0 && sizes[1] == size
                # this is a group that can be closed
                return count_solutions(springs[2:end], sizes[2:end], 0)
            else
                # this group cannot be closed, not valid
                return 0
            end
        else
            # not in a group, continue
            return count_solutions(springs[2:end], sizes, 0)
        end
    end

    if springs[1] == '?'
        return branch('#') + branch('.')
    else
        return branch(springs[1])
    end
    end
end

function parse1(line::SubString{String})::Tuple{Springs, Groups}
    springs, rle = split(line)
    springs = collect(springs)
    rle = map(toint, eachsplit(rle, ","))
    springs, rle
end

function solve1(line::SubString{String})::Int
    springs, rle = parse1(line)
    count_solutions(append!(springs, '.'), rle, 0)
end

function part1(inp)
    map(solve1, eachsplit(inp, "\n")) |> sum
end

function solve2(line::SubString{String})::Int
    springs, rle = parse1(line)
    springs = repeat(append!(springs,'?'),5)
    springs[end] = '.'
    rle = repeat(rle, 5)
    count_solutions(springs, rle, 0)
end

function part2(inp)
    map(solve2, eachsplit(inp, "\n")) |> sum
end

inp = strip(open(io->read(io, String), "input12.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
