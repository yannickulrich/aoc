function options(matrix, y, x)::Vector{Tuple{Int,Int}}
    if matrix[y,x] == '>'
        return [(y,x+1)]
    elseif matrix[y,x] == '<'
        return [(y,x-1)]
    elseif matrix[y,x] == 'v'
        return [(y+1,x)]
    elseif matrix[y,x] == '^'
        return [(y-1,x)]
    else
        opts = [(y+1,x),(y-1,x),(y,x+1),(y,x-1)]
        opts = filter(l->l[1] >= 1 && l[2] >= 1, opts)
        opts = filter(l->matrix[l[1],l[2]] != '#', opts)
        return opts
    end
end


function bfs(matrix, stop)
    queue = [(1,2,Set())]
    dist = zeros(Int, size(matrix,1), size(matrix,2))
    dist[1,2] = 0

    while length(queue) > 0
        y,x,seen = popfirst!(queue)
        if (y,x) == stop
            continue
        end
        alt = dist[y,x] + 1
        for (yy,xx) in options(matrix,y,x)
            if (yy,xx) in seen
                continue
            end

            if dist[yy,xx] < alt
                s = copy(seen)
                dist[yy,xx] = alt
                push!(s, (yy,xx))
                push!(queue, (yy,xx,s))
            end
        end
    end
    return dist[stop[1],stop[2]]
end

function dfs(matrix, stop, loc, seen)
    if loc == stop
        return length(seen)
    end

    if loc in seen
        return 0
    end

    s = copy(seen)
    push!(s, loc)
    map(l->dfs(matrix, stop, l, s), options(matrix, loc[1], loc[2])) |> maximum
end

function part1(inp)
    matrix = mapreduce(permutedims, vcat, map(collect,eachsplit(inp,"\n")))
    bfs(matrix,(size(matrix,1),size(matrix,2)-1))
    # dfs(matrix, (size(matrix,1),size(matrix,2)-1), (1,2), Set())
end

function options2(matrix, y, x)::Vector{Tuple{Int,Int}}
    opts = [(y+1,x),(y-1,x),(y,x+1),(y,x-1)]
    opts = filter(l->l[1] >= 1 && l[2] >= 1 && l[1] <= size(matrix,1) && l[2] <= size(matrix,2), opts)
    opts = filter(l->matrix[l[1],l[2]] != '#', opts)
    return opts
end


function find_nodes(matrix)
    ans = [
        (1,2),
        (size(matrix,1),size(matrix,2)-1),
    ]

    vcat(ans,[
        (y,x)
        for y in 1:size(matrix,1)-1
        for x in 1:size(matrix,2)-1
        if matrix[y,x] != '#' && length(options2(matrix, y,x)) > 2
    ])
end

function node2node(matrix, nodes, start, rmatrix)
    queue = [(nodes[start], 0)]
    seen = Set()

    while length(queue) > 0
        (y,x), d = popfirst!(queue)
        if x > size(matrix, 2) || y > size(matrix, 1)
            continue
        end
        ind = findfirst(l->l == (y,x), nodes)
        if !isnothing(ind) && start != ind
            rmatrix[start, ind] = d
            continue
        end
        for n in options2(matrix,y,x)
            if n in seen
                continue
            end

            push!(seen, n)
            push!(queue, (n, d+1))
        end
    end
end


function dfs(matrix, start::Int, stop::Int, seen::UInt64, tally::Int)::Int
    if start == stop
        return tally
    end

    if seen & (1<<(start-1)) != 0
        return 0
    end

    s = seen | (1<<(start-1))

    ans = 0
    for (i, d) in enumerate(matrix[:,start])
        if d == 0
            continue
        end
        new = dfs(matrix, i, stop, s, tally+d)
        if ans < new
            ans = new
        end
    end
    ans
end

function part2(inp)
    matrix = mapreduce(permutedims, vcat, map(collect,eachsplit(inp,"\n")))
    nodes = find_nodes(matrix)

    rmatrix = zeros(Int, size(nodes,1), size(nodes,1))

    for i in 1:length(nodes)
        node2node(matrix, nodes, i, rmatrix)
    end

    dfs(rmatrix,1,2,UInt64(0), 0)
end

inp = strip(open(io->read(io, String), "input23.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
