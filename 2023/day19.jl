function parseworkflow(line)
    function parse1(cmd)::Tuple{Char, Function, String}
        var, value, then = split(cmd, r"[<>:]")
        var = var[1]
        value = parse(Int, value)

        if cmd[2] == '>'
            op = x -> x > value
        elseif cmd[2] == '<'
            op = x -> x < value
        end
        return (var, op, String(then))
    end

    line = split(replace(line, r"[{},]" => " "))
    name = popfirst!(line)
    final = pop!(line)
    return (String(name), (map(parse1, line), String(final)))
end

function parseinput(line)
    numbers = map(x->parse(Int,x), split(line[2:end-1], r"[,=]")[2:2:end])
    Dict('x' => numbers[1], 'm' => numbers[2], 'a' => numbers[3], 's' => numbers[4])
end

function processpart(program, part, func)
    if func == "A"
        return true
    elseif func == "R"
        return false
    end

    steps, final = program[func]
    for (var, op, then) in steps
        if op(part[var])
            return processpart(program, part, then)
        end
    end
    return processpart(program, part, final)
end

function score(part)
    part['x'] + part['m'] + part['a'] + part['s']
end


Leaf = Bool

struct Tree
    v::Char
    op::Char
    th::Int
    l::Union{Leaf, Tree}
    r::Union{Leaf, Tree}
end

function parseworkflowV2(program)
    buf = Dict()
    for line in eachsplit(program, "\n")
        line = split(replace(line, r"[{},]" => " "))
        name = popfirst!(line)
        buf[name] = line
    end

    function parsethen(then)::Union{Leaf, Tree}
        if then == "R"
            return false
        elseif then == "A"
            return true
        else
            return buildtree(then)
        end
    end

    function buildtree(node)
        final = buf[node][end]
        right = parsethen(final)

        for cmd in reverse(buf[node])[2:end]
            th, then = split(cmd[3:end], ":")
            left = parsethen(then)
            right = Tree(cmd[1], cmd[2], parse(Int, th), left, right)
        end
        right
    end

    buildtree("in")
end

function processpartV2(leaf::Leaf, part::Dict)
    return leaf
end

function processpartV2(tree::Tree, part::Dict)
    if tree.op == '<'
        sub = part[tree.v] < tree.th ? tree.l : tree.r
    else
        sub = part[tree.v] > tree.th ? tree.l : tree.r
    end
    processpartV2(sub, part)
end

function part1(inp)
    program, input = split(inp, "\n\n")
    program = parseworkflowV2(program)

    parts = map(parseinput, eachsplit(input))
    parts = filter(x -> processpartV2(program, x), parts)
    map(score, parts) |> sum
end

function processrange(leaf::Leaf, ranges::Dict)
    if leaf
        prod(map(x -> x[2]-x[1] +1, values(ranges)))
    else
        0
    end
end

function processrange(tree::Tree, ranges::Dict)
    mi, ma = ranges[tree.v]

    smaller = copy(ranges)
    bigger  = copy(ranges)

    if tree.op == '<'
        smaller[tree.v] = (mi, tree.th - 1)
        bigger[tree.v] = (tree.th, ma)
        processrange(tree.l, smaller) + processrange(tree.r, bigger)
    else
        smaller[tree.v] = (mi, tree.th)
        bigger[tree.v] = (tree.th+1, ma)
        processrange(tree.r, smaller) + processrange(tree.l, bigger)
    end
end

function part2(inp)
    program, _ = split(inp, "\n\n")
    program = parseworkflowV2(program)
    ranges = Dict('x' => (1,4000), 'm' => (1,4000), 'a' => (1,4000), 's' => (1,4000))
    processrange(program, ranges)
end

inp = strip(open(io->read(io, String), "input19.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
