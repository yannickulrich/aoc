using LinearAlgebra
function parse1(line)
    lhs, rhs = split(line, ": ")
    (lhs, collect(split(rhs)))
end

function part1(inp)
    connections = Dict(map(parse1, eachsplit(inp, "\n")))

    nodes = Set(keys(connections))
    for v in values(connections)
        union!(nodes, v)
    end
    nodes = collect(nodes)

    adj = zeros(Int, length(nodes), length(nodes))
    for (k, v) in connections
        i = findfirst(l->l==k, nodes)
        for vv in v
            j = findfirst(l->l==vv, nodes)

            adj[i,j] = 1
            adj[j,i] = 1
        end
    end

    deg = Diagonal([sum(adj[i,:]) for i in 1:length(nodes)])
    L = deg - adj
    F = eigen(L)

    # find 2nd smallest eigen value
    ind = partialsortperm(F.values, 1:2)[2]
    # otherwise the graph is already disconnected
    @assert F.values[ind] > 0

    fiedler = F.vectors[:, ind]
    n = count(x -> x>0, fiedler)
    n * (length(nodes)-n)
end

inp = strip(open(io->read(io, String), "input25.txt"))
print(part1(inp), "\n")
