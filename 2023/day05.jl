toint = x->parse(Int, x)
function applymap(data, ms)
    ms = collect(eachsplit(ms,"\n"))[2:end]
    ms = map(x -> map(toint, split(x)), ms)
    for i = 1:length(data)
        for (d0, s0, r) in ms
            if s0 <= data[i] <= s0+r
                data[i] = (data[i]-s0)+d0
                break
            end
        end
    end
    data
end

function part1(inp)
    it = eachsplit(inp,"\n\n")
    seeds, it = Base.Iterators.peel(it)
    seeds = map(toint, Base.Iterators.drop(split(seeds),1))

    for m in it
        seeds = applymap(seeds, m)
    end
    minimum(seeds)
end

function applymap2(data, ms)
    ms = collect(eachsplit(ms,"\n"))[2:end]
    ms = map(x -> map(toint, split(x)), ms)

    function apply1((ds,dr)) :: Vector{Tuple{Int64, Int64}}
        if dr == 0
            return []
        end
        for (ts, ss, mr) in ms
            # we have six options
            if ds+dr-1 < ss
                # dat ---
                # src      ---
                continue
            elseif ds < ss <= ds+dr-1 < ss+mr
                # dat -----
                # src   -----
                return vcat(apply1((ds, ss-ds)), [(ts, dr-(ss-ds))])
            elseif ds < ss == ds+dr-1 < ss+mr
                # dat -----
                # src     ---
                return vcat(apply1((ds, dr-1)), [(ts, 1)])
            elseif ss <= ds < ds+dr-1 <= ss+mr
                # dat   ---
                # src -------
                return [(ts + (ds-ss), dr)]
            elseif ds <= ss < ss+mr < ds+dr
                # dat -------
                # src   ---
                return vcat(apply1((ds, ss-ds)), [(ts, mr)], apply1((ss+mr,ds+dr - (ss+mr))))
            elseif ss < ds < ss+mr < ds+dr
                # dat   -------
                # src -----
                return vcat(apply1((ss+mr, ds+dr-(ss+mr))), [(ts + (ds-ss), mr - (ds-ss))])
            elseif ss < ds == ss+mr-1 < ds+dr
                # dat     -----
                # src -----
                return vcat(apply1((ds+1, dr-1)), [(ts, 1)])
            elseif ss < ss+mr == ds < ds+dr
                # dat    ---
                # src ---
                continue
            elseif ss < ss+mr < ds < ds+dr
                # dat      ---
                # src ---
                continue
            else
                print("Erm", (ds,dr)," ", (ss,mr), "\n")
            end
        end
        return [(ds,dr)]
    end
    map(apply1, data) |> Iterators.flatten |> collect
end

function part2(inp)
    it = eachsplit(inp,"\n\n")
    seeds, it = Base.Iterators.peel(it)
    seeds = map(toint, Base.Iterators.drop(split(seeds),1))
    seeds = collect(zip(seeds[1:2:end], seeds[2:2:end]))
    for m in it
        seeds = applymap2(seeds, m)
    end
    map(x->x[1], seeds) |> minimum
end

inp = strip(open(io->read(io, String), "input05.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
