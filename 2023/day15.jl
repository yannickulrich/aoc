function HASH(inp)
    foldl((x,y) -> UInt8(17)*(x + UInt8(y)), inp, init=UInt8(0)) |> Int
end

function part1(inp)
    map(HASH, eachsplit(inp, ",")) |> sum
end

Box = Vector{Tuple{String, Int}}

function score(boxes)
    tally = 0
    for (ib, box) in enumerate(boxes)
        for (il, (_, fl)) in enumerate(box)
            tally += ib * il * fl
        end
    end
    tally
end

function part2(inp)
    boxes = [Box() for i in 1:256]
    for cmd in eachsplit(inp, ",")
        if cmd[end] == '-'
            lab = cmd[1:end-1]
            box = boxes[HASH(lab)+1]
            for i in 1:length(box)
                if box[i][1] == lab
                    popat!(box, i)
                    break
                end
            end
        else
            lab, fl = split(cmd, "=")
            lab = String(lab)
            fl = parse(Int, fl)

            box = boxes[HASH(lab)+1]
            done = false
            for (i, (labi, fli)) in enumerate(box)
                if labi == lab
                    done = true
                    box[i] = (lab, fl)
                    break
                end
            end
            if !done
                append!(box, [(lab, fl)])
            end
        end
    end
    score(boxes)
end

inp = strip(open(io->read(io, String), "input15.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
