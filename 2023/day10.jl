function parse(inp)
    matrix = mapreduce(permutedims, vcat, map(collect,eachsplit(inp,"\n")))
    start = findall(x->x=='S',matrix)[1]
    return matrix, start
end

function transverse((matrix, start))
    y,x = start.I
    y0, x0 = start.I
    mainloop = Set{Tuple{Int,Int}}([(y,x)])

    if matrix[y,x+1] == 'J' || matrix[y,x+1] == '7' || matrix[y,x+1] == '-'
        x += 1
    elseif matrix[y,x-1] == 'L' || matrix[y,x-1] == 'F' || matrix[y,x+1] == '-'
        x -= 1
    elseif matrix[y+1,x] == 'L' || matrix[y+1,x] == 'J' || matrix[y+1,x] == '|'
        y += 1
    elseif matrix[y-1,x] == '7' || matrix[y-1,x] == 'F' || matrix[y-1,x] == '|'
        y -= 1
    end

    while (y,x) != start.I
        push!(mainloop, (y,x))
        yt = y ; xt = x

        if matrix[y,x] == '|'
            if y > y0
                y += 1
            else
                y -= 1
            end
        elseif matrix[y,x] == '-'
            if x > x0
                x += 1
            else
                x -= 1
            end
        elseif matrix[y,x] == 'L'
            if y > y0
                x += 1
            else
                y -= 1
            end
        elseif matrix[y,x] == 'J'
            if y > y0
                x -= 1
            else
                y -= 1
            end
        elseif matrix[y,x] == '7'
            if y < y0
                x -= 1
            else
                y += 1
            end
        elseif matrix[y,x] == 'F'
            if y < y0
                x += 1
            else
                y += 1
            end
        end

        y0 = yt ; x0 = xt
    end
    return mainloop
end

function part1(inp)
    matrix, start = parse(inp)
    div(length(transverse((matrix, start))), 2)
end

function part2(inp)
    matrix, start = parse(inp)
    mainloop = transverse((matrix, start))

    n = 0
    for y in 1:size(matrix,1)
        for x in 1:size(matrix,2)
            insideloop = false
            insidewall = 0
            if !((y,x) in mainloop)
                # let's test this point
                for xx in 1:x-1
                    if (y,xx) in mainloop
                        if matrix[y,xx] == 'L'
                            insidewall = +1
                        elseif matrix[y,xx] == 'F'
                            insidewall = -1
                        elseif matrix[y,xx] == 'J'
                            if insidewall == -1
                                insideloop = !insideloop
                            end
                            insidewall = 0
                        elseif matrix[y,xx] == '7'
                            if insidewall == +1
                                insideloop = !insideloop
                            end
                            insidewall = 0
                        elseif matrix[y,xx] == '|'
                            insideloop = !insideloop
                        end
                    end
                end
                if insideloop
                    n+=1
                end
            end
        end
    end
    n
end

inp = strip(open(io->read(io, String), "input10.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
