dat = Import["input09.txt","Table"]
n = Length[dat[[1]]];
paras=a/@Range[1,n];
poly=Total[paras x^Range[0,n-1]];
sols = First@Solve[Equal[poly/.x->Range[0,n-1], #]]&/@dat;
Print@Total[poly/.sols/.x->21]
Print@Total[poly/.sols/.x->-1]
