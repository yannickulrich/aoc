import Base.parse

function parse(line, jv)
    function card2int(c) ::Int
        if c == 'A'
            14
        elseif c == 'K'
            13
        elseif c == 'Q'
            12
        elseif c == 'J'
            jv
        elseif c == 'T'
            10
        else
            parse(Int, string(c))
        end
    end

    cards, bid = split(line)
    map(card2int, collect(cards)), parse(Int, bid)
end

function hand2type(hand)
    u = unique(hand)
    d=Dict([(i,count(x->x==i,hand)) for i in u if i != 1])
    nj = count(x->x==1,hand)
    if nj == 5
        return 6
    end
    if length(d) == 1
        return 6 # five of a kind XXXXX
    elseif length(d) == 2
        if maximum(values(d))+nj == 4
            return 5 # four of a kind XXXXY
        elseif maximum(values(d))+nj == 3
            return 4 # full house XXXYY
        else
            println("erm..")
        end
    elseif length(d) == 3
        if maximum(values(d))+nj == 3
            return 3 # three of a kind XXXYZ
        else
            return 2 # two pairs XXYYZ
        end
    elseif length(d) == 4
        return 1 # one pair XXYZA
    else
        return 0 # high card XYZAB
    end
end

function cmp(h1, h2)
    t1 = hand2type(h1)
    t2 = hand2type(h2)
    if t1 < t2
        return true
    elseif t1 > t2
        return false
    elseif t1 == t2
        return h1<h2
    end
end

function solve(inp, nj)
    function score1((n, (_, bid)))
        n * bid
    end
    score = x -> map(score1, x)

    dat = map(l -> parse(l, nj), eachsplit(inp, "\n"))
    sort(dat, lt=cmp, by=x->x[1]) |> enumerate |> score |> sum
end

function part1(inp)
    solve(inp, 11)
end
function part2(inp)
    solve(inp, 1)
end

inp = strip(open(io->read(io, String), "input07.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
