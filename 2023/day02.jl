import Base.parse

function myparse(line)
    function parse1(draw)
        n, colour = collect(eachsplit(draw, " "))
        n = parse(Int, n)
        if colour == "red"
            [n,0,0]
        elseif colour == "green"
            [0,n,0]
        elseif colour == "blue"
            [0,0,n]
        end
    end
    drawn = first(Iterators.drop(eachsplit(line, ": "),1))
    map( draw -> sum(map(parse1, eachsplit(draw, ", "))), eachsplit(drawn, "; "))
end

function part1(inp)
    check_valid1(x) = map(g -> all(g.<=[12,13,14]), x)
    check_valid(x) = x[2] |> myparse |> check_valid1 |> all
    filtered = x -> filter(check_valid, x)
    summed = x -> map(y -> y[1], x) |> sum
    eachsplit(inp, "\n") |> enumerate |> collect |> filtered |> summed
end

function part2(inp)
    actual_parse = x -> reduce(hcat, myparse(x)) #WHY??!?
    power = x -> maximum(x, dims=2) |> prod
    map(x -> actual_parse(x) |> power, eachsplit(inp, "\n")) |> sum
end

inp = strip(open(io->read(io, String), "input02.txt"))
print(part1(inp), "\n")
print(part2(inp), "\n")
