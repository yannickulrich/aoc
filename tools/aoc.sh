#!/bin/bash
set -e

if [[ -z "$AOC_SESSION" ]] ; then
    folder=$(dirname $0)
    if [[ -f "$folder/cookie.txt" ]]; then
        AOC_SESSION=$(cat $folder/cookie.txt)
    else
        echo "Need AOC session cookie!" >&2
    fi
fi

year=$(basename $(pwd))

function check_day {
    day=$(echo $1 | perl -pe 's/[A-Za-z]*0*(\d*)(\..*)?/\1/g')
    test $day -ge  1 || help
    test $day -le 25 || help
    echo $day
}

function get_input {
    day=$(check_day $1)
    curl -s -b session=$AOC_SESSION https://adventofcode.com/$year/day/$day/input
}

function get_ans {
    day=$(check_day $1)
    delim=${2:-'\n'}
    regex='Your puzzle answer was <code>([^<]*)<\/code>'
    curl -s -b session=$AOC_SESSION https://adventofcode.com/$year/day/$day \
        | tr '\n' ' ' \
        | perl -pe "s/.*$regex.*$regex.*/\1$delim\2$delim/g" \
        | perl -pe "s/.*$regex.*/\1$delim/g"
}

function help {
    echo "Usage: $0 [input|answer] <day>"
    exit 1
}


case "$1" in
    inp*)
        shift
        get_input $@
        ;;

    ans*)
        shift
        get_ans $1 "$2" || help
        ;;

    *|"")
        ;;
esac
