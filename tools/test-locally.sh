#!/bin/bash
set -e

docker build -t aoc -f tools/Dockerfile .
docker run --rm --name xx aoc sleep infinity &
pid=$!
sleep 0.1
docker cp . xx:/opt/
docker cp ~/.eggnog/sessionid.txt xx:/root/.eggnog/
docker exec -w /opt xx make clean

docker exec -w /opt xx time -f "\n Everything took %Us\n" make check

docker kill xx
